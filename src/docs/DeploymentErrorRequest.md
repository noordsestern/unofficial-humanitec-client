# DeploymentErrorRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**error_type** | **str** |  | [optional] 
**message** | **str** |  | [optional] 
**object_id** | **str** |  | [optional] 
**scope** | **str** |  | [optional] 
**summary** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

