# swagger_client.RuntimeInfoApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_runtime_get**](RuntimeInfoApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime | Get Runtime information about the environment.
[**orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put**](RuntimeInfoApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime/paused | Pause / Resume an environment.
[**orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch**](RuntimeInfoApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime/replicas | Set number of replicas for an environment&#x27;s modules.
[**orgs_org_id_apps_app_id_runtime_get**](RuntimeInfoApi.md#orgs_org_id_apps_app_id_runtime_get) | **GET** /orgs/{orgId}/apps/{appId}/runtime | Get Runtime information about specific environments.

# **orgs_org_id_apps_app_id_envs_env_id_runtime_get**
> RuntimeInfoResponse orgs_org_id_apps_app_id_envs_env_id_runtime_get(org_id, app_id, env_id)

Get Runtime information about the environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RuntimeInfoApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Get Runtime information about the environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RuntimeInfoApi->orgs_org_id_apps_app_id_envs_env_id_runtime_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**RuntimeInfoResponse**](RuntimeInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put**
> orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put(body, org_id, app_id, env_id)

Pause / Resume an environment.

On pause requests, all the Kubernetes Deployment resources are scaled down to 0 replicas.  On resume requests, all the Kubernetes Deployment resources are scaled up to the number of replicas running before the environment was paused.  When an environment is paused, it is not possible to:  ```   - Deploy the environment within Humanitec.   - Scale the number of replicas running of any workload. ```

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RuntimeInfoApi()
body = True # bool | If the value is `true` the request is to pause an environment, if it is `false` is to resume an environment.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Pause / Resume an environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling RuntimeInfoApi->orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**bool**](bool.md)| If the value is &#x60;true&#x60; the request is to pause an environment, if it is &#x60;false&#x60; is to resume an environment.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch**
> orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch(body, org_id, app_id, env_id)

Set number of replicas for an environment's modules.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RuntimeInfoApi()
body = NULL # dict(str, int) | map of replicas by modules.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Set number of replicas for an environment's modules.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling RuntimeInfoApi->orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**dict(str, int)**](dict.md)| map of replicas by modules.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_runtime_get**
> list[EnvironmentRuntimeInfoResponse] orgs_org_id_apps_app_id_runtime_get(org_id, app_id, id=id)

Get Runtime information about specific environments.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RuntimeInfoApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
id = 'id_example' # str | Filter environments by ID (required). Up to 5 ids can be supplied per request.   (optional)

try:
    # Get Runtime information about specific environments.
    api_response = api_instance.orgs_org_id_apps_app_id_runtime_get(org_id, app_id, id=id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RuntimeInfoApi->orgs_org_id_apps_app_id_runtime_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **id** | **str**| Filter environments by ID (required). Up to 5 ids can be supplied per request.   | [optional] 

### Return type

[**list[EnvironmentRuntimeInfoResponse]**](EnvironmentRuntimeInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

