# RegistryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp of when this record was created. | [optional] 
**created_by** | **str** | The user who created this record. | [optional] 
**enable_ci** | **bool** | Indicates if registry secrets and credentials should be exposed to CI agents. | 
**id** | **str** | Registry ID, unique within the Organization. | 
**registry** | **str** | Registry name, usually in a \&quot;{domain}\&quot; or \&quot;{domain}/{project}\&quot; format. | 
**secrets** | [**ClusterSecretsMapResponse**](ClusterSecretsMapResponse.md) |  | [optional] 
**type** | **str** | Registry type, describes the registry authentication method, and defines the schema for the credentials.  Supported values:  - &#x60;public&#x60;  - &#x60;basic&#x60;  - &#x60;google_gcr&#x60;  - &#x60;amazon_ecr&#x60;  - &#x60;secret_ref&#x60; | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

