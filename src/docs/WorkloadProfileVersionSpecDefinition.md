# WorkloadProfileVersionSpecDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**WorkloadProfileVersionSpecDefinitionProperties**](WorkloadProfileVersionSpecDefinitionProperties.md) |  | [optional] 
**runtime_properties** | [**list[WorkloadProfileVersionSpecDefinitionRuntimeProperty]**](WorkloadProfileVersionSpecDefinitionRuntimeProperty.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

