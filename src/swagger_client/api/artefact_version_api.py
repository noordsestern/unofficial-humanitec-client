# coding: utf-8

"""
    Humanitec API

    # Introduction The *Humanitec API* allows you to automate and integrate Humanitec into your developer and operational workflows. The API is a REST based API. It is based around a set of concepts:  * Core * External Resources * Sets and Deltas  ## Authentication Almost all requests made to the Humanitec API require Authentication. Humanitec provides 2 ways of authenticating with the API: `Bearer` and `JWT`.  ### Bearer Authentication This form of authentication makes use of a **static token**. It is intended to be used when machines interact with the Humanitec API. Bearer tokens should be used for very narrow purposes. This allows for the token to be revoked if it is compromised and so limit the scope of exposure. New Bearer tokens can be obtained via the UI:  1. Log into Humanitec at https://app.humanitec.io 1. Go to **Organization Settings** 1. Select **API tokens** 1. Enter a *name* for the new token and click on **Generate new token**  The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_TOKEN`, the request could be made as follows:  ```     curl -H 'Authorization: Bearer HUMANITEC_TOKEN' https://api.humanitec.io/orgs/my-org/apps ```  ### JWT Authentication This form of authentication makes use of a **JSON Web Token (JWT)**. It is intended to be used when humans interact with the Humanitec API. JWTs expire after a period of time. This means that a new JWT will need to be generated regularly. This makes them well suited to working in short sessions, but not for automation. (See Bearer Authentication.) The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_JWT`, the request could be made as follows:  ```     curl -H 'Authorization: JWT HUMANITEC_JWT' https://api.humanitec.io/orgs/my-org/apps ```  ## Content Types All of the Humanitec API unless explicitly only accepts content types of `application/json` and will always return valid `application/json` or an empty response.  ## Response Codes ### Success Any response code in the `2xx` range should be regarded as success.  | **Code** | **Meaning** | | --- | --- | | `200` | Success | | `201` | Success (In future, `201` will be replaced by `200`) | | `204` | Success, but no content in response |  _Note: We plan to simplify the interface by replacing 201 with 200 status codes._  ### Failure Any response code in the `4xx` should be regarded as an error which can be rectified by the client. `5xx` error codes indicate errors that cannot be corrected by the client.  | **Code** | **Meaning** | | --- | --- | | `400` | General error. (Body will contain details) | | `401` | Attempt to access protected resource without `Authorization` Header. | | `403` | The `Bearer` or `JWT` does not grant access to the requested resource. | | `404` | Resource not found. | | `405` | Method not allowed | | `409` | Conflict. Usually indicated a resource with that ID already exists. | | `422` | Unprocessable Entity. The body was not valid JSON, was empty or contained an object different from what was expected. | | `429` | Too many requests - request rate limit has been reached. | | `500` | Internal Error. If it occurs repeatedly, contact support. |   # noqa: E501

    OpenAPI spec version: 0.24.1
    Contact: apiteam@humanitec.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class ArtefactVersionApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def orgs_org_id_artefact_versions_artefact_version_id_get(
        self, org_id, artefact_version_id, **kwargs
    ):  # noqa: E501
        """Get an Artefacts Versions.  # noqa: E501

        Returns a specific Artefact Version.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefact_versions_artefact_version_id_get(org_id, artefact_version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str artefact_version_id: The Artefact Version ID.   (required)
        :return: ArtefactVersionResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.orgs_org_id_artefact_versions_artefact_version_id_get_with_http_info(
                org_id, artefact_version_id, **kwargs
            )  # noqa: E501
        else:
            (
                data
            ) = self.orgs_org_id_artefact_versions_artefact_version_id_get_with_http_info(
                org_id, artefact_version_id, **kwargs
            )  # noqa: E501
            return data

    def orgs_org_id_artefact_versions_artefact_version_id_get_with_http_info(
        self, org_id, artefact_version_id, **kwargs
    ):  # noqa: E501
        """Get an Artefacts Versions.  # noqa: E501

        Returns a specific Artefact Version.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefact_versions_artefact_version_id_get_with_http_info(org_id, artefact_version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str artefact_version_id: The Artefact Version ID.   (required)
        :return: ArtefactVersionResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["org_id", "artefact_version_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in six.iteritems(params["kwargs"]):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method orgs_org_id_artefact_versions_artefact_version_id_get"
                    % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'org_id' is set
        if "org_id" not in params or params["org_id"] is None:
            raise ValueError(
                "Missing the required parameter `org_id` when calling `orgs_org_id_artefact_versions_artefact_version_id_get`"
            )  # noqa: E501
        # verify the required parameter 'artefact_version_id' is set
        if "artefact_version_id" not in params or params["artefact_version_id"] is None:
            raise ValueError(
                "Missing the required parameter `artefact_version_id` when calling `orgs_org_id_artefact_versions_artefact_version_id_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "org_id" in params:
            path_params["orgId"] = params["org_id"]  # noqa: E501
        if "artefact_version_id" in params:
            path_params["artefactVersionId"] = params[
                "artefact_version_id"
            ]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = (
            self.api_client.configuration.auth_settings().keys()
        )  # noqa: E501

        return self.api_client.call_api(
            "/orgs/{orgId}/artefact-versions/{artefactVersionId}",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ArtefactVersionResponse",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def orgs_org_id_artefact_versions_get(self, org_id, **kwargs):  # noqa: E501
        """List all Artefacts Versions.  # noqa: E501

        Returns the Artefact Versions registered with your organization. If no elements are found, an empty list is returned.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefact_versions_get(org_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str name: (Optional) Filter Artefact Versions by name.
        :param str reference: (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with `name`.
        :param str archived: (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.
        :return: list[ArtefactVersionResponse]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.orgs_org_id_artefact_versions_get_with_http_info(
                org_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.orgs_org_id_artefact_versions_get_with_http_info(
                org_id, **kwargs
            )  # noqa: E501
            return data

    def orgs_org_id_artefact_versions_get_with_http_info(
        self, org_id, **kwargs
    ):  # noqa: E501
        """List all Artefacts Versions.  # noqa: E501

        Returns the Artefact Versions registered with your organization. If no elements are found, an empty list is returned.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefact_versions_get_with_http_info(org_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str name: (Optional) Filter Artefact Versions by name.
        :param str reference: (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with `name`.
        :param str archived: (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.
        :return: list[ArtefactVersionResponse]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["org_id", "name", "reference", "archived"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in six.iteritems(params["kwargs"]):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method orgs_org_id_artefact_versions_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'org_id' is set
        if "org_id" not in params or params["org_id"] is None:
            raise ValueError(
                "Missing the required parameter `org_id` when calling `orgs_org_id_artefact_versions_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "org_id" in params:
            path_params["orgId"] = params["org_id"]  # noqa: E501

        query_params = []
        if "name" in params:
            query_params.append(("name", params["name"]))  # noqa: E501
        if "reference" in params:
            query_params.append(("reference", params["reference"]))  # noqa: E501
        if "archived" in params:
            query_params.append(("archived", params["archived"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = (
            self.api_client.configuration.auth_settings().keys()
        )  # noqa: E501

        return self.api_client.call_api(
            "/orgs/{orgId}/artefact-versions",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="list[ArtefactVersionResponse]",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def orgs_org_id_artefact_versions_post(self, body, org_id, **kwargs):  # noqa: E501
        """Register a new Artefact Version with your organization.  # noqa: E501

               This method makes a synchronous HTTP request by default. To make an
               asynchronous HTTP request, please pass async_req=True
               >>> thread = api.orgs_org_id_artefact_versions_post(body, org_id, async_req=True)
               >>> result = thread.get()

               :param async_req bool
               :param AddArtefactVersionPayloadRequest body: The data needed to register a new Artefact Version within the organization.

        (required)
               :param str org_id: The organization ID.   (required)
               :param str vcs: (Optional) Which version control system the version comes from. Default value is \"git\". If this parameter is not supplied or its value is \"git\", the provided ref, if not empty, is checked to ensure that it has the prefix \"refs/\".
               :return: ArtefactVersionResponse
                        If the method is called asynchronously,
                        returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.orgs_org_id_artefact_versions_post_with_http_info(
                body, org_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.orgs_org_id_artefact_versions_post_with_http_info(
                body, org_id, **kwargs
            )  # noqa: E501
            return data

    def orgs_org_id_artefact_versions_post_with_http_info(
        self, body, org_id, **kwargs
    ):  # noqa: E501
        """Register a new Artefact Version with your organization.  # noqa: E501

               This method makes a synchronous HTTP request by default. To make an
               asynchronous HTTP request, please pass async_req=True
               >>> thread = api.orgs_org_id_artefact_versions_post_with_http_info(body, org_id, async_req=True)
               >>> result = thread.get()

               :param async_req bool
               :param AddArtefactVersionPayloadRequest body: The data needed to register a new Artefact Version within the organization.

        (required)
               :param str org_id: The organization ID.   (required)
               :param str vcs: (Optional) Which version control system the version comes from. Default value is \"git\". If this parameter is not supplied or its value is \"git\", the provided ref, if not empty, is checked to ensure that it has the prefix \"refs/\".
               :return: ArtefactVersionResponse
                        If the method is called asynchronously,
                        returns the request thread.
        """

        all_params = ["body", "org_id", "vcs"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in six.iteritems(params["kwargs"]):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method orgs_org_id_artefact_versions_post" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `orgs_org_id_artefact_versions_post`"
            )  # noqa: E501
        # verify the required parameter 'org_id' is set
        if "org_id" not in params or params["org_id"] is None:
            raise ValueError(
                "Missing the required parameter `org_id` when calling `orgs_org_id_artefact_versions_post`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "org_id" in params:
            path_params["orgId"] = params["org_id"]  # noqa: E501

        query_params = []
        if "vcs" in params:
            query_params.append(("vcs", params["vcs"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = (
            self.api_client.configuration.auth_settings().keys()
        )  # noqa: E501

        return self.api_client.call_api(
            "/orgs/{orgId}/artefact-versions",
            "POST",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ArtefactVersionResponse",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def orgs_org_id_artefacts_artefact_id_versions_get(
        self, org_id, artefact_id, **kwargs
    ):  # noqa: E501
        """List all Artefact Versions of an Artefact.  # noqa: E501

        Returns the Artefact Versions of a specified Artefact registered with your organization. If no elements are found, an empty list is returned.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefacts_artefact_id_versions_get(org_id, artefact_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str artefact_id: The Artefact ID.   (required)
        :param str archived: (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.
        :param str reference: (Optional) Filter Artefact Versions by by name including a version or digest.
        :param str limit: (Optional) Limit the number of versions returned by the endpoint.
        :return: list[ArtefactVersionResponse]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.orgs_org_id_artefacts_artefact_id_versions_get_with_http_info(
                org_id, artefact_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.orgs_org_id_artefacts_artefact_id_versions_get_with_http_info(
                org_id, artefact_id, **kwargs
            )  # noqa: E501
            return data

    def orgs_org_id_artefacts_artefact_id_versions_get_with_http_info(
        self, org_id, artefact_id, **kwargs
    ):  # noqa: E501
        """List all Artefact Versions of an Artefact.  # noqa: E501

        Returns the Artefact Versions of a specified Artefact registered with your organization. If no elements are found, an empty list is returned.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.orgs_org_id_artefacts_artefact_id_versions_get_with_http_info(org_id, artefact_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str org_id: The organization ID.   (required)
        :param str artefact_id: The Artefact ID.   (required)
        :param str archived: (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.
        :param str reference: (Optional) Filter Artefact Versions by by name including a version or digest.
        :param str limit: (Optional) Limit the number of versions returned by the endpoint.
        :return: list[ArtefactVersionResponse]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "org_id",
            "artefact_id",
            "archived",
            "reference",
            "limit",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in six.iteritems(params["kwargs"]):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method orgs_org_id_artefacts_artefact_id_versions_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'org_id' is set
        if "org_id" not in params or params["org_id"] is None:
            raise ValueError(
                "Missing the required parameter `org_id` when calling `orgs_org_id_artefacts_artefact_id_versions_get`"
            )  # noqa: E501
        # verify the required parameter 'artefact_id' is set
        if "artefact_id" not in params or params["artefact_id"] is None:
            raise ValueError(
                "Missing the required parameter `artefact_id` when calling `orgs_org_id_artefacts_artefact_id_versions_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "org_id" in params:
            path_params["orgId"] = params["org_id"]  # noqa: E501
        if "artefact_id" in params:
            path_params["artefactId"] = params["artefact_id"]  # noqa: E501

        query_params = []
        if "archived" in params:
            query_params.append(("archived", params["archived"]))  # noqa: E501
        if "reference" in params:
            query_params.append(("reference", params["reference"]))  # noqa: E501
        if "limit" in params:
            query_params.append(("limit", params["limit"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = (
            self.api_client.configuration.auth_settings().keys()
        )  # noqa: E501

        return self.api_client.call_api(
            "/orgs/{orgId}/artefacts/{artefactId}/versions",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="list[ArtefactVersionResponse]",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def orgs_org_id_artefacts_artefact_id_versions_version_id_patch(
        self, body, org_id, artefact_id, version_id, **kwargs
    ):  # noqa: E501
        """Update Version of an Artefact.  # noqa: E501

               Update the version of a specified Artefact registered with your organization\".  # noqa: E501
               This method makes a synchronous HTTP request by default. To make an
               asynchronous HTTP request, please pass async_req=True
               >>> thread = api.orgs_org_id_artefacts_artefact_id_versions_version_id_patch(body, org_id, artefact_id, version_id, async_req=True)
               >>> result = thread.get()

               :param async_req bool
               :param UpdateArtefactVersionPayloadRequest body: The Artefact Version Update Request. Only the field `archive` can be updated.

        (required)
               :param str org_id: The organization ID.   (required)
               :param str artefact_id: The Artefact ID.   (required)
               :param str version_id: The Version ID.   (required)
               :return: ArtefactVersionResponse
                        If the method is called asynchronously,
                        returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.orgs_org_id_artefacts_artefact_id_versions_version_id_patch_with_http_info(
                body, org_id, artefact_id, version_id, **kwargs
            )  # noqa: E501
        else:
            (
                data
            ) = self.orgs_org_id_artefacts_artefact_id_versions_version_id_patch_with_http_info(
                body, org_id, artefact_id, version_id, **kwargs
            )  # noqa: E501
            return data

    def orgs_org_id_artefacts_artefact_id_versions_version_id_patch_with_http_info(
        self, body, org_id, artefact_id, version_id, **kwargs
    ):  # noqa: E501
        """Update Version of an Artefact.  # noqa: E501

               Update the version of a specified Artefact registered with your organization\".  # noqa: E501
               This method makes a synchronous HTTP request by default. To make an
               asynchronous HTTP request, please pass async_req=True
               >>> thread = api.orgs_org_id_artefacts_artefact_id_versions_version_id_patch_with_http_info(body, org_id, artefact_id, version_id, async_req=True)
               >>> result = thread.get()

               :param async_req bool
               :param UpdateArtefactVersionPayloadRequest body: The Artefact Version Update Request. Only the field `archive` can be updated.

        (required)
               :param str org_id: The organization ID.   (required)
               :param str artefact_id: The Artefact ID.   (required)
               :param str version_id: The Version ID.   (required)
               :return: ArtefactVersionResponse
                        If the method is called asynchronously,
                        returns the request thread.
        """

        all_params = ["body", "org_id", "artefact_id", "version_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in six.iteritems(params["kwargs"]):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method orgs_org_id_artefacts_artefact_id_versions_version_id_patch"
                    % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `orgs_org_id_artefacts_artefact_id_versions_version_id_patch`"
            )  # noqa: E501
        # verify the required parameter 'org_id' is set
        if "org_id" not in params or params["org_id"] is None:
            raise ValueError(
                "Missing the required parameter `org_id` when calling `orgs_org_id_artefacts_artefact_id_versions_version_id_patch`"
            )  # noqa: E501
        # verify the required parameter 'artefact_id' is set
        if "artefact_id" not in params or params["artefact_id"] is None:
            raise ValueError(
                "Missing the required parameter `artefact_id` when calling `orgs_org_id_artefacts_artefact_id_versions_version_id_patch`"
            )  # noqa: E501
        # verify the required parameter 'version_id' is set
        if "version_id" not in params or params["version_id"] is None:
            raise ValueError(
                "Missing the required parameter `version_id` when calling `orgs_org_id_artefacts_artefact_id_versions_version_id_patch`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "org_id" in params:
            path_params["orgId"] = params["org_id"]  # noqa: E501
        if "artefact_id" in params:
            path_params["artefactId"] = params["artefact_id"]  # noqa: E501
        if "version_id" in params:
            path_params["versionId"] = params["version_id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = (
            self.api_client.configuration.auth_settings().keys()
        )  # noqa: E501

        return self.api_client.call_api(
            "/orgs/{orgId}/artefacts/{artefactId}/versions/{versionId}",
            "PATCH",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ArtefactVersionResponse",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )
