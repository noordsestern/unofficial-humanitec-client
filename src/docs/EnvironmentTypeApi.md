# swagger_client.EnvironmentTypeApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_env_types_env_type_id_delete**](EnvironmentTypeApi.md#orgs_org_id_env_types_env_type_id_delete) | **DELETE** /orgs/{orgId}/env-types/{envTypeId} | Deletes an Environment Type
[**orgs_org_id_env_types_env_type_id_get**](EnvironmentTypeApi.md#orgs_org_id_env_types_env_type_id_get) | **GET** /orgs/{orgId}/env-types/{envTypeId} | Get an Environment Type
[**orgs_org_id_env_types_get**](EnvironmentTypeApi.md#orgs_org_id_env_types_get) | **GET** /orgs/{orgId}/env-types | List all Environment Types
[**orgs_org_id_env_types_post**](EnvironmentTypeApi.md#orgs_org_id_env_types_post) | **POST** /orgs/{orgId}/env-types | Add a new Environment Type

# **orgs_org_id_env_types_env_type_id_delete**
> EnvironmentTypeResponse orgs_org_id_env_types_env_type_id_delete(org_id, env_type_id)

Deletes an Environment Type

Deletes a specific Environment Type from an Organization. If there are Environments with this Type in the Organization, the operation will fail.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentTypeApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type_id = 'env_type_id_example' # str | ID of the Environment Type.  

try:
    # Deletes an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_id_delete(org_id, env_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentTypeApi->orgs_org_id_env_types_env_type_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type_id** | **str**| ID of the Environment Type.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_id_get**
> EnvironmentTypeResponse orgs_org_id_env_types_env_type_id_get(org_id, env_type_id)

Get an Environment Type

Gets a specific Environment Type within an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentTypeApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type_id = 'env_type_id_example' # str | ID of the Environment Type.  

try:
    # Get an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_id_get(org_id, env_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentTypeApi->orgs_org_id_env_types_env_type_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type_id** | **str**| ID of the Environment Type.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_get**
> list[EnvironmentTypeResponse] orgs_org_id_env_types_get(org_id)

List all Environment Types

Lists all Environment Types in an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentTypeApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List all Environment Types
    api_response = api_instance.orgs_org_id_env_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentTypeApi->orgs_org_id_env_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[EnvironmentTypeResponse]**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_post**
> EnvironmentTypeResponse orgs_org_id_env_types_post(body, org_id)

Add a new Environment Type

Adds a new Environment Type to an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentTypeApi()
body = swagger_client.EnvironmentTypeRequest() # EnvironmentTypeRequest | New Environment Type.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Add a new Environment Type
    api_response = api_instance.orgs_org_id_env_types_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentTypeApi->orgs_org_id_env_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnvironmentTypeRequest**](EnvironmentTypeRequest.md)| New Environment Type.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

