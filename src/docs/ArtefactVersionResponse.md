# ArtefactVersionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**archived** | **bool** | If the Artefact Version is archived. | 
**artefact_id** | **str** | The UUID of the Artefact. | 
**commit** | **str** | (Optional) The commit ID the Artefact Version was built on. | 
**created_at** | **str** | The time when the Artefact Version was added to Humanitec. | [optional] 
**created_by** | **str** | The user ID of the user who added the Artefact Version to Humanitec. | [optional] 
**digest** | **str** | (Optional) The Artefact Version digest. | 
**id** | **str** | The UUID of the Artefact Version. | 
**name** | **str** | The name of the Artefact. | 
**ref** | **str** | (Optional) The ref the Artefact Version was built from. | 
**updated_at** | **str** | The time when the Artefact Version was updated for the last time. | [optional] 
**updated_by** | **str** | The user ID of the user who performed the last updated on the Artefact Version. | [optional] 
**version** | **str** | (Optional) The version of the Artefact Version. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

