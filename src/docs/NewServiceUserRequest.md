# NewServiceUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | The email address that should get notifications about this service user. (Optional) | [optional] 
**name** | **str** | The name that should be shown for this service user. | 
**role** | **str** | The role that the service user should have on the organization it is created in | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

