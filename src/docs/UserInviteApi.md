# swagger_client.UserInviteApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_invitations_get**](UserInviteApi.md#orgs_org_id_invitations_get) | **GET** /orgs/{orgId}/invitations | List the invites issued for the organization.

# **orgs_org_id_invitations_get**
> list[UserInviteResponse] orgs_org_id_invitations_get(org_id)

List the invites issued for the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserInviteApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List the invites issued for the organization.
    api_response = api_instance.orgs_org_id_invitations_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserInviteApi->orgs_org_id_invitations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserInviteResponse]**](UserInviteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

