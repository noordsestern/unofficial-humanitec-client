# swagger_client.ResourceAccountApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_accounts_acc_id_delete**](ResourceAccountApi.md#orgs_org_id_resources_accounts_acc_id_delete) | **DELETE** /orgs/{orgId}/resources/accounts/{accId} | Delete an unused Resource Account.
[**orgs_org_id_resources_accounts_acc_id_get**](ResourceAccountApi.md#orgs_org_id_resources_accounts_acc_id_get) | **GET** /orgs/{orgId}/resources/accounts/{accId} | Get a Resource Account.
[**orgs_org_id_resources_accounts_acc_id_patch**](ResourceAccountApi.md#orgs_org_id_resources_accounts_acc_id_patch) | **PATCH** /orgs/{orgId}/resources/accounts/{accId} | Update a Resource Account.
[**orgs_org_id_resources_accounts_get**](ResourceAccountApi.md#orgs_org_id_resources_accounts_get) | **GET** /orgs/{orgId}/resources/accounts | List Resource Accounts in the organization.
[**orgs_org_id_resources_accounts_post**](ResourceAccountApi.md#orgs_org_id_resources_accounts_post) | **POST** /orgs/{orgId}/resources/accounts | Create a new Resource Account in the organization.

# **orgs_org_id_resources_accounts_acc_id_delete**
> orgs_org_id_resources_accounts_acc_id_delete(org_id, acc_id)

Delete an unused Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceAccountApi()
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Delete an unused Resource Account.
    api_instance.orgs_org_id_resources_accounts_acc_id_delete(org_id, acc_id)
except ApiException as e:
    print("Exception when calling ResourceAccountApi->orgs_org_id_resources_accounts_acc_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_acc_id_get**
> ResourceAccountResponse orgs_org_id_resources_accounts_acc_id_get(org_id, acc_id)

Get a Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceAccountApi()
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Get a Resource Account.
    api_response = api_instance.orgs_org_id_resources_accounts_acc_id_get(org_id, acc_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceAccountApi->orgs_org_id_resources_accounts_acc_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_acc_id_patch**
> ResourceAccountResponse orgs_org_id_resources_accounts_acc_id_patch(body, org_id, acc_id)

Update a Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceAccountApi()
body = swagger_client.UpdateResourceAccountRequestRequest() # UpdateResourceAccountRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Update a Resource Account.
    api_response = api_instance.orgs_org_id_resources_accounts_acc_id_patch(body, org_id, acc_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceAccountApi->orgs_org_id_resources_accounts_acc_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateResourceAccountRequestRequest**](UpdateResourceAccountRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_get**
> list[ResourceAccountResponse] orgs_org_id_resources_accounts_get(org_id)

List Resource Accounts in the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceAccountApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Accounts in the organization.
    api_response = api_instance.orgs_org_id_resources_accounts_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceAccountApi->orgs_org_id_resources_accounts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ResourceAccountResponse]**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_post**
> ResourceAccountResponse orgs_org_id_resources_accounts_post(body, org_id)

Create a new Resource Account in the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceAccountApi()
body = swagger_client.CreateResourceAccountRequestRequest() # CreateResourceAccountRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a new Resource Account in the organization.
    api_response = api_instance.orgs_org_id_resources_accounts_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceAccountApi->orgs_org_id_resources_accounts_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateResourceAccountRequestRequest**](CreateResourceAccountRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

