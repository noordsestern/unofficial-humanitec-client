# ValueSetVersionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**change** | [**JSONPatchesResponse**](JSONPatchesResponse.md) |  | 
**comment** | **str** |  | 
**created_at** | **datetime** |  | 
**created_by** | **str** |  | 
**id** | **str** |  | 
**result_of** | [**ValueSetVersionResultOf**](ValueSetVersionResultOf.md) |  | 
**source_value_set_version_id** | **str** |  | 
**updated_at** | **datetime** |  | 
**values** | [**ValueSetResponse**](ValueSetResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

