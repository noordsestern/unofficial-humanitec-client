# NodeBodyResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_class** | **str** |  | 
**criteria_id** | **str** |  | 
**def_id** | **str** |  | 
**depends_on** | **list[str]** |  | 
**driver** | **dict(str, object)** |  | 
**driver_type** | **str** |  | 
**guresid** | **str** |  | 
**id** | **str** |  | 
**resource** | **dict(str, object)** |  | 
**resource_schema** | **dict(str, object)** |  | 
**target** | **str** |  | [optional] 
**type** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

