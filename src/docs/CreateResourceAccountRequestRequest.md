# CreateResourceAccountRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials** | **dict(str, object)** | Credentials associated with the account. | [optional] 
**id** | **str** | Unique identifier for the account (in scope of the organization it belongs to). | [optional] 
**name** | **str** | Display name. | [optional] 
**type** | **str** | The type of the account | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

