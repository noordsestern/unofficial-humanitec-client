# UpdateResourceAccountRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials** | **dict(str, object)** | Credentials associated with the account. | [optional] 
**name** | **str** | Display name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

