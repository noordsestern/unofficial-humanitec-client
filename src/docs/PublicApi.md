# swagger_client.PublicApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**approve_approval_request**](PublicApi.md#approve_approval_request) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId}/approve | Approve the approval request
[**cancel_run**](PublicApi.md#cancel_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/cancel | Cancel a Run within an Pipeline.
[**create_pipeline**](PublicApi.md#create_pipeline) | **POST** /orgs/{orgId}/apps/{appId}/pipelines | Create a Pipeline within an Application.
[**create_pipeline_run**](PublicApi.md#create_pipeline_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs | Create a run within a pipeline.
[**create_public_key**](PublicApi.md#create_public_key) | **POST** /orgs/{orgId}/keys | Associate a new RSA public key to an organization.
[**create_workload_profile_chart_version**](PublicApi.md#create_workload_profile_chart_version) | **POST** /orgs/{orgId}/workload-profile-chart-versions | Add new Workload Profile Chart Version
[**create_workload_profile_version**](PublicApi.md#create_workload_profile_version) | **POST** /orgs/{orgId}/workload-profiles/{profileQid}/versions | Add new Version of the Workload Profile
[**current_user_get**](PublicApi.md#current_user_get) | **GET** /current-user | Gets the extended profile of the current user
[**current_user_patch**](PublicApi.md#current_user_patch) | **PATCH** /current-user | Updates the extended profile of the current user.
[**delete_pipeline**](PublicApi.md#delete_pipeline) | **DELETE** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | Delete a pipeline within an application.
[**delete_public_key**](PublicApi.md#delete_public_key) | **DELETE** /orgs/{orgId}/keys/{keyId} | Delete a public key associated to an organization.
[**delete_run**](PublicApi.md#delete_run) | **DELETE** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId} | Deleting a completed Run within a Pipeline.
[**deny_approval_request**](PublicApi.md#deny_approval_request) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId}/deny | Deny the approval request
[**get_approval_request**](PublicApi.md#get_approval_request) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId} | Get an approval request
[**get_delta**](PublicApi.md#get_delta) | **GET** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Fetch an existing Delta
[**get_humanitec_public_keys**](PublicApi.md#get_humanitec_public_keys) | **GET** /orgs/{orgId}/humanitec-keys | List all the public keys Humanitec shares with an organization.
[**get_latest_pipeline_definition_schema**](PublicApi.md#get_latest_pipeline_definition_schema) | **GET** /orgs/{orgId}/pipeline-schemas/latest | Gets the latest pipeline schema
[**get_latest_workload_profile_version**](PublicApi.md#get_latest_workload_profile_version) | **GET** /orgs/{orgId}/workload-profiles/{profileQid}/versions/latest | Latest version of the given workload profile with optional constraint.
[**get_pipeline**](PublicApi.md#get_pipeline) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | Get a Pipeline within an Application.
[**get_pipeline_definition**](PublicApi.md#get_pipeline_definition) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/schema | Get a pipeline schema.
[**get_public_key**](PublicApi.md#get_public_key) | **GET** /orgs/{orgId}/keys/{keyId} | Retrieve a single public key associated to an organization.
[**get_public_keys**](PublicApi.md#get_public_keys) | **GET** /orgs/{orgId}/keys | List all public keys associated to an organization.
[**get_run**](PublicApi.md#get_run) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId} | Get a run within an pipeline.
[**get_run_job**](PublicApi.md#get_run_job) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId} | List the details of a Job including Step information
[**get_sets**](PublicApi.md#get_sets) | **GET** /orgs/{orgId}/apps/{appId}/sets | Get all Deployment Sets
[**list_approval_requests**](PublicApi.md#list_approval_requests) | **GET** /orgs/{orgId}/apps/{appId}/approvals | List of the approval requests
[**list_pipeline_runs**](PublicApi.md#list_pipeline_runs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs | List runs within a pipeline.
[**list_pipeline_runs_by_org**](PublicApi.md#list_pipeline_runs_by_org) | **GET** /orgs/{orgId}/pipeline-runs | List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.
[**list_pipeline_versions**](PublicApi.md#list_pipeline_versions) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/versions | List all versions of the pipeline
[**list_pipelines**](PublicApi.md#list_pipelines) | **GET** /orgs/{orgId}/apps/{appId}/pipelines | List Pipelines within an Application.
[**list_pipelines_in_org**](PublicApi.md#list_pipelines_in_org) | **GET** /orgs/{orgId}/pipelines | List all Pipelines within an Organization. This can be filtered by Application.
[**list_run_job_step_logs**](PublicApi.md#list_run_job_step_logs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/steps/{stepIndex}/logs | Get a page of log output for a given step within a job.
[**list_run_jobs**](PublicApi.md#list_run_jobs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs | List the details of the jobs with in a pipeline run.
[**list_workload_profile_chart_versions**](PublicApi.md#list_workload_profile_chart_versions) | **GET** /orgs/{orgId}/workload-profile-chart-versions | Workload Profile Chart Versions for the given organization.
[**list_workload_profile_versions**](PublicApi.md#list_workload_profile_versions) | **GET** /orgs/{orgId}/workload-profiles/{profileQid}/versions | List versions of the given workload profile.
[**orgs_get**](PublicApi.md#orgs_get) | **GET** /orgs | List active organizations the user has access to.
[**orgs_org_id_apps_app_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId} | Delete an Application
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put**](PublicApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/archived | Mark a Delta as \&quot;archived\&quot;
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put**](PublicApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/env_id | Change the Environment of a Delta
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put**](PublicApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/name | Change the name of a Delta
[**orgs_org_id_apps_app_id_deltas_delta_id_patch**](PublicApi.md#orgs_org_id_apps_app_id_deltas_delta_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Update an existing Delta
[**orgs_org_id_apps_app_id_deltas_get**](PublicApi.md#orgs_org_id_apps_app_id_deltas_get) | **GET** /orgs/{orgId}/apps/{appId}/deltas | List Deltas in an Application
[**orgs_org_id_apps_app_id_deltas_post**](PublicApi.md#orgs_org_id_apps_app_id_deltas_post) | **POST** /orgs/{orgId}/apps/{appId}/deltas | Create a new Delta
[**orgs_org_id_apps_app_id_envs_env_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId} | Delete a specific Environment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys/{deployId}/errors | List errors that occurred in a Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys/{deployId} | Get a specific Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys | List Deployments in an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys | Start a new Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/from_deploy_id | Rebase to a different Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId} | Get a specific Environment.
[**orgs_org_id_apps_app_id_envs_env_id_resources_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources | List Active Resources provisioned in an environment.
[**orgs_org_id_apps_app_id_envs_env_id_resources_graph_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_graph_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources/graph | Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.
[**orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources/{type}/{resId} | Delete Active Resources.
[**orgs_org_id_apps_app_id_envs_env_id_rules_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules | List all Automation Rules in an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules | Create a new Automation Rule for an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Delete Automation Rule from an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Get a specific Automation Rule for an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Update an existing Automation Rule for an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_runtime_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime | Get Runtime information about the environment.
[**orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime/paused | Pause / Resume an environment.
[**orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/envs/{envId}/runtime/replicas | Set number of replicas for an environment&#x27;s modules.
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions | List Value Set Versions in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId} | Get a single Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/purge/{key} | Purge the value of a specific Shared Value from the App Environment Version history.
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/restore/{key} | Restore a specific key from the Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/restore | Restore a Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_values_delete**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | Delete all Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | List Shared Values in an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_delete**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Delete Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_patch**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Update Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_put**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Update Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_env_id_values_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | Create a Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_get**](PublicApi.md#orgs_org_id_apps_app_id_envs_get) | **GET** /orgs/{orgId}/apps/{appId}/envs | List all Environments.
[**orgs_org_id_apps_app_id_envs_post**](PublicApi.md#orgs_org_id_apps_app_id_envs_post) | **POST** /orgs/{orgId}/apps/{appId}/envs | Add a new Environment to an Application.
[**orgs_org_id_apps_app_id_get**](PublicApi.md#orgs_org_id_apps_app_id_get) | **GET** /orgs/{orgId}/apps/{appId} | Get an existing Application
[**orgs_org_id_apps_app_id_jobs_delete**](PublicApi.md#orgs_org_id_apps_app_id_jobs_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/jobs | Deletes all Jobs for the Application
[**orgs_org_id_apps_app_id_runtime_get**](PublicApi.md#orgs_org_id_apps_app_id_runtime_get) | **GET** /orgs/{orgId}/apps/{appId}/runtime | Get Runtime information about specific environments.
[**orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get**](PublicApi.md#orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get) | **GET** /orgs/{orgId}/apps/{appId}/sets/{setId}/diff/{sourceSetId} | Get the difference between 2 Deployment Sets
[**orgs_org_id_apps_app_id_sets_set_id_get**](PublicApi.md#orgs_org_id_apps_app_id_sets_set_id_get) | **GET** /orgs/{orgId}/apps/{appId}/sets/{setId} | Get a Deployment Set
[**orgs_org_id_apps_app_id_sets_set_id_post**](PublicApi.md#orgs_org_id_apps_app_id_sets_set_id_post) | **POST** /orgs/{orgId}/apps/{appId}/sets/{setId} | Apply a Deployment Delta to a Deployment Set
[**orgs_org_id_apps_app_id_users_get**](PublicApi.md#orgs_org_id_apps_app_id_users_get) | **GET** /orgs/{orgId}/apps/{appId}/users | List Users with roles in an App
[**orgs_org_id_apps_app_id_users_post**](PublicApi.md#orgs_org_id_apps_app_id_users_post) | **POST** /orgs/{orgId}/apps/{appId}/users | Adds a User to an Application with a Role
[**orgs_org_id_apps_app_id_users_user_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_users_user_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/users/{userId} | Remove the role of a User on an Application
[**orgs_org_id_apps_app_id_users_user_id_get**](PublicApi.md#orgs_org_id_apps_app_id_users_user_id_get) | **GET** /orgs/{orgId}/apps/{appId}/users/{userId} | Get the role of a User on an Application
[**orgs_org_id_apps_app_id_users_user_id_patch**](PublicApi.md#orgs_org_id_apps_app_id_users_user_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/users/{userId} | Update the role of a User on an Application
[**orgs_org_id_apps_app_id_value_set_versions_get**](PublicApi.md#orgs_org_id_apps_app_id_value_set_versions_get) | **GET** /orgs/{orgId}/apps/{appId}/value-set-versions | List Value Set Versions in the App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get**](PublicApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get) | **GET** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId} | Get a single Value Set Version from the App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post**](PublicApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/purge/{key} | Purge the value of a specific Shared Value from the App Version history.
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post**](PublicApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/restore/{key} | Restore a specific key from the Value Set Version in an App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post**](PublicApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/restore | Restore a Value Set Version in an App
[**orgs_org_id_apps_app_id_values_delete**](PublicApi.md#orgs_org_id_apps_app_id_values_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/values | Delete all Shared Value for an App
[**orgs_org_id_apps_app_id_values_get**](PublicApi.md#orgs_org_id_apps_app_id_values_get) | **GET** /orgs/{orgId}/apps/{appId}/values | List Shared Values in an Application
[**orgs_org_id_apps_app_id_values_key_delete**](PublicApi.md#orgs_org_id_apps_app_id_values_key_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/values/{key} | Delete Shared Value for an Application
[**orgs_org_id_apps_app_id_values_key_patch**](PublicApi.md#orgs_org_id_apps_app_id_values_key_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/values/{key} | Update Shared Value for an Application
[**orgs_org_id_apps_app_id_values_key_put**](PublicApi.md#orgs_org_id_apps_app_id_values_key_put) | **PUT** /orgs/{orgId}/apps/{appId}/values/{key} | Update Shared Value for an Application
[**orgs_org_id_apps_app_id_values_post**](PublicApi.md#orgs_org_id_apps_app_id_values_post) | **POST** /orgs/{orgId}/apps/{appId}/values | Create a Shared Value for an Application
[**orgs_org_id_apps_app_id_webhooks_get**](PublicApi.md#orgs_org_id_apps_app_id_webhooks_get) | **GET** /orgs/{orgId}/apps/{appId}/webhooks | List Webhooks
[**orgs_org_id_apps_app_id_webhooks_job_id_delete**](PublicApi.md#orgs_org_id_apps_app_id_webhooks_job_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Delete a Webhook
[**orgs_org_id_apps_app_id_webhooks_job_id_get**](PublicApi.md#orgs_org_id_apps_app_id_webhooks_job_id_get) | **GET** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Get a Webhook
[**orgs_org_id_apps_app_id_webhooks_job_id_patch**](PublicApi.md#orgs_org_id_apps_app_id_webhooks_job_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Update a Webhook
[**orgs_org_id_apps_app_id_webhooks_post**](PublicApi.md#orgs_org_id_apps_app_id_webhooks_post) | **POST** /orgs/{orgId}/apps/{appId}/webhooks | Create a new Webhook
[**orgs_org_id_apps_get**](PublicApi.md#orgs_org_id_apps_get) | **GET** /orgs/{orgId}/apps | List all Applications in an Organization.
[**orgs_org_id_apps_post**](PublicApi.md#orgs_org_id_apps_post) | **POST** /orgs/{orgId}/apps | Add a new Application to an Organization
[**orgs_org_id_artefact_versions_artefact_version_id_get**](PublicApi.md#orgs_org_id_artefact_versions_artefact_version_id_get) | **GET** /orgs/{orgId}/artefact-versions/{artefactVersionId} | Get an Artefacts Versions.
[**orgs_org_id_artefact_versions_get**](PublicApi.md#orgs_org_id_artefact_versions_get) | **GET** /orgs/{orgId}/artefact-versions | List all Artefacts Versions.
[**orgs_org_id_artefact_versions_post**](PublicApi.md#orgs_org_id_artefact_versions_post) | **POST** /orgs/{orgId}/artefact-versions | Register a new Artefact Version with your organization.
[**orgs_org_id_artefacts_artefact_id_delete**](PublicApi.md#orgs_org_id_artefacts_artefact_id_delete) | **DELETE** /orgs/{orgId}/artefacts/{artefactId} | Delete Artefact and all related Artefact Versions
[**orgs_org_id_artefacts_artefact_id_versions_get**](PublicApi.md#orgs_org_id_artefacts_artefact_id_versions_get) | **GET** /orgs/{orgId}/artefacts/{artefactId}/versions | List all Artefact Versions of an Artefact.
[**orgs_org_id_artefacts_artefact_id_versions_version_id_patch**](PublicApi.md#orgs_org_id_artefacts_artefact_id_versions_version_id_patch) | **PATCH** /orgs/{orgId}/artefacts/{artefactId}/versions/{versionId} | Update Version of an Artefact.
[**orgs_org_id_artefacts_get**](PublicApi.md#orgs_org_id_artefacts_get) | **GET** /orgs/{orgId}/artefacts | List all Artefacts.
[**orgs_org_id_env_types_env_type_id_delete**](PublicApi.md#orgs_org_id_env_types_env_type_id_delete) | **DELETE** /orgs/{orgId}/env-types/{envTypeId} | Deletes an Environment Type
[**orgs_org_id_env_types_env_type_id_get**](PublicApi.md#orgs_org_id_env_types_env_type_id_get) | **GET** /orgs/{orgId}/env-types/{envTypeId} | Get an Environment Type
[**orgs_org_id_env_types_env_type_users_post**](PublicApi.md#orgs_org_id_env_types_env_type_users_post) | **POST** /orgs/{orgId}/env-types/{envType}/users | Adds a User to an Environment Type with a Role
[**orgs_org_id_env_types_env_type_users_user_id_delete**](PublicApi.md#orgs_org_id_env_types_env_type_users_user_id_delete) | **DELETE** /orgs/{orgId}/env-types/{envType}/users/{userId} | Remove the role of a User on an Environment Type
[**orgs_org_id_env_types_env_type_users_user_id_get**](PublicApi.md#orgs_org_id_env_types_env_type_users_user_id_get) | **GET** /orgs/{orgId}/env-types/{envType}/users/{userId} | Get the role of a User on an Environment Type
[**orgs_org_id_env_types_env_type_users_user_id_patch**](PublicApi.md#orgs_org_id_env_types_env_type_users_user_id_patch) | **PATCH** /orgs/{orgId}/env-types/{envType}/users/{userId} | Update the role of a User on an Environment Type
[**orgs_org_id_env_types_get**](PublicApi.md#orgs_org_id_env_types_get) | **GET** /orgs/{orgId}/env-types | List all Environment Types
[**orgs_org_id_env_types_post**](PublicApi.md#orgs_org_id_env_types_post) | **POST** /orgs/{orgId}/env-types | Add a new Environment Type
[**orgs_org_id_events_get**](PublicApi.md#orgs_org_id_events_get) | **GET** /orgs/{orgId}/events | List Events
[**orgs_org_id_get**](PublicApi.md#orgs_org_id_get) | **GET** /orgs/{orgId} | Get the specified Organization.
[**orgs_org_id_images_get**](PublicApi.md#orgs_org_id_images_get) | **GET** /orgs/{orgId}/images | List all Container Images
[**orgs_org_id_images_image_id_builds_get**](PublicApi.md#orgs_org_id_images_image_id_builds_get) | **GET** /orgs/{orgId}/images/{imageId}/builds | Lists all the Builds of an Image
[**orgs_org_id_images_image_id_builds_post**](PublicApi.md#orgs_org_id_images_image_id_builds_post) | **POST** /orgs/{orgId}/images/{imageId}/builds | Add a new Image Build
[**orgs_org_id_images_image_id_get**](PublicApi.md#orgs_org_id_images_image_id_get) | **GET** /orgs/{orgId}/images/{imageId} | Get a specific Image Object
[**orgs_org_id_invitations_get**](PublicApi.md#orgs_org_id_invitations_get) | **GET** /orgs/{orgId}/invitations | List the invites issued for the organization.
[**orgs_org_id_invitations_post**](PublicApi.md#orgs_org_id_invitations_post) | **POST** /orgs/{orgId}/invitations | Invites a user to an Organization with a specified role.
[**orgs_org_id_registries_get**](PublicApi.md#orgs_org_id_registries_get) | **GET** /orgs/{orgId}/registries | Lists available registries for the organization.
[**orgs_org_id_registries_post**](PublicApi.md#orgs_org_id_registries_post) | **POST** /orgs/{orgId}/registries | Creates a new registry record.
[**orgs_org_id_registries_reg_id_creds_get**](PublicApi.md#orgs_org_id_registries_reg_id_creds_get) | **GET** /orgs/{orgId}/registries/{regId}/creds | Returns current account credentials or secret details for the registry.
[**orgs_org_id_registries_reg_id_delete**](PublicApi.md#orgs_org_id_registries_reg_id_delete) | **DELETE** /orgs/{orgId}/registries/{regId} | Deletes an existing registry record and all associated credentials and secrets.
[**orgs_org_id_registries_reg_id_get**](PublicApi.md#orgs_org_id_registries_reg_id_get) | **GET** /orgs/{orgId}/registries/{regId} | Loads a registry record details.
[**orgs_org_id_registries_reg_id_patch**](PublicApi.md#orgs_org_id_registries_reg_id_patch) | **PATCH** /orgs/{orgId}/registries/{regId} | Updates (patches) an existing registry record.
[**orgs_org_id_resources_account_types_get**](PublicApi.md#orgs_org_id_resources_account_types_get) | **GET** /orgs/{orgId}/resources/account-types | List Resource Account Types available to the organization.
[**orgs_org_id_resources_accounts_acc_id_delete**](PublicApi.md#orgs_org_id_resources_accounts_acc_id_delete) | **DELETE** /orgs/{orgId}/resources/accounts/{accId} | Delete an unused Resource Account.
[**orgs_org_id_resources_accounts_acc_id_get**](PublicApi.md#orgs_org_id_resources_accounts_acc_id_get) | **GET** /orgs/{orgId}/resources/accounts/{accId} | Get a Resource Account.
[**orgs_org_id_resources_accounts_acc_id_patch**](PublicApi.md#orgs_org_id_resources_accounts_acc_id_patch) | **PATCH** /orgs/{orgId}/resources/accounts/{accId} | Update a Resource Account.
[**orgs_org_id_resources_accounts_get**](PublicApi.md#orgs_org_id_resources_accounts_get) | **GET** /orgs/{orgId}/resources/accounts | List Resource Accounts in the organization.
[**orgs_org_id_resources_accounts_post**](PublicApi.md#orgs_org_id_resources_accounts_post) | **POST** /orgs/{orgId}/resources/accounts | Create a new Resource Account in the organization.
[**orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**](PublicApi.md#orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete) | **DELETE** /orgs/{orgId}/resources/defs/{defId}/criteria/{criteriaId} | Delete a Matching Criteria from a Resource Definition.
[**orgs_org_id_resources_defs_def_id_criteria_post**](PublicApi.md#orgs_org_id_resources_defs_def_id_criteria_post) | **POST** /orgs/{orgId}/resources/defs/{defId}/criteria | Add a new Matching Criteria to a Resource Definition.
[**orgs_org_id_resources_defs_def_id_delete**](PublicApi.md#orgs_org_id_resources_defs_def_id_delete) | **DELETE** /orgs/{orgId}/resources/defs/{defId} | Delete a Resource Definition.
[**orgs_org_id_resources_defs_def_id_get**](PublicApi.md#orgs_org_id_resources_defs_def_id_get) | **GET** /orgs/{orgId}/resources/defs/{defId} | Get a specific Resource Definition.
[**orgs_org_id_resources_defs_def_id_patch**](PublicApi.md#orgs_org_id_resources_defs_def_id_patch) | **PATCH** /orgs/{orgId}/resources/defs/{defId} | Update a Resource Definition.
[**orgs_org_id_resources_defs_def_id_put**](PublicApi.md#orgs_org_id_resources_defs_def_id_put) | **PUT** /orgs/{orgId}/resources/defs/{defId} | Update a Resource Definition.
[**orgs_org_id_resources_defs_def_id_resources_get**](PublicApi.md#orgs_org_id_resources_defs_def_id_resources_get) | **GET** /orgs/{orgId}/resources/defs/{defId}/resources | List Active Resources provisioned via a specific Resource Definition.
[**orgs_org_id_resources_defs_get**](PublicApi.md#orgs_org_id_resources_defs_get) | **GET** /orgs/{orgId}/resources/defs | List Resource Definitions.
[**orgs_org_id_resources_defs_post**](PublicApi.md#orgs_org_id_resources_defs_post) | **POST** /orgs/{orgId}/resources/defs | Create a new Resource Definition.
[**orgs_org_id_resources_drivers_driver_id_delete**](PublicApi.md#orgs_org_id_resources_drivers_driver_id_delete) | **DELETE** /orgs/{orgId}/resources/drivers/{driverId} | Delete a Resources Driver.
[**orgs_org_id_resources_drivers_driver_id_get**](PublicApi.md#orgs_org_id_resources_drivers_driver_id_get) | **GET** /orgs/{orgId}/resources/drivers/{driverId} | Get a Resource Driver.
[**orgs_org_id_resources_drivers_driver_id_put**](PublicApi.md#orgs_org_id_resources_drivers_driver_id_put) | **PUT** /orgs/{orgId}/resources/drivers/{driverId} | Update a Resource Driver.
[**orgs_org_id_resources_drivers_get**](PublicApi.md#orgs_org_id_resources_drivers_get) | **GET** /orgs/{orgId}/resources/drivers | List Resource Drivers.
[**orgs_org_id_resources_drivers_post**](PublicApi.md#orgs_org_id_resources_drivers_post) | **POST** /orgs/{orgId}/resources/drivers | Register a new Resource Driver.
[**orgs_org_id_resources_types_get**](PublicApi.md#orgs_org_id_resources_types_get) | **GET** /orgs/{orgId}/resources/types | List Resource Types.
[**orgs_org_id_secretstores_get**](PublicApi.md#orgs_org_id_secretstores_get) | **GET** /orgs/{orgId}/secretstores | Get list of Secret Stores for the given organization.
[**orgs_org_id_secretstores_post**](PublicApi.md#orgs_org_id_secretstores_post) | **POST** /orgs/{orgId}/secretstores | Create a Secret Store for the given organization.
[**orgs_org_id_secretstores_store_id_delete**](PublicApi.md#orgs_org_id_secretstores_store_id_delete) | **DELETE** /orgs/{orgId}/secretstores/{storeId} | Delete the Secret Store.
[**orgs_org_id_secretstores_store_id_get**](PublicApi.md#orgs_org_id_secretstores_store_id_get) | **GET** /orgs/{orgId}/secretstores/{storeId} | Get the Secret Store.
[**orgs_org_id_secretstores_store_id_patch**](PublicApi.md#orgs_org_id_secretstores_store_id_patch) | **PATCH** /orgs/{orgId}/secretstores/{storeId} | Update the Secret Store.
[**orgs_org_id_users_get**](PublicApi.md#orgs_org_id_users_get) | **GET** /orgs/{orgId}/users | List Users with roles in an Organization
[**orgs_org_id_users_post**](PublicApi.md#orgs_org_id_users_post) | **POST** /orgs/{orgId}/users | Creates a new service user.
[**orgs_org_id_users_user_id_delete**](PublicApi.md#orgs_org_id_users_user_id_delete) | **DELETE** /orgs/{orgId}/users/{userId} | Remove the role of a User on an Organization
[**orgs_org_id_users_user_id_get**](PublicApi.md#orgs_org_id_users_user_id_get) | **GET** /orgs/{orgId}/users/{userId} | Get the role of a User on an Organization
[**orgs_org_id_users_user_id_patch**](PublicApi.md#orgs_org_id_users_user_id_patch) | **PATCH** /orgs/{orgId}/users/{userId} | Update the role of a User on an Organization
[**orgs_org_id_workload_profiles_get**](PublicApi.md#orgs_org_id_workload_profiles_get) | **GET** /orgs/{orgId}/workload-profiles | List workload profiles available to the organization.
[**orgs_org_id_workload_profiles_post**](PublicApi.md#orgs_org_id_workload_profiles_post) | **POST** /orgs/{orgId}/workload-profiles | Create new Workload Profile
[**orgs_org_id_workload_profiles_profile_id_versions_version_delete**](PublicApi.md#orgs_org_id_workload_profiles_profile_id_versions_version_delete) | **DELETE** /orgs/{orgId}/workload-profiles/{profileId}/versions/{version} | Delete a Workload Profile Version
[**orgs_org_id_workload_profiles_profile_qid_delete**](PublicApi.md#orgs_org_id_workload_profiles_profile_qid_delete) | **DELETE** /orgs/{orgId}/workload-profiles/{profileQid} | Delete a Workload Profile
[**orgs_org_id_workload_profiles_profile_qid_get**](PublicApi.md#orgs_org_id_workload_profiles_profile_qid_get) | **GET** /orgs/{orgId}/workload-profiles/{profileQid} | Get a Workload Profile
[**put_delta**](PublicApi.md#put_delta) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Update an existing Delta
[**restart_run**](PublicApi.md#restart_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/restart | Restart a Run within an Pipeline by cloning it with the same trigger and inputs.
[**tokens_get**](PublicApi.md#tokens_get) | **GET** /tokens | DEPRECATED
[**tokens_token_id_delete**](PublicApi.md#tokens_token_id_delete) | **DELETE** /tokens/{tokenId} | DEPRECATED
[**update_pipeline**](PublicApi.md#update_pipeline) | **PATCH** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | update a Pipeline within an Application.
[**users_me_get**](PublicApi.md#users_me_get) | **GET** /users/me | DEPRECATED
[**users_user_id_tokens_get**](PublicApi.md#users_user_id_tokens_get) | **GET** /users/{userId}/tokens | Lists tokens associated with a user
[**users_user_id_tokens_post**](PublicApi.md#users_user_id_tokens_post) | **POST** /users/{userId}/tokens | Creates a new static token for a user.
[**users_user_id_tokens_token_id_delete**](PublicApi.md#users_user_id_tokens_token_id_delete) | **DELETE** /users/{userId}/tokens/{tokenId} | Deletes a specific token associated with a user
[**users_user_id_tokens_token_id_get**](PublicApi.md#users_user_id_tokens_token_id_get) | **GET** /users/{userId}/tokens/{tokenId} | Gets a specific token associated with a user

# **approve_approval_request**
> ApprovalRequest approve_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Approve the approval request

Approve the approval requested.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Approve the approval request
    api_response = api_instance.approve_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->approve_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cancel_run**
> cancel_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)

Cancel a Run within an Pipeline.

Attempts to cancel the specified Run. If the Run is in a queued state, this cancellation will be applied immediately. If the Run is executing, the cancellation will be stored and will be resolved by the next Job or Step that supports in-flight cancellation. Runs that are in any other state, are not cancellable. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Cancel a Run within an Pipeline.
    api_instance.cancel_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PublicApi->cancel_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_pipeline**
> Pipeline create_pipeline(body, org_id, app_id)

Create a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = NULL # object | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID

try:
    # Create a Pipeline within an Application.
    api_response = api_instance.create_pipeline(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->create_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**object**](object.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-yaml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_pipeline_run**
> RunResponse create_pipeline_run(body, org_id, app_id, pipeline_id, idempotency_key=idempotency_key)

Create a run within a pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RunCreateRequest() # RunCreateRequest | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
idempotency_key = 'idempotency_key_example' # str | The HTTP Idempotency-Key (optional)

try:
    # Create a run within a pipeline.
    api_response = api_instance.create_pipeline_run(body, org_id, app_id, pipeline_id, idempotency_key=idempotency_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->create_pipeline_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RunCreateRequest**](RunCreateRequest.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **idempotency_key** | **str**| The HTTP Idempotency-Key | [optional] 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_public_key**
> PublicKey create_public_key(body, org_id)

Associate a new RSA public key to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = '\"-----BEGIN PUBLIC KEY-----\\\\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAo22jj/h+WPhL5M8RUyqC\\\\nEqRK3FOYD6KuDTyUHZL2QUX/q35bT1aGIOtu+22oM/8hTZ/6+BTknvF+Z+e7At8E\\\\n/I6/qNZilJFH/HqNHYflykceILVCFQM6v0ar6CSPh88DjwQPGLrh+UYp0NKBF6D5\\\\n2LEVQpxsM/0qV4fOZngRVI9UeKiYehk8aXJi20nI5Cj9GnV7BuEo7sKq7NewTOMN\\\\nwHqSnCtMV+E7SIgyy4+aKAFHOR0Y4FgJN14bAjE2GX/VUmAYBNtPgGwkCNDbA9v1\\\\nJJnFmdKflp8foqeWwC9UcUDLqjZzMT2a16pnL89iHV4fJ4vI6h26Jf2wUJbb7xE3\\\\nxJNhCR9Qf2IHx8TTVetIRl5I/ZUPkRrq5iskOIWV4+I+NzS+gbnCUv6sCO3cnZaa\\\\noZ4Wk5V+5qfctANeTn0TEAabYAJ/zFgYcs2IwYNqcN5eTss99zGNgtDL3oJr/A6g\\\\neGBgzad3p80qPMb1le64cHAAFZaerI2kcdnNpWGLXJ/J7IKF9uNfsrvi7Zdv8AXg\\\\nVAyqBARfzSkgnUKN6iWkWxAAzylKnQ3Etw81huaLwDG/6Lqd8I5OePMwHEZkollD\\\\nEBD16L/iyV9veru2zLVXkX/nL64YtZERLOWI5NUMaSurAR4N3ptFAsDRcO5Z4+Dq\\\\nO3pqszSh1aCyJvyl6cjQLT8CAwEAAQ==\\\\n-----END PUBLIC KEY-----\\\\n\"' # str | A pcks8 RSA public key PEM encoded (as the ones produced by openssl), whose module length is greater or equal than 4096 bits. It should be provided as a single line. This might be accomplished through the usage of `awk` bash tool: `awk -v ORS='\n' '1' public_key.pem`
org_id = 'org_id_example' # str | The organization ID.

try:
    # Associate a new RSA public key to an organization.
    api_response = api_instance.create_public_key(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->create_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| A pcks8 RSA public key PEM encoded (as the ones produced by openssl), whose module length is greater or equal than 4096 bits. It should be provided as a single line. This might be accomplished through the usage of &#x60;awk&#x60; bash tool: &#x60;awk -v ORS&#x3D;&#x27;\n&#x27; &#x27;1&#x27; public_key.pem&#x60; | 
 **org_id** | **str**| The organization ID. | 

### Return type

[**PublicKey**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_workload_profile_chart_version**
> WorkloadProfileChartVersionResponse create_workload_profile_chart_version(file, org_id)

Add new Workload Profile Chart Version

Creates a Workload Profile Chart Version from the uploaded Helm chart. The name and version is retrieved from the chart's metadata (Charts.yaml file).  The request has content type `multipart/form-data` and the request body includes one part:  1. `file` with `application/x-gzip` content type which is an archive containing a Helm chart.  Request body example:   Content-Type: multipart/form-data; boundary=----boundary  ----boundary  Content-Disposition: form-data; name=\"file\"; filename=\"my-workload-1.0.1.tgz\"  Content-Type: application/x-gzip  [TGZ_DATA]  ----boundary

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
file = 'file_example' # str | 
org_id = 'org_id_example' # str | The Organization ID

try:
    # Add new Workload Profile Chart Version
    api_response = api_instance.create_workload_profile_chart_version(file, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->create_workload_profile_chart_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **str**|  | 
 **org_id** | **str**| The Organization ID | 

### Return type

[**WorkloadProfileChartVersionResponse**](WorkloadProfileChartVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_workload_profile_version**
> WorkloadProfileVersionResponse create_workload_profile_version(body, org_id, profile_qid)

Add new Version of the Workload Profile

Creates a Workload Profile Version for the given Workload Profile.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.WorkloadProfileVersionRequest() # WorkloadProfileVersionRequest | Workload profile version metadata.


org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.

try:
    # Add new Version of the Workload Profile
    api_response = api_instance.create_workload_profile_version(body, org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->create_workload_profile_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WorkloadProfileVersionRequest**](WorkloadProfileVersionRequest.md)| Workload profile version metadata.

 | 
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 

### Return type

[**WorkloadProfileVersionResponse**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **current_user_get**
> UserProfileExtendedResponse current_user_get()

Gets the extended profile of the current user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()

try:
    # Gets the extended profile of the current user
    api_response = api_instance.current_user_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->current_user_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserProfileExtendedResponse**](UserProfileExtendedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **current_user_patch**
> UserProfileExtendedResponse current_user_patch(body)

Updates the extended profile of the current user.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UserProfileExtendedRequest() # UserProfileExtendedRequest | 

try:
    # Updates the extended profile of the current user.
    api_response = api_instance.current_user_patch(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->current_user_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserProfileExtendedRequest**](UserProfileExtendedRequest.md)|  | 

### Return type

[**UserProfileExtendedResponse**](UserProfileExtendedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_pipeline**
> delete_pipeline(org_id, app_id, pipeline_id, if_match=if_match)

Delete a pipeline within an application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Delete a pipeline within an application.
    api_instance.delete_pipeline(org_id, app_id, pipeline_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PublicApi->delete_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_public_key**
> delete_public_key(org_id, key_id)

Delete a public key associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.
key_id = 'key_id_example' # str | The public key ID.

try:
    # Delete a public key associated to an organization.
    api_instance.delete_public_key(org_id, key_id)
except ApiException as e:
    print("Exception when calling PublicApi->delete_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **key_id** | **str**| The public key ID. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_run**
> delete_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)

Deleting a completed Run within a Pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Deleting a completed Run within a Pipeline.
    api_instance.delete_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PublicApi->delete_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deny_approval_request**
> ApprovalRequest deny_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Deny the approval request

Deny the approval requested.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Deny the approval request
    api_response = api_instance.deny_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->deny_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_approval_request**
> ApprovalRequest get_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Get an approval request

Get an approval request.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Get an approval request
    api_response = api_instance.get_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delta**
> DeltaResponse get_delta(org_id, app_id, delta_id)

Fetch an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to fetch.  

try:
    # Fetch an existing Delta
    api_response = api_instance.get_delta(org_id, app_id, delta_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_delta: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to fetch.   | 

### Return type

[**DeltaResponse**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_humanitec_public_keys**
> list[HumanitecPublicKey] get_humanitec_public_keys(org_id, active=active)

List all the public keys Humanitec shares with an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.
active = true # bool | If set to true, the response includes only the active key, if set to false only non-active keys, otherwise both active and non-active keys. (optional)

try:
    # List all the public keys Humanitec shares with an organization.
    api_response = api_instance.get_humanitec_public_keys(org_id, active=active)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_humanitec_public_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **active** | **bool**| If set to true, the response includes only the active key, if set to false only non-active keys, otherwise both active and non-active keys. | [optional] 

### Return type

[**list[HumanitecPublicKey]**](HumanitecPublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_latest_pipeline_definition_schema**
> object get_latest_pipeline_definition_schema(org_id)

Gets the latest pipeline schema

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID

try:
    # Gets the latest pipeline schema
    api_response = api_instance.get_latest_pipeline_definition_schema(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_latest_pipeline_definition_schema: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_latest_workload_profile_version**
> WorkloadProfileVersionResponse get_latest_workload_profile_version(org_id, profile_qid)

Latest version of the given workload profile with optional constraint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.

try:
    # Latest version of the given workload profile with optional constraint.
    api_response = api_instance.get_latest_workload_profile_version(org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_latest_workload_profile_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 

### Return type

[**WorkloadProfileVersionResponse**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline**
> Pipeline get_pipeline(org_id, app_id, pipeline_id, version=version)

Get a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
version = 'version_example' # str | An optional Pipeline Version ID. (optional)

try:
    # Get a Pipeline within an Application.
    api_response = api_instance.get_pipeline(org_id, app_id, pipeline_id, version=version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **version** | **str**| An optional Pipeline Version ID. | [optional] 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_definition**
> object get_pipeline_definition(org_id, app_id, pipeline_id, version=version, accept=accept)

Get a pipeline schema.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
version = 'version_example' # str | An optional Pipeline Version ID. (optional)
accept = 'accept_example' # str |  (optional)

try:
    # Get a pipeline schema.
    api_response = api_instance.get_pipeline_definition(org_id, app_id, pipeline_id, version=version, accept=accept)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_pipeline_definition: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **version** | **str**| An optional Pipeline Version ID. | [optional] 
 **accept** | **str**|  | [optional] 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x.humanitec-pipelines-v1.0+yaml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_key**
> PublicKey get_public_key(org_id, key_id)

Retrieve a single public key associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.
key_id = 'key_id_example' # str | The public key ID.

try:
    # Retrieve a single public key associated to an organization.
    api_response = api_instance.get_public_key(org_id, key_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **key_id** | **str**| The public key ID. | 

### Return type

[**PublicKey**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_keys**
> list[PublicKey] get_public_keys(org_id, fingerprint=fingerprint)

List all public keys associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.
fingerprint = 'fingerprint_example' # str | The fingerprint of the requested key. If a value is provided, the result will contain a single key, if any. (optional)

try:
    # List all public keys associated to an organization.
    api_response = api_instance.get_public_keys(org_id, fingerprint=fingerprint)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_public_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **fingerprint** | **str**| The fingerprint of the requested key. If a value is provided, the result will contain a single key, if any. | [optional] 

### Return type

[**list[PublicKey]**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_run**
> RunResponse get_run(org_id, app_id, pipeline_id, run_id)

Get a run within an pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID

try:
    # Get a run within an pipeline.
    api_response = api_instance.get_run(org_id, app_id, pipeline_id, run_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_run_job**
> RunJobResponse get_run_job(org_id, app_id, pipeline_id, run_id, job_id)

List the details of a Job including Step information

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID

try:
    # List the details of a Job including Step information
    api_response = api_instance.get_run_job(org_id, app_id, pipeline_id, run_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_run_job: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 

### Return type

[**RunJobResponse**](RunJobResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_sets**
> list[SetResponse] get_sets(org_id, app_id)

Get all Deployment Sets

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Get all Deployment Sets
    api_response = api_instance.get_sets(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->get_sets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[SetResponse]**](SetResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_approval_requests**
> list[ApprovalRequest] list_approval_requests(org_id, app_id, per_page=per_page, page=page, pipeline=pipeline, run=run, status=status)

List of the approval requests

List of the approval requests with in an app. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
pipeline = ['pipeline_example'] # list[str] | An optional list of Pipeline IDs. (optional)
run = ['run_example'] # list[str] | An optional Pipeline Run ID. (optional)
status = 'status_example' # str | Optional filter by status. (optional)

try:
    # List of the approval requests
    api_response = api_instance.list_approval_requests(org_id, app_id, per_page=per_page, page=page, pipeline=pipeline, run=run, status=status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_approval_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **pipeline** | [**list[str]**](str.md)| An optional list of Pipeline IDs. | [optional] 
 **run** | [**list[str]**](str.md)| An optional Pipeline Run ID. | [optional] 
 **status** | **str**| Optional filter by status. | [optional] 

### Return type

[**list[ApprovalRequest]**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_runs**
> list[RunResponse] list_pipeline_runs(org_id, app_id, pipeline_id, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)

List runs within a pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
status = ['status_example'] # list[str] | Optional filter by status. (optional)
completed = true # bool | Optional filer by completed or not. (optional)
created_after = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation after date time. (optional)
created_before = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation before date time (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List runs within a pipeline.
    api_response = api_instance.list_pipeline_runs(org_id, app_id, pipeline_id, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_pipeline_runs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **completed** | **bool**| Optional filer by completed or not. | [optional] 
 **created_after** | **datetime**| Optional filter by creation after date time. | [optional] 
 **created_before** | **datetime**| Optional filter by creation before date time | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunResponse]**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_runs_by_org**
> list[RunResponse] list_pipeline_runs_by_org(org_id, app=app, pipeline=pipeline, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)

List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app = ['app_example'] # list[str] | An optional list of Application IDs. (optional)
pipeline = ['pipeline_example'] # list[str] | An optional list of Pipeline IDs. (optional)
status = ['status_example'] # list[str] | Optional filter by status. (optional)
completed = true # bool | Optional filer by completed or not. (optional)
created_after = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation after date time. (optional)
created_before = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation before date time (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.
    api_response = api_instance.list_pipeline_runs_by_org(org_id, app=app, pipeline=pipeline, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_pipeline_runs_by_org: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app** | [**list[str]**](str.md)| An optional list of Application IDs. | [optional] 
 **pipeline** | [**list[str]**](str.md)| An optional list of Pipeline IDs. | [optional] 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **completed** | **bool**| Optional filer by completed or not. | [optional] 
 **created_after** | **datetime**| Optional filter by creation after date time. | [optional] 
 **created_before** | **datetime**| Optional filter by creation before date time | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunResponse]**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_versions**
> list[PipelineVersion] list_pipeline_versions(org_id, app_id, pipeline_id, per_page=per_page, page=page)

List all versions of the pipeline

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List all versions of the pipeline
    api_response = api_instance.list_pipeline_versions(org_id, app_id, pipeline_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_pipeline_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[PipelineVersion]**](PipelineVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipelines**
> list[Pipeline] list_pipelines(org_id, app_id, per_page=per_page, page=page, trigger=trigger)

List Pipelines within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
trigger = 'trigger_example' # str | An optional filter by trigger type. (optional)

try:
    # List Pipelines within an Application.
    api_response = api_instance.list_pipelines(org_id, app_id, per_page=per_page, page=page, trigger=trigger)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_pipelines: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **trigger** | **str**| An optional filter by trigger type. | [optional] 

### Return type

[**list[Pipeline]**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipelines_in_org**
> list[Pipeline] list_pipelines_in_org(org_id, app=app, per_page=per_page, page=page, trigger=trigger)

List all Pipelines within an Organization. This can be filtered by Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app = ['app_example'] # list[str] | An optional list of Application IDs. (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
trigger = 'trigger_example' # str | An optional filter by trigger type. (optional)

try:
    # List all Pipelines within an Organization. This can be filtered by Application.
    api_response = api_instance.list_pipelines_in_org(org_id, app=app, per_page=per_page, page=page, trigger=trigger)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_pipelines_in_org: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app** | [**list[str]**](str.md)| An optional list of Application IDs. | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **trigger** | **str**| An optional filter by trigger type. | [optional] 

### Return type

[**list[Pipeline]**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_run_job_step_logs**
> list[RunJobStepLog] list_run_job_step_logs(org_id, app_id, pipeline_id, run_id, job_id, step_index, page=page)

Get a page of log output for a given step within a job.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
step_index = 56 # int | The index of the step within the Job
page = 'page_example' # str | The page token to request from (optional)

try:
    # Get a page of log output for a given step within a job.
    api_response = api_instance.list_run_job_step_logs(org_id, app_id, pipeline_id, run_id, job_id, step_index, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_run_job_step_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **step_index** | **int**| The index of the step within the Job | 
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunJobStepLog]**](RunJobStepLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_run_jobs**
> list[RunJobListResponse] list_run_jobs(org_id, app_id, pipeline_id, run_id, status=status, per_page=per_page, page=page)

List the details of the jobs with in a pipeline run.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
status = ['status_example'] # list[str] | Optional filter by status. (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List the details of the jobs with in a pipeline run.
    api_response = api_instance.list_run_jobs(org_id, app_id, pipeline_id, run_id, status=status, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_run_jobs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunJobListResponse]**](RunJobListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_workload_profile_chart_versions**
> list[WorkloadProfileChartVersionResponse] list_workload_profile_chart_versions(org_id, per_page=per_page, page=page)

Workload Profile Chart Versions for the given organization.

Returns all Workload Profile Chart Versions for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # Workload Profile Chart Versions for the given organization.
    api_response = api_instance.list_workload_profile_chart_versions(org_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_workload_profile_chart_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileChartVersionResponse]**](WorkloadProfileChartVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_workload_profile_versions**
> list[WorkloadProfileVersionResponse] list_workload_profile_versions(org_id, profile_qid, per_page=per_page, page=page)

List versions of the given workload profile.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List versions of the given workload profile.
    api_response = api_instance.list_workload_profile_versions(org_id, profile_qid, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->list_workload_profile_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileVersionResponse]**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_get**
> list[OrganizationResponse] orgs_get()

List active organizations the user has access to.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()

try:
    # List active organizations the user has access to.
    api_response = api_instance.orgs_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[OrganizationResponse]**](OrganizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_delete**
> orgs_org_id_apps_app_id_delete(org_id, app_id)

Delete an Application

Deleting an Application will also delete everything associated with it. This includes Environments, Deployment history on those Environments, and any shared values and secrets associated.  _Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Delete an Application
    api_instance.orgs_org_id_apps_app_id_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put(body, org_id, app_id, delta_id)

Mark a Delta as \"archived\"

Archived Deltas are still accessible but can no longer be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = true # bool | Either `true` or `false`.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Mark a Delta as \"archived\"
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**bool**](bool.md)| Either &#x60;true&#x60; or &#x60;false&#x60;.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put(body, org_id, app_id, delta_id)

Change the Environment of a Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = '\"new-env\"' # str | The new Environment ID. (NOTE: The string must still be JSON encoded.)


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Change the Environment of a Delta
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The new Environment ID. (NOTE: The string must still be JSON encoded.)

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put(body, org_id, app_id, delta_id)

Change the name of a Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = '\"Update for ticket #s 2568 & 2572\"' # str | The new name.(NOTE: The string must still be JSON encoded.)


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Change the name of a Delta
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The new name.(NOTE: The string must still be JSON encoded.)

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_patch**
> DeltaResponse orgs_org_id_apps_app_id_deltas_delta_id_patch(body, org_id, app_id, delta_id)

Update an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = [swagger_client.DeltaRequest()] # list[DeltaRequest] | An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the `modules.add` property are replaced with the new Delta's values. If the value of a Module is `null`, and the ID is in the `modules.remove` list, it is removed from the `modules.remove` list.

* All IDs listed in `modules.remove` are combined. Any ID in `modules.remove` and also in `modules.add` are removed from `modules.add`

* The lists of JSON Patches in `modules.update` are concatenated or created in `modules.updates`.

Simplification involves:

* Applying any entries in `modules.updates` that have matching IDs in `modules.add` to the `modules.add` entry and removing the `modules.update` entry.

* Reducing the number of JSON Patches in each `modules.update` entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the `value` of the `remove` action can be set to `{"scope": "delta"}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the `id` or `metadata` properties are specified, they will be ignored._
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to update.  

try:
    # Update an existing Delta
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_delta_id_patch(body, org_id, app_id, delta_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_delta_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[DeltaRequest]**](DeltaRequest.md)| An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the &#x60;modules.add&#x60; property are replaced with the new Delta&#x27;s values. If the value of a Module is &#x60;null&#x60;, and the ID is in the &#x60;modules.remove&#x60; list, it is removed from the &#x60;modules.remove&#x60; list.

* All IDs listed in &#x60;modules.remove&#x60; are combined. Any ID in &#x60;modules.remove&#x60; and also in &#x60;modules.add&#x60; are removed from &#x60;modules.add&#x60;

* The lists of JSON Patches in &#x60;modules.update&#x60; are concatenated or created in &#x60;modules.updates&#x60;.

Simplification involves:

* Applying any entries in &#x60;modules.updates&#x60; that have matching IDs in &#x60;modules.add&#x60; to the &#x60;modules.add&#x60; entry and removing the &#x60;modules.update&#x60; entry.

* Reducing the number of JSON Patches in each &#x60;modules.update&#x60; entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the &#x60;value&#x60; of the &#x60;remove&#x60; action can be set to &#x60;{&quot;scope&quot;: &quot;delta&quot;}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the &#x60;id&#x60; or &#x60;metadata&#x60; properties are specified, they will be ignored._ | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to update.   | 

### Return type

[**DeltaResponse**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_get**
> list[DeltaResponse] orgs_org_id_apps_app_id_deltas_get(org_id, app_id, archived=archived, env=env)

List Deltas in an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
archived = true # bool | If true, return archived Deltas.   (optional)
env = 'env_example' # str | Only return Deltas associated with the specified Environment.   (optional)

try:
    # List Deltas in an Application
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_get(org_id, app_id, archived=archived, env=env)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **archived** | **bool**| If true, return archived Deltas.   | [optional] 
 **env** | **str**| Only return Deltas associated with the specified Environment.   | [optional] 

### Return type

[**list[DeltaResponse]**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_post**
> InlineResponse200 orgs_org_id_apps_app_id_deltas_post(body, org_id, app_id)

Create a new Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.DeltaRequest() # DeltaRequest | A Deployment Delta to create.

The Deployment Delta will be added with the provided content of `modules` and the 'env_id' and 'name' properties of the 'metadata' property.

NOTE: If the `id` property is specified, it will be ignored. A new ID will be generated and returned in the response.
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a new Delta
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_deltas_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| A Deployment Delta to create.

The Deployment Delta will be added with the provided content of &#x60;modules&#x60; and the &#x27;env_id&#x27; and &#x27;name&#x27; properties of the &#x27;metadata&#x27; property.

NOTE: If the &#x60;id&#x60; property is specified, it will be ignored. A new ID will be generated and returned in the response. | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_delete(org_id, app_id, env_id)

Delete a specific Environment.

Deletes a specific Environment in an Application.  Deleting an Environment will also delete the Deployment history of the Environment.  _Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Delete a specific Environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_delete(org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get**
> list[DeploymentErrorResponse] orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get(org_id, app_id, env_id, deploy_id)

List errors that occurred in a Deployment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
deploy_id = 'deploy_id_example' # str | The Deployment ID.  

try:
    # List errors that occurred in a Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get(org_id, app_id, env_id, deploy_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **deploy_id** | **str**| The Deployment ID.   | 

### Return type

[**list[DeploymentErrorResponse]**](DeploymentErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get**
> DeploymentResponse orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get(org_id, app_id, env_id, deploy_id)

Get a specific Deployment.

Gets a specific Deployment in an Application and an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
deploy_id = 'deploy_id_example' # str | The Deployment ID.  

try:
    # Get a specific Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get(org_id, app_id, env_id, deploy_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **deploy_id** | **str**| The Deployment ID.   | 

### Return type

[**DeploymentResponse**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_get**
> list[DeploymentResponse] orgs_org_id_apps_app_id_envs_env_id_deploys_get(org_id, app_id, env_id)

List Deployments in an Environment.

List all of the Deployments that have been carried out in the current Environment. Deployments are returned with the newest first.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Deployments in an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_deploys_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[DeploymentResponse]**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_post**
> DeploymentResponse orgs_org_id_apps_app_id_envs_env_id_deploys_post(body, org_id, app_id, env_id)

Start a new Deployment.

At Humanitec, Deployments are defined as changes to the state of the Environment. The state can be changed by defining a set of desired changes to the current state via a Deployment Delta or by resetting the current state after a previous Deployment. (See Environment Rebase.) Both types of changes can be combined into a single Deployment during which the Delta is applied to the Rebased state.  When specifying a Delta, a Delta ID must be used. That Delta must have been committed to the Delta store prior to the Deployment.  A Set ID can also be defined in the deployment to force the state of the environment to a particular state. This will be ignored if the Delta is specified.  **NOTE:**  Directly setting a `set_id` in a deployment is not recommended as it will not record history of where the set came from. If the intention is to replicate an existing environment, use the environment rebasing approach described above.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.DeploymentRequest() # DeploymentRequest | The Delta describing the change to the Environment and a comment.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Start a new Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_deploys_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeploymentRequest**](DeploymentRequest.md)| The Delta describing the change to the Environment and a comment.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**DeploymentResponse**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put**
> orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put(body, org_id, app_id, env_id)

Rebase to a different Deployment.

Rebasing an Environment means that the next Deployment to the Environment will be based on the Deployment specified in the rebase rather than the last one in the Environment. The Deployment to rebase to can either be current or a previous Deployment. The Deployment can be from any Environment of the same Application.  _Running code will only be affected on the next Deployment to the Environment._  Common use cases for rebasing an Environment:  * _Rollback_: Rebasing to a previous Deployment in the current Environment and then Deploying without additional changes will execute a rollback to the previous Deployment state.  * _Clone_: Rebasing to the current Deployment in a different Environment and then deploying without additional changes will clone all of the configuration of the other Environment into the current one. (NOTE: External Resources will not be cloned in the process - the current External Resources of the Environment will remain unchanged and will be used by the deployed Application in the Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = 'body_example' # str | The Deployment ID to rebase to.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Rebase to a different Deployment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The Deployment ID to rebase to.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_get**
> EnvironmentResponse orgs_org_id_apps_app_id_envs_env_id_get(org_id, app_id, env_id)

Get a specific Environment.

Gets a specific Environment in an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Get a specific Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**EnvironmentResponse**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_resources_get**
> list[ActiveResourceResponse] orgs_org_id_apps_app_id_envs_env_id_resources_get(org_id, app_id, env_id)

List Active Resources provisioned in an environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Active Resources provisioned in an environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[ActiveResourceResponse]**](ActiveResourceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_resources_graph_post**
> list[NodeBodyResponse] orgs_org_id_apps_app_id_envs_env_id_resources_graph_post(body, org_id, app_id, env_id)

Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = [swagger_client.ResourceProvisionRequestRequest()] # list[ResourceProvisionRequestRequest] | Resources to provision.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_graph_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_resources_graph_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[ResourceProvisionRequestRequest]**](ResourceProvisionRequestRequest.md)| Resources to provision.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[NodeBodyResponse]**](NodeBodyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete(org_id, app_id, env_id, type, res_id)

Delete Active Resources.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
type = 'type_example' # str | The Resource Type, may include a resource class: {type}.{class}.  
res_id = 'res_id_example' # str | The Resource ID.  

try:
    # Delete Active Resources.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete(org_id, app_id, env_id, type, res_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **type** | **str**| The Resource Type, may include a resource class: {type}.{class}.   | 
 **res_id** | **str**| The Resource ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_get**
> list[AutomationRuleResponse] orgs_org_id_apps_app_id_envs_env_id_rules_get(org_id, app_id, env_id)

List all Automation Rules in an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List all Automation Rules in an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_rules_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[AutomationRuleResponse]**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_post**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_post(body, org_id, app_id, env_id)

Create a new Automation Rule for an Environment.

Items marked as deprecated are still supported (however not recommended) for use and are incompatible with properties of the latest api version. In particular an error is raised if  `images_filter` (deprecated) and `artefacts_filter` are used in the same payload. The same is true for `exclude_images_filter` (deprecated) and `exclude_artefacts_filter`. `match` and `update_to` are still supported but will trigger an error if combined with `match_ref`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.AutomationRuleRequest() # AutomationRuleRequest | The definition of the Automation Rule.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Create a new Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_rules_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AutomationRuleRequest**](AutomationRuleRequest.md)| The definition of the Automation Rule.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete(org_id, app_id, env_id, rule_id)

Delete Automation Rule from an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Delete Automation Rule from an Environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete(org_id, app_id, env_id, rule_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get(org_id, app_id, env_id, rule_id)

Get a specific Automation Rule for an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Get a specific Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get(org_id, app_id, env_id, rule_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put(body, org_id, app_id, env_id, rule_id)

Update an existing Automation Rule for an Environment.

Items marked as deprecated are still supported (however not recommended) for use and are incompatible with properties of the latest api version. In particular an error is raised if  `images_filter` (deprecated) and `artefacts_filter` are used in the same payload. The same is true for `exclude_images_filter` (deprecated) and `exclude_artefacts_filter`. `match` and `update_to` are still supported but will trigger an error if combined with `match_ref`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.AutomationRuleRequest() # AutomationRuleRequest | The definition of the Automation Rule.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Update an existing Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put(body, org_id, app_id, env_id, rule_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AutomationRuleRequest**](AutomationRuleRequest.md)| The definition of the Automation Rule.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_runtime_get**
> RuntimeInfoResponse orgs_org_id_apps_app_id_envs_env_id_runtime_get(org_id, app_id, env_id)

Get Runtime information about the environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Get Runtime information about the environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_runtime_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**RuntimeInfoResponse**](RuntimeInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put**
> orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put(body, org_id, app_id, env_id)

Pause / Resume an environment.

On pause requests, all the Kubernetes Deployment resources are scaled down to 0 replicas.  On resume requests, all the Kubernetes Deployment resources are scaled up to the number of replicas running before the environment was paused.  When an environment is paused, it is not possible to:  ```   - Deploy the environment within Humanitec.   - Scale the number of replicas running of any workload. ```

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = True # bool | If the value is `true` the request is to pause an environment, if it is `false` is to resume an environment.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Pause / Resume an environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_runtime_paused_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**bool**](bool.md)| If the value is &#x60;true&#x60; the request is to pause an environment, if it is &#x60;false&#x60; is to resume an environment.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch**
> orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch(body, org_id, app_id, env_id)

Set number of replicas for an environment's modules.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = NULL # dict(str, int) | map of replicas by modules.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Set number of replicas for an environment's modules.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_runtime_replicas_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**dict(str, int)**](dict.md)| map of replicas by modules.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get**
> list[ValueSetVersionResponse] orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get(org_id, app_id, env_id, key_changed=key_changed)

List Value Set Versions in an Environment of an App

A new Value Set Version is created on every modification of a Value inside the an Environment of an App. In case this environment has no overrides the response is the same as the App level endpoint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key_changed = 'key_changed_example' # str | (Optional) Return only value set version where the specified key changed   (optional)

try:
    # List Value Set Versions in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get(org_id, app_id, env_id, key_changed=key_changed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key_changed** | **str**| (Optional) Return only value set version where the specified key changed   | [optional] 

### Return type

[**list[ValueSetVersionResponse]**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get(org_id, app_id, env_id, value_set_version_id)

Get a single Value Set Version in an Environment of an App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Get a single Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get(org_id, app_id, env_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post**
> orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, env_id, value_set_version_id, key)

Purge the value of a specific Shared Value from the App Environment Version history.

Purging permanently removes the value of a specific Shared Value in an application. A purged value is no longer accessible, can't be restored and can't be used by deployments referencing a Value Set Version where the value was present.  Learn more about purging in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#purge). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be purged.  

try:
    # Purge the value of a specific Shared Value from the App Environment Version history.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, env_id, value_set_version_id, key)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be purged.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, env_id, value_set_version_id, key)

Restore a specific key from the Value Set Version in an Environment of an App

Restore the values of a single Shared Value in an Environment from a specific version.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be restored.  

try:
    # Restore a specific key from the Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, env_id, value_set_version_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be restored.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, env_id, value_set_version_id)

Restore a Value Set Version in an Environment of an App

Restore the values of all Shared Values in an environment from a specific version. Keys not existing in the selected version are deleted.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Restore a Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, env_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_delete**
> orgs_org_id_apps_app_id_envs_env_id_values_delete(org_id, app_id, env_id)

Delete all Shared Value for an Environment

All Shared Values will be deleted. If the Shared Values are marked as a secret, they will also be deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Delete all Shared Value for an Environment
    api_instance.orgs_org_id_apps_app_id_envs_env_id_values_delete(org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_get**
> list[ValueResponse] orgs_org_id_apps_app_id_envs_env_id_values_get(org_id, app_id, env_id)

List Shared Values in an Environment

The returned values will be the base Application values with the Environment overrides where applicable. The `source` field will specify the level from which the value is from.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Shared Values in an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[ValueResponse]**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_delete**
> orgs_org_id_apps_app_id_envs_env_id_values_key_delete(org_id, app_id, env_id, key)

Delete Shared Value for an Environment

The specified Shared Value will be permanently deleted. If the Shared Value is marked as a secret, it will also be permanently deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Delete Shared Value for an Environment
    api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_delete(org_id, app_id, env_id, key)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_key_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_patch**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_key_patch(body, org_id, app_id, env_id, key)

Update Shared Value for an Environment

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValuePatchPayloadRequest() # ValuePatchPayloadRequest | At least `value` or `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_patch(body, org_id, app_id, env_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_key_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValuePatchPayloadRequest**](ValuePatchPayloadRequest.md)| At least &#x60;value&#x60; or &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_put**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_key_put(body, org_id, app_id, env_id, key)

Update Shared Value for an Environment

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueEditPayloadRequest() # ValueEditPayloadRequest | Both `value` and `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_put(body, org_id, app_id, env_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_key_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueEditPayloadRequest**](ValueEditPayloadRequest.md)| Both &#x60;value&#x60; and &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_post**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_post(body, org_id, app_id, env_id)

Create a Shared Value for an Environment

The Shared Value created will only be available to the specific Environment.  If a Value is marked as a secret, it will be securely stored. It will not be possible to retrieve the value again through the API. The value of the secret can however be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueCreatePayloadRequest() # ValueCreatePayloadRequest | Definition of the new Shared Value.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Create a Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_env_id_values_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueCreatePayloadRequest**](ValueCreatePayloadRequest.md)| Definition of the new Shared Value.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_get**
> list[EnvironmentResponse] orgs_org_id_apps_app_id_envs_get(org_id, app_id)

List all Environments.

Lists all of the Environments in the Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List all Environments.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[EnvironmentResponse]**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_post**
> EnvironmentResponse orgs_org_id_apps_app_id_envs_post(body, org_id, app_id)

Add a new Environment to an Application.

Creates a new Environment of the specified Type and associates it with the Application specified by `appId`.  The Environment is also initialized to the **current or past state of Deployment in another Environment**. This ensures that every Environment is derived from a previously known state. This means it is not possible to create a new Environment for an Application until at least one Deployment has occurred. (The Deployment does not have to be successful.)  The Type of the Environment must be already defined in the Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.EnvironmentDefinitionRequest() # EnvironmentDefinitionRequest | The ID, Name, Type, and Deployment the Environment will be derived from.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Add a new Environment to an Application.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_envs_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnvironmentDefinitionRequest**](EnvironmentDefinitionRequest.md)| The ID, Name, Type, and Deployment the Environment will be derived from.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**EnvironmentResponse**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_get**
> ApplicationResponse orgs_org_id_apps_app_id_get(org_id, app_id)

Get an existing Application

Gets a specific Application in the specified Organization by ID.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Get an existing Application
    api_response = api_instance.orgs_org_id_apps_app_id_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**ApplicationResponse**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_jobs_delete**
> orgs_org_id_apps_app_id_jobs_delete(org_id, app_id)

Deletes all Jobs for the Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Deletes all Jobs for the Application
    api_instance.orgs_org_id_apps_app_id_jobs_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_jobs_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_runtime_get**
> list[EnvironmentRuntimeInfoResponse] orgs_org_id_apps_app_id_runtime_get(org_id, app_id, id=id)

Get Runtime information about specific environments.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
id = 'id_example' # str | Filter environments by ID (required). Up to 5 ids can be supplied per request.   (optional)

try:
    # Get Runtime information about specific environments.
    api_response = api_instance.orgs_org_id_apps_app_id_runtime_get(org_id, app_id, id=id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_runtime_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **id** | **str**| Filter environments by ID (required). Up to 5 ids can be supplied per request.   | [optional] 

### Return type

[**list[EnvironmentRuntimeInfoResponse]**](EnvironmentRuntimeInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get**
> PlainDeltaResponse orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get(org_id, app_id, set_id, source_set_id)

Get the difference between 2 Deployment Sets

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  
source_set_id = 'source_set_id_example' # str | ID of the Deployment Set to diff against.  

try:
    # Get the difference between 2 Deployment Sets
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get(org_id, app_id, set_id, source_set_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 
 **source_set_id** | **str**| ID of the Deployment Set to diff against.   | 

### Return type

[**PlainDeltaResponse**](PlainDeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_get**
> InlineResponse2001 orgs_org_id_apps_app_id_sets_set_id_get(org_id, app_id, set_id, diff=diff)

Get a Deployment Set

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  
diff = 'diff_example' # str | ID of the Deployment Set to compared against. (optional)

try:
    # Get a Deployment Set
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_get(org_id, app_id, set_id, diff=diff)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_sets_set_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 
 **diff** | **str**| ID of the Deployment Set to compared against. | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_post**
> str orgs_org_id_apps_app_id_sets_set_id_post(body, org_id, app_id, set_id)

Apply a Deployment Delta to a Deployment Set

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.DeltaRequest() # DeltaRequest | The Delta to apply to the Set.

NOTE: The `id` parameter is ignored if provided. The request body should be the full Delta.
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  

try:
    # Apply a Deployment Delta to a Deployment Set
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_post(body, org_id, app_id, set_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_sets_set_id_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| The Delta to apply to the Set.

NOTE: The &#x60;id&#x60; parameter is ignored if provided. The request body should be the full Delta. | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_get**
> list[UserRoleResponse] orgs_org_id_apps_app_id_users_get(org_id, app_id)

List Users with roles in an App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Users with roles in an App
    api_response = api_instance.orgs_org_id_apps_app_id_users_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_post**
> UserRoleResponse orgs_org_id_apps_app_id_users_post(body, org_id, app_id)

Adds a User to an Application with a Role

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UserRoleRequest() # UserRoleRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Adds a User to an Application with a Role
    api_response = api_instance.orgs_org_id_apps_app_id_users_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserRoleRequest**](UserRoleRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_delete**
> orgs_org_id_apps_app_id_users_user_id_delete(org_id, app_id, user_id)

Remove the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Application
    api_instance.orgs_org_id_apps_app_id_users_user_id_delete(org_id, app_id, user_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_get**
> UserRoleResponse orgs_org_id_apps_app_id_users_user_id_get(org_id, app_id, user_id)

Get the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Application
    api_response = api_instance.orgs_org_id_apps_app_id_users_user_id_get(org_id, app_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_patch**
> UserRoleResponse orgs_org_id_apps_app_id_users_user_id_patch(body, org_id, app_id, user_id)

Update the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user role


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Application
    api_response = api_instance.orgs_org_id_apps_app_id_users_user_id_patch(body, org_id, app_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_get**
> list[ValueSetVersionResponse] orgs_org_id_apps_app_id_value_set_versions_get(org_id, app_id, key_changed=key_changed)

List Value Set Versions in the App

A new Value Set Version is created on every modification of a Value inside the app.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key_changed = 'key_changed_example' # str | (Optional) Return only value set version where the specified key changed   (optional)

try:
    # List Value Set Versions in the App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_get(org_id, app_id, key_changed=key_changed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_value_set_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key_changed** | **str**| (Optional) Return only value set version where the specified key changed   | [optional] 

### Return type

[**list[ValueSetVersionResponse]**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get(org_id, app_id, value_set_version_id)

Get a single Value Set Version from the App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Get a single Value Set Version from the App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get(org_id, app_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post**
> orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, value_set_version_id, key)

Purge the value of a specific Shared Value from the App Version history.

Purging permanently removes the value of a specific Shared Value in an Application. A purged value is no longer accessible, can't be restored and can't be used by deployments referencing a Value Set Version where the value was present.  Learn more about purging in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#purge). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be purged.  

try:
    # Purge the value of a specific Shared Value from the App Version history.
    api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, value_set_version_id, key)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be purged.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, value_set_version_id, key)

Restore a specific key from the Value Set Version in an App

Restore the values of a single Shared Value in an application from a specific version.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be restored.  

try:
    # Restore a specific key from the Value Set Version in an App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, value_set_version_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be restored.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, value_set_version_id)

Restore a Value Set Version in an App

Restore the values of all Shared Values in an application from a specific version. Keys not existing in the selected version are deleted.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Restore a Value Set Version in an App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_delete**
> orgs_org_id_apps_app_id_values_delete(org_id, app_id)

Delete all Shared Value for an App

All Shared Values will be deleted. If the Shared Values are marked as a secret, they will also be deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Delete all Shared Value for an App
    api_instance.orgs_org_id_apps_app_id_values_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_get**
> list[ValueResponse] orgs_org_id_apps_app_id_values_get(org_id, app_id)

List Shared Values in an Application

The returned values will be the \"base\" values for the Application. The overridden value for the Environment can be retrieved via the `/orgs/{orgId}/apps/{appId}/envs/{envId}/values` endpoint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Shared Values in an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[ValueResponse]**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_delete**
> orgs_org_id_apps_app_id_values_key_delete(org_id, app_id, key)

Delete Shared Value for an Application

The specified Shared Value will be permanently deleted. If the Shared Value is marked as a secret, it will also be permanently deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Delete Shared Value for an Application
    api_instance.orgs_org_id_apps_app_id_values_key_delete(org_id, app_id, key)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_key_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_patch**
> ValueResponse orgs_org_id_apps_app_id_values_key_patch(body, org_id, app_id, key)

Update Shared Value for an Application

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValuePatchPayloadRequest() # ValuePatchPayloadRequest | At least `value` or `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_key_patch(body, org_id, app_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_key_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValuePatchPayloadRequest**](ValuePatchPayloadRequest.md)| At least &#x60;value&#x60; or &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_put**
> ValueResponse orgs_org_id_apps_app_id_values_key_put(body, org_id, app_id, key)

Update Shared Value for an Application

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueEditPayloadRequest() # ValueEditPayloadRequest | Both `value` and `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_key_put(body, org_id, app_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_key_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueEditPayloadRequest**](ValueEditPayloadRequest.md)| Both &#x60;value&#x60; and &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_post**
> ValueResponse orgs_org_id_apps_app_id_values_post(body, org_id, app_id)

Create a Shared Value for an Application

The Shared Value created will be available to all Environments in that Application.  If a Value is marked as a secret, it will be securely stored. It will not be possible to retrieve the value again through the API. The value of the secret can however be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ValueCreatePayloadRequest() # ValueCreatePayloadRequest | Definition of the new Shared Value.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_values_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueCreatePayloadRequest**](ValueCreatePayloadRequest.md)| Definition of the new Shared Value.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_get**
> list[WebhookResponse] orgs_org_id_apps_app_id_webhooks_get(org_id, app_id)

List Webhooks

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Webhooks
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_webhooks_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[WebhookResponse]**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_delete**
> orgs_org_id_apps_app_id_webhooks_job_id_delete(org_id, app_id, job_id)

Delete a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Delete a Webhook
    api_instance.orgs_org_id_apps_app_id_webhooks_job_id_delete(org_id, app_id, job_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_webhooks_job_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_get**
> WebhookResponse orgs_org_id_apps_app_id_webhooks_job_id_get(org_id, app_id, job_id)

Get a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Get a Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_job_id_get(org_id, app_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_webhooks_job_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_patch**
> WebhookUpdateResponse orgs_org_id_apps_app_id_webhooks_job_id_patch(body, org_id, app_id, job_id)

Update a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.WebhookRequest() # WebhookRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Update a Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_job_id_patch(body, org_id, app_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_webhooks_job_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebhookRequest**](WebhookRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

[**WebhookUpdateResponse**](WebhookUpdateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_post**
> WebhookResponse orgs_org_id_apps_app_id_webhooks_post(body, org_id, app_id)

Create a new Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.WebhookRequest() # WebhookRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a new Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_app_id_webhooks_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebhookRequest**](WebhookRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_get**
> list[ApplicationResponse] orgs_org_id_apps_get(org_id)

List all Applications in an Organization.

Listing or lists of all Applications that exist within a specific Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List all Applications in an Organization.
    api_response = api_instance.orgs_org_id_apps_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ApplicationResponse]**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_post**
> ApplicationResponse orgs_org_id_apps_post(body, org_id)

Add a new Application to an Organization

Creates a new Application, then adds it to the specified Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ApplicationCreationRequest() # ApplicationCreationRequest | The request ID, Human-friendly name and environment of the Application.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Add a new Application to an Organization
    api_response = api_instance.orgs_org_id_apps_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_apps_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApplicationCreationRequest**](ApplicationCreationRequest.md)| The request ID, Human-friendly name and environment of the Application.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ApplicationResponse**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefact_versions_artefact_version_id_get**
> ArtefactVersionResponse orgs_org_id_artefact_versions_artefact_version_id_get(org_id, artefact_version_id)

Get an Artefacts Versions.

Returns a specific Artefact Version.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_version_id = 'artefact_version_id_example' # str | The Artefact Version ID.  

try:
    # Get an Artefacts Versions.
    api_response = api_instance.orgs_org_id_artefact_versions_artefact_version_id_get(org_id, artefact_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefact_versions_artefact_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_version_id** | **str**| The Artefact Version ID.   | 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefact_versions_get**
> list[ArtefactVersionResponse] orgs_org_id_artefact_versions_get(org_id, name=name, reference=reference, archived=archived)

List all Artefacts Versions.

Returns the Artefact Versions registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
name = 'name_example' # str | (Optional) Filter Artefact Versions by name.   (optional)
reference = 'reference_example' # str | (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with `name`.   (optional)
archived = 'archived_example' # str | (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   (optional)

try:
    # List all Artefacts Versions.
    api_response = api_instance.orgs_org_id_artefact_versions_get(org_id, name=name, reference=reference, archived=archived)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefact_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **name** | **str**| (Optional) Filter Artefact Versions by name.   | [optional] 
 **reference** | **str**| (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with &#x60;name&#x60;.   | [optional] 
 **archived** | **str**| (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   | [optional] 

### Return type

[**list[ArtefactVersionResponse]**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefact_versions_post**
> ArtefactVersionResponse orgs_org_id_artefact_versions_post(body, org_id, vcs=vcs)

Register a new Artefact Version with your organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.AddArtefactVersionPayloadRequest() # AddArtefactVersionPayloadRequest | The data needed to register a new Artefact Version within the organization.


org_id = 'org_id_example' # str | The organization ID.  
vcs = 'vcs_example' # str | (Optional) Which version control system the version comes from. Default value is \"git\". If this parameter is not supplied or its value is \"git\", the provided ref, if not empty, is checked to ensure that it has the prefix \"refs/\".   (optional)

try:
    # Register a new Artefact Version with your organization.
    api_response = api_instance.orgs_org_id_artefact_versions_post(body, org_id, vcs=vcs)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefact_versions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddArtefactVersionPayloadRequest**](AddArtefactVersionPayloadRequest.md)| The data needed to register a new Artefact Version within the organization.

 | 
 **org_id** | **str**| The organization ID.   | 
 **vcs** | **str**| (Optional) Which version control system the version comes from. Default value is \&quot;git\&quot;. If this parameter is not supplied or its value is \&quot;git\&quot;, the provided ref, if not empty, is checked to ensure that it has the prefix \&quot;refs/\&quot;.   | [optional] 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_artefact_id_delete**
> orgs_org_id_artefacts_artefact_id_delete(org_id, artefact_id)

Delete Artefact and all related Artefact Versions

The specified Artefact and its Artefact Versions will be permanently deleted. Only Administrators can delete an Artefact.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  

try:
    # Delete Artefact and all related Artefact Versions
    api_instance.orgs_org_id_artefacts_artefact_id_delete(org_id, artefact_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefacts_artefact_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_artefact_id_versions_get**
> list[ArtefactVersionResponse] orgs_org_id_artefacts_artefact_id_versions_get(org_id, artefact_id, archived=archived, reference=reference, limit=limit)

List all Artefact Versions of an Artefact.

Returns the Artefact Versions of a specified Artefact registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  
archived = 'archived_example' # str | (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   (optional)
reference = 'reference_example' # str | (Optional) Filter Artefact Versions by by name including a version or digest.   (optional)
limit = 'limit_example' # str | (Optional) Limit the number of versions returned by the endpoint.   (optional)

try:
    # List all Artefact Versions of an Artefact.
    api_response = api_instance.orgs_org_id_artefacts_artefact_id_versions_get(org_id, artefact_id, archived=archived, reference=reference, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefacts_artefact_id_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 
 **archived** | **str**| (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   | [optional] 
 **reference** | **str**| (Optional) Filter Artefact Versions by by name including a version or digest.   | [optional] 
 **limit** | **str**| (Optional) Limit the number of versions returned by the endpoint.   | [optional] 

### Return type

[**list[ArtefactVersionResponse]**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_artefact_id_versions_version_id_patch**
> ArtefactVersionResponse orgs_org_id_artefacts_artefact_id_versions_version_id_patch(body, org_id, artefact_id, version_id)

Update Version of an Artefact.

Update the version of a specified Artefact registered with your organization\".

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UpdateArtefactVersionPayloadRequest() # UpdateArtefactVersionPayloadRequest | The Artefact Version Update Request. Only the field `archive` can be updated.


org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  
version_id = 'version_id_example' # str | The Version ID.  

try:
    # Update Version of an Artefact.
    api_response = api_instance.orgs_org_id_artefacts_artefact_id_versions_version_id_patch(body, org_id, artefact_id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefacts_artefact_id_versions_version_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateArtefactVersionPayloadRequest**](UpdateArtefactVersionPayloadRequest.md)| The Artefact Version Update Request. Only the field &#x60;archive&#x60; can be updated.

 | 
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 
 **version_id** | **str**| The Version ID.   | 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_get**
> list[ArtefactResponse] orgs_org_id_artefacts_get(org_id, type=type, name=name)

List all Artefacts.

Returns the Artefacts registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
type = 'type_example' # str | (Optional) Filter Artefacts by type.   (optional)
name = 'name_example' # str | (Optional) Filter Artefacts by name.   (optional)

try:
    # List all Artefacts.
    api_response = api_instance.orgs_org_id_artefacts_get(org_id, type=type, name=name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_artefacts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **type** | **str**| (Optional) Filter Artefacts by type.   | [optional] 
 **name** | **str**| (Optional) Filter Artefacts by name.   | [optional] 

### Return type

[**list[ArtefactResponse]**](ArtefactResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_id_delete**
> EnvironmentTypeResponse orgs_org_id_env_types_env_type_id_delete(org_id, env_type_id)

Deletes an Environment Type

Deletes a specific Environment Type from an Organization. If there are Environments with this Type in the Organization, the operation will fail.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type_id = 'env_type_id_example' # str | ID of the Environment Type.  

try:
    # Deletes an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_id_delete(org_id, env_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type_id** | **str**| ID of the Environment Type.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_id_get**
> EnvironmentTypeResponse orgs_org_id_env_types_env_type_id_get(org_id, env_type_id)

Get an Environment Type

Gets a specific Environment Type within an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type_id = 'env_type_id_example' # str | ID of the Environment Type.  

try:
    # Get an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_id_get(org_id, env_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type_id** | **str**| ID of the Environment Type.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_post**
> UserRoleResponse orgs_org_id_env_types_env_type_users_post(body, org_id, env_type)

Adds a User to an Environment Type with a Role

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UserRoleRequest() # UserRoleRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  

try:
    # Adds a User to an Environment Type with a Role
    api_response = api_instance.orgs_org_id_env_types_env_type_users_post(body, org_id, env_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserRoleRequest**](UserRoleRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_delete**
> orgs_org_id_env_types_env_type_users_user_id_delete(org_id, env_type, user_id)

Remove the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Environment Type
    api_instance.orgs_org_id_env_types_env_type_users_user_id_delete(org_id, env_type, user_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_get**
> UserRoleResponse orgs_org_id_env_types_env_type_users_user_id_get(org_id, env_type, user_id)

Get the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_users_user_id_get(org_id, env_type, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_patch**
> UserRoleResponse orgs_org_id_env_types_env_type_users_user_id_patch(body, org_id, env_type, user_id)

Update the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user role


org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_users_user_id_patch(body, org_id, env_type, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_env_type_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_get**
> list[EnvironmentTypeResponse] orgs_org_id_env_types_get(org_id)

List all Environment Types

Lists all Environment Types in an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List all Environment Types
    api_response = api_instance.orgs_org_id_env_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[EnvironmentTypeResponse]**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_post**
> EnvironmentTypeResponse orgs_org_id_env_types_post(body, org_id)

Add a new Environment Type

Adds a new Environment Type to an Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.EnvironmentTypeRequest() # EnvironmentTypeRequest | New Environment Type.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Add a new Environment Type
    api_response = api_instance.orgs_org_id_env_types_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_env_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnvironmentTypeRequest**](EnvironmentTypeRequest.md)| New Environment Type.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**EnvironmentTypeResponse**](EnvironmentTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_events_get**
> list[EventResponse] orgs_org_id_events_get(org_id)

List Events

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Events
    api_response = api_instance.orgs_org_id_events_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_events_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[EventResponse]**](EventResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_get**
> OrganizationResponse orgs_org_id_get(org_id)

Get the specified Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Get the specified Organization.
    api_response = api_instance.orgs_org_id_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**OrganizationResponse**](OrganizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_get**
> list[ImageResponse] orgs_org_id_images_get(org_id)

List all Container Images

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  Lists all of the Container Images registered for this organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  

try:
    # List all Container Images
    api_response = api_instance.orgs_org_id_images_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_images_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 

### Return type

[**list[ImageResponse]**](ImageResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_builds_get**
> list[ImageBuildResponse] orgs_org_id_images_image_id_builds_get(org_id, image_id)

Lists all the Builds of an Image

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  The response lists all available Image Builds of an Image.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Lists all the Builds of an Image
    api_response = api_instance.orgs_org_id_images_image_id_builds_get(org_id, image_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_images_image_id_builds_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

[**list[ImageBuildResponse]**](ImageBuildResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_builds_post**
> orgs_org_id_images_image_id_builds_post(body, org_id, image_id)

Add a new Image Build

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  This endpoint is used by Continuous Integration (CI) pipelines to notify Humanitec that a new Image Build is available.  If there is no Image with ID `imageId`, it will be automatically created.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.ImageBuildRequest() # ImageBuildRequest | The metadata associated with the build.


org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Add a new Image Build
    api_instance.orgs_org_id_images_image_id_builds_post(body, org_id, image_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_images_image_id_builds_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ImageBuildRequest**](ImageBuildRequest.md)| The metadata associated with the build.

 | 
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_get**
> ImageResponse orgs_org_id_images_image_id_get(org_id, image_id)

Get a specific Image Object

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  The response includes a list of Image Builds as well as some metadata about the Image such as its Image Source.  Note, `imageId` may not be the same as the container name. `imageId` is determined by the system making notifications about new builds.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Get a specific Image Object
    api_response = api_instance.orgs_org_id_images_image_id_get(org_id, image_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_images_image_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

[**ImageResponse**](ImageResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_invitations_get**
> list[UserInviteResponse] orgs_org_id_invitations_get(org_id)

List the invites issued for the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List the invites issued for the organization.
    api_response = api_instance.orgs_org_id_invitations_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_invitations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserInviteResponse]**](UserInviteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_invitations_post**
> list[UserRoleResponse] orgs_org_id_invitations_post(body, org_id)

Invites a user to an Organization with a specified role.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UserInviteRequestRequest() # UserInviteRequestRequest | The email and the desired role


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Invites a user to an Organization with a specified role.
    api_response = api_instance.orgs_org_id_invitations_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_invitations_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserInviteRequestRequest**](UserInviteRequestRequest.md)| The email and the desired role

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_get**
> list[RegistryResponse] orgs_org_id_registries_get(org_id)

Lists available registries for the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  

try:
    # Lists available registries for the organization.
    api_response = api_instance.orgs_org_id_registries_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 

### Return type

[**list[RegistryResponse]**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_post**
> RegistryResponse orgs_org_id_registries_post(body, org_id)

Creates a new registry record.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RegistryRequest() # RegistryRequest | A new record details.


org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  

try:
    # Creates a new registry record.
    api_response = api_instance.orgs_org_id_registries_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RegistryRequest**](RegistryRequest.md)| A new record details.

 | 
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_creds_get**
> RegistryCredsResponse orgs_org_id_registries_reg_id_creds_get(org_id, reg_id)

Returns current account credentials or secret details for the registry.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Returns current account credentials or secret details for the registry.
    api_response = api_instance.orgs_org_id_registries_reg_id_creds_get(org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_reg_id_creds_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryCredsResponse**](RegistryCredsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_delete**
> orgs_org_id_registries_reg_id_delete(org_id, reg_id)

Deletes an existing registry record and all associated credentials and secrets.

_Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Deletes an existing registry record and all associated credentials and secrets.
    api_instance.orgs_org_id_registries_reg_id_delete(org_id, reg_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_reg_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_get**
> RegistryResponse orgs_org_id_registries_reg_id_get(org_id, reg_id)

Loads a registry record details.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Loads a registry record details.
    api_response = api_instance.orgs_org_id_registries_reg_id_get(org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_reg_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_patch**
> RegistryResponse orgs_org_id_registries_reg_id_patch(body, org_id, reg_id)

Updates (patches) an existing registry record.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RegistryRequest() # RegistryRequest | Record details to update.


org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Updates (patches) an existing registry record.
    api_response = api_instance.orgs_org_id_registries_reg_id_patch(body, org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_registries_reg_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RegistryRequest**](RegistryRequest.md)| Record details to update.

 | 
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_account_types_get**
> list[AccountTypeResponse] orgs_org_id_resources_account_types_get(org_id)

List Resource Account Types available to the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Account Types available to the organization.
    api_response = api_instance.orgs_org_id_resources_account_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_account_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[AccountTypeResponse]**](AccountTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_acc_id_delete**
> orgs_org_id_resources_accounts_acc_id_delete(org_id, acc_id)

Delete an unused Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Delete an unused Resource Account.
    api_instance.orgs_org_id_resources_accounts_acc_id_delete(org_id, acc_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_accounts_acc_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_acc_id_get**
> ResourceAccountResponse orgs_org_id_resources_accounts_acc_id_get(org_id, acc_id)

Get a Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Get a Resource Account.
    api_response = api_instance.orgs_org_id_resources_accounts_acc_id_get(org_id, acc_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_accounts_acc_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_acc_id_patch**
> ResourceAccountResponse orgs_org_id_resources_accounts_acc_id_patch(body, org_id, acc_id)

Update a Resource Account.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UpdateResourceAccountRequestRequest() # UpdateResourceAccountRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
acc_id = 'acc_id_example' # str | The Resource Account ID.  

try:
    # Update a Resource Account.
    api_response = api_instance.orgs_org_id_resources_accounts_acc_id_patch(body, org_id, acc_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_accounts_acc_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateResourceAccountRequestRequest**](UpdateResourceAccountRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **acc_id** | **str**| The Resource Account ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_get**
> list[ResourceAccountResponse] orgs_org_id_resources_accounts_get(org_id)

List Resource Accounts in the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Accounts in the organization.
    api_response = api_instance.orgs_org_id_resources_accounts_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_accounts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ResourceAccountResponse]**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_accounts_post**
> ResourceAccountResponse orgs_org_id_resources_accounts_post(body, org_id)

Create a new Resource Account in the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.CreateResourceAccountRequestRequest() # CreateResourceAccountRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a new Resource Account in the organization.
    api_response = api_instance.orgs_org_id_resources_accounts_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_accounts_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateResourceAccountRequestRequest**](CreateResourceAccountRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ResourceAccountResponse**](ResourceAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**
> orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)

Delete a Matching Criteria from a Resource Definition.

If there **are no** Active Resources that would match to a different Resource Definition when the current Matching Criteria is deleted, the Matching Criteria is deleted immediately.  If there **are** Active Resources that would match to a different Resource Definition, the request fails with HTTP status code 409 (Conflict). The response content will list all of affected Active Resources and their new matches.  The request can take an optional `force` query parameter. If set to `true`, the Matching Criteria is deleted immediately. Referenced Active Resources would match to a different Resource Definition during the next deployment in the target environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  
criteria_id = 'criteria_id_example' # str | The Matching Criteria ID.  
force = true # bool | If set to `true`, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   (optional)

try:
    # Delete a Matching Criteria from a Resource Definition.
    api_instance.orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 
 **criteria_id** | **str**| The Matching Criteria ID.   | 
 **force** | **bool**| If set to &#x60;true&#x60;, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_criteria_post**
> MatchingCriteriaResponse orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)

Add a new Matching Criteria to a Resource Definition.

Matching Criteria are combined with Resource Type to select a specific definition. Matching Criteria can be set for any combination of Application ID, Environment ID, Environment Type, and Resource ID. In the event of multiple matches, the most specific match is chosen.  For example, given 3 sets of matching criteria for the same type:  ```  1. {\"env_type\":\"test\"}  2. {\"env_type\":\"development\"}  3. {\"env_type\":\"test\", \"app_id\":\"my-app\"} ```  If, a resource of that time was needed in an Application `my-app`, Environment `qa-team` with Type `test` and Resource ID `modules.my-module-externals.my-resource`, there would be two resource definitions matching the criteria: #1 & #3. Definition #3 will be chosen because its matching criteria is the most specific.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.MatchingCriteriaRuleRequest() # MatchingCriteriaRuleRequest | Matching Criteria rules.


org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Add a new Matching Criteria to a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_criteria_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MatchingCriteriaRuleRequest**](MatchingCriteriaRuleRequest.md)| Matching Criteria rules.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**MatchingCriteriaResponse**](MatchingCriteriaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_delete**
> orgs_org_id_resources_defs_def_id_delete(org_id, def_id, force=force)

Delete a Resource Definition.

If there **are no** Active Resources provisioned via the current definition, the Resource Definition is deleted immediately.  If there **are** Active Resources provisioned via the current definition, the request fails. The response will describe the changes to the affected Active Resources if operation is forced.  The request can take an optional `force` query parameter. If set to `true`, the current Resource Definition is **marked as** pending deletion and will be deleted (purged) as soon as no existing Active Resources reference it. With the next deployment matching criteria for Resources will be re-evaluated, and current Active Resources for the target environment would be either linked to another matching Resource Definition or decommissioned and created using the new or default Resource Definition (when available).  The Resource Definition that has been marked for deletion cannot be used to provision new resources.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  
force = true # bool | If set to `true`, will mark the Resource Definition for deletion, even if it affects existing Active Resources.   (optional)

try:
    # Delete a Resource Definition.
    api_instance.orgs_org_id_resources_defs_def_id_delete(org_id, def_id, force=force)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 
 **force** | **bool**| If set to &#x60;true&#x60;, will mark the Resource Definition for deletion, even if it affects existing Active Resources.   | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_get**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_get(org_id, def_id)

Get a specific Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Get a specific Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_get(org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_patch**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_patch(body, org_id, def_id)

Update a Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.PatchResourceDefinitionRequestRequest() # PatchResourceDefinitionRequestRequest | The Resource Definition record details.

The PATCH operation would change the value of the property if it is included in the request payload JSON, and not `null`. Missing and `null` properties are ignored.

For the map properties, such as PatchResourceDefinitionRequest.DriverInputs, the merge operation is applied.

Merge rules are as follows:

- If a map property has a value, it is replaced (or added).

- If a map property is set to `null`, it is removed.

- If a map property is not included (missing in JSON), it remains unchanged.
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Update a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_patch(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PatchResourceDefinitionRequestRequest**](PatchResourceDefinitionRequestRequest.md)| The Resource Definition record details.

The PATCH operation would change the value of the property if it is included in the request payload JSON, and not &#x60;null&#x60;. Missing and &#x60;null&#x60; properties are ignored.

For the map properties, such as PatchResourceDefinitionRequest.DriverInputs, the merge operation is applied.

Merge rules are as follows:

- If a map property has a value, it is replaced (or added).

- If a map property is set to &#x60;null&#x60;, it is removed.

- If a map property is not included (missing in JSON), it remains unchanged. | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_put**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_put(body, org_id, def_id)

Update a Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UpdateResourceDefinitionRequestRequest() # UpdateResourceDefinitionRequestRequest | The Resource Definition record details.

The PUT operation updates a resource definition using the provided payload. An empty driver_account or driver_inputs property will unset the existing values.

Currently the resource and driver types can't be changed.
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Update a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_put(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateResourceDefinitionRequestRequest**](UpdateResourceDefinitionRequestRequest.md)| The Resource Definition record details.

The PUT operation updates a resource definition using the provided payload. An empty driver_account or driver_inputs property will unset the existing values.

Currently the resource and driver types can&#x27;t be changed. | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_resources_get**
> list[ActiveResourceResponse] orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)

List Active Resources provisioned via a specific Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # List Active Resources provisioned via a specific Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_def_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**list[ActiveResourceResponse]**](ActiveResourceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_get**
> list[ResourceDefinitionResponse] orgs_org_id_resources_defs_get(org_id, app=app, env=env, env_type=env_type, res=res, res_type=res_type, _class=_class)

List Resource Definitions.

Filter criteria can be applied to obtain all the resource definitions that could match the filters, grouped by type and sorted by matching rank.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
app = 'app_example' # str | (Optional) Filter Resource Definitions that may match a specific Application.   (optional)
env = 'env_example' # str | (Optional) Filter Resource Definitions that may match a specific Environment.   (optional)
env_type = 'env_type_example' # str | (Optional) Filter Resource Definitions that may match a specific Environment Type.   (optional)
res = 'res_example' # str | (Optional) Filter Resource Definitions that may match a specific Resource.   (optional)
res_type = 'res_type_example' # str | (Optional) Filter Resource Definitions that may match a specific Resource Type.   (optional)
_class = '_class_example' # str | (Optional) Filter Resource Definitions that may match a specific Class.   (optional)

try:
    # List Resource Definitions.
    api_response = api_instance.orgs_org_id_resources_defs_get(org_id, app=app, env=env, env_type=env_type, res=res, res_type=res_type, _class=_class)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app** | **str**| (Optional) Filter Resource Definitions that may match a specific Application.   | [optional] 
 **env** | **str**| (Optional) Filter Resource Definitions that may match a specific Environment.   | [optional] 
 **env_type** | **str**| (Optional) Filter Resource Definitions that may match a specific Environment Type.   | [optional] 
 **res** | **str**| (Optional) Filter Resource Definitions that may match a specific Resource.   | [optional] 
 **res_type** | **str**| (Optional) Filter Resource Definitions that may match a specific Resource Type.   | [optional] 
 **_class** | **str**| (Optional) Filter Resource Definitions that may match a specific Class.   | [optional] 

### Return type

[**list[ResourceDefinitionResponse]**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_post**
> ResourceDefinitionResponse orgs_org_id_resources_defs_post(body, org_id)

Create a new Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.CreateResourceDefinitionRequestRequest() # CreateResourceDefinitionRequestRequest | The Resource Definition details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a new Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_defs_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateResourceDefinitionRequestRequest**](CreateResourceDefinitionRequestRequest.md)| The Resource Definition details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_driver_id_delete**
> orgs_org_id_resources_drivers_driver_id_delete(org_id, driver_id)

Delete a Resources Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resources Driver ID to delete.  

try:
    # Delete a Resources Driver.
    api_instance.orgs_org_id_resources_drivers_driver_id_delete(org_id, driver_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_drivers_driver_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resources Driver ID to delete.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_driver_id_get**
> DriverDefinitionResponse orgs_org_id_resources_drivers_driver_id_get(org_id, driver_id)

Get a Resource Driver.

# Only drivers that belongs to the given organization or registered as `public` are accessible through this endpoint

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resource Driver ID.  

try:
    # Get a Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_driver_id_get(org_id, driver_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_drivers_driver_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resource Driver ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_driver_id_put**
> DriverDefinitionResponse orgs_org_id_resources_drivers_driver_id_put(body, org_id, driver_id)

Update a Resource Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UpdateDriverRequestRequest() # UpdateDriverRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resource Driver ID.  

try:
    # Update a Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_driver_id_put(body, org_id, driver_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_drivers_driver_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateDriverRequestRequest**](UpdateDriverRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resource Driver ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_get**
> list[DriverDefinitionResponse] orgs_org_id_resources_drivers_get(org_id)

List Resource Drivers.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Drivers.
    api_response = api_instance.orgs_org_id_resources_drivers_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_drivers_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[DriverDefinitionResponse]**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_post**
> DriverDefinitionResponse orgs_org_id_resources_drivers_post(body, org_id)

Register a new Resource Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.CreateDriverRequestRequest() # CreateDriverRequestRequest | Resources Driver details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Register a new Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_drivers_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateDriverRequestRequest**](CreateDriverRequestRequest.md)| Resources Driver details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_types_get**
> list[ResourceTypeResponse] orgs_org_id_resources_types_get(org_id)

List Resource Types.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Types.
    api_response = api_instance.orgs_org_id_resources_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_resources_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ResourceTypeResponse]**](ResourceTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_get**
> list[SecretStoreResponse] orgs_org_id_secretstores_get(org_id)

Get list of Secret Stores for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Get list of Secret Stores for the given organization.
    api_response = api_instance.orgs_org_id_secretstores_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_secretstores_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[SecretStoreResponse]**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_post**
> SecretStoreResponse orgs_org_id_secretstores_post(body, org_id)

Create a Secret Store for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.CreateSecretStorePayloadRequest() # CreateSecretStorePayloadRequest | Secret Store data.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a Secret Store for the given organization.
    api_response = api_instance.orgs_org_id_secretstores_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_secretstores_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateSecretStorePayloadRequest**](CreateSecretStorePayloadRequest.md)| Secret Store data.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_delete**
> orgs_org_id_secretstores_store_id_delete(org_id, store_id)

Delete the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Delete the Secret Store.
    api_instance.orgs_org_id_secretstores_store_id_delete(org_id, store_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_secretstores_store_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_get**
> SecretStoreResponse orgs_org_id_secretstores_store_id_get(org_id, store_id)

Get the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Get the Secret Store.
    api_response = api_instance.orgs_org_id_secretstores_store_id_get(org_id, store_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_secretstores_store_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_patch**
> SecretStoreResponse orgs_org_id_secretstores_store_id_patch(body, org_id, store_id)

Update the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.UpdateSecretStorePayloadRequest() # UpdateSecretStorePayloadRequest | Secret Store data.


org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Update the Secret Store.
    api_response = api_instance.orgs_org_id_secretstores_store_id_patch(body, org_id, store_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_secretstores_store_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateSecretStorePayloadRequest**](UpdateSecretStorePayloadRequest.md)| Secret Store data.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_get**
> list[UserRoleResponse] orgs_org_id_users_get(org_id)

List Users with roles in an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Users with roles in an Organization
    api_response = api_instance.orgs_org_id_users_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_post**
> UserProfileResponse orgs_org_id_users_post(body, org_id)

Creates a new service user.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.NewServiceUserRequest() # NewServiceUserRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Creates a new service user.
    api_response = api_instance.orgs_org_id_users_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NewServiceUserRequest**](NewServiceUserRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**UserProfileResponse**](UserProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_delete**
> orgs_org_id_users_user_id_delete(org_id, user_id)

Remove the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Organization
    api_instance.orgs_org_id_users_user_id_delete(org_id, user_id)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_get**
> UserRoleResponse orgs_org_id_users_user_id_get(org_id, user_id)

Get the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Organization
    api_response = api_instance.orgs_org_id_users_user_id_get(org_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_patch**
> UserRoleResponse orgs_org_id_users_user_id_patch(body, org_id, user_id)

Update the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user the role


org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Organization
    api_response = api_instance.orgs_org_id_users_user_id_patch(body, org_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_get**
> list[WorkloadProfileResponse] orgs_org_id_workload_profiles_get(org_id, per_page=per_page, page=page)

List workload profiles available to the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List workload profiles available to the organization.
    api_response = api_instance.orgs_org_id_workload_profiles_get(org_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_workload_profiles_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileResponse]**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_post**
> WorkloadProfileResponse orgs_org_id_workload_profiles_post(body, org_id)

Create new Workload Profile

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.WorkloadProfileRequest() # WorkloadProfileRequest | Workload profile details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create new Workload Profile
    api_response = api_instance.orgs_org_id_workload_profiles_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_workload_profiles_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WorkloadProfileRequest**](WorkloadProfileRequest.md)| Workload profile details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**WorkloadProfileResponse**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_id_versions_version_delete**
> orgs_org_id_workload_profiles_profile_id_versions_version_delete(org_id, profile_id, version)

Delete a Workload Profile Version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_id = 'profile_id_example' # str | The Workload profile ID.  
version = 'version_example' # str | The Version.  

try:
    # Delete a Workload Profile Version
    api_instance.orgs_org_id_workload_profiles_profile_id_versions_version_delete(org_id, profile_id, version)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_workload_profiles_profile_id_versions_version_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_id** | **str**| The Workload profile ID.   | 
 **version** | **str**| The Version.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_qid_delete**
> orgs_org_id_workload_profiles_profile_qid_delete(org_id, profile_qid)

Delete a Workload Profile

This will also delete all versions of a workload profile.  It is not possible to delete profiles of other organizations.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_qid = 'profile_qid_example' # str | The Workload profile ID.  

try:
    # Delete a Workload Profile
    api_instance.orgs_org_id_workload_profiles_profile_qid_delete(org_id, profile_qid)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_workload_profiles_profile_qid_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_qid** | **str**| The Workload profile ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_qid_get**
> WorkloadProfileResponse orgs_org_id_workload_profiles_profile_qid_get(org_id, profile_qid)

Get a Workload Profile

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_qid = 'profile_qid_example' # str | The fully qualified Workload ID. (If not a profile from the current org, must be prefixed with `{orgId}.` e.g. `humanitec.default-cronjob`)  

try:
    # Get a Workload Profile
    api_response = api_instance.orgs_org_id_workload_profiles_profile_qid_get(org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->orgs_org_id_workload_profiles_profile_qid_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_qid** | **str**| The fully qualified Workload ID. (If not a profile from the current org, must be prefixed with &#x60;{orgId}.&#x60; e.g. &#x60;humanitec.default-cronjob&#x60;)   | 

### Return type

[**WorkloadProfileResponse**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_delta**
> put_delta(body, org_id, app_id, delta_id)

Update an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.DeltaRequest() # DeltaRequest | An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the `modules.add` property are replaced with the new Delta's values. If the value of a Module is `null`, and the ID is in the `modules.remove` list, it is removed from the `modules.remove` list.

* All IDs listed in `modules.remove` are combined. Any ID in `modules.remove` and also in `modules.add` are removed from `modules.add`

* The lists of JSON Patches in `modules.update` are concatenated or created in `modules.updates`.

Simplification involves:

* Applying any entries in `modules.updates` that have matching IDs in `modules.add` to the `modules.add` entry and removing the `modules.update` entry.

* Reducing the number of JSON Patches in each `modules.update` entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the `value` of the `remove` action can be set to `{"scope": "delta"}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the `id` or `metadata` properties are specified, they will be ignored._
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to update.  

try:
    # Update an existing Delta
    api_instance.put_delta(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling PublicApi->put_delta: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the &#x60;modules.add&#x60; property are replaced with the new Delta&#x27;s values. If the value of a Module is &#x60;null&#x60;, and the ID is in the &#x60;modules.remove&#x60; list, it is removed from the &#x60;modules.remove&#x60; list.

* All IDs listed in &#x60;modules.remove&#x60; are combined. Any ID in &#x60;modules.remove&#x60; and also in &#x60;modules.add&#x60; are removed from &#x60;modules.add&#x60;

* The lists of JSON Patches in &#x60;modules.update&#x60; are concatenated or created in &#x60;modules.updates&#x60;.

Simplification involves:

* Applying any entries in &#x60;modules.updates&#x60; that have matching IDs in &#x60;modules.add&#x60; to the &#x60;modules.add&#x60; entry and removing the &#x60;modules.update&#x60; entry.

* Reducing the number of JSON Patches in each &#x60;modules.update&#x60; entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the &#x60;value&#x60; of the &#x60;remove&#x60; action can be set to &#x60;{&quot;scope&quot;: &quot;delta&quot;}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the &#x60;id&#x60; or &#x60;metadata&#x60; properties are specified, they will be ignored._ | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **restart_run**
> RunResponse restart_run(org_id, app_id, pipeline_id, run_id, idempotency_key=idempotency_key)

Restart a Run within an Pipeline by cloning it with the same trigger and inputs.

Attempts to copy and restart the specified Run. The run must be in a completed state. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
idempotency_key = 'idempotency_key_example' # str | The HTTP Idempotency-Key (optional)

try:
    # Restart a Run within an Pipeline by cloning it with the same trigger and inputs.
    api_response = api_instance.restart_run(org_id, app_id, pipeline_id, run_id, idempotency_key=idempotency_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->restart_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **idempotency_key** | **str**| The HTTP Idempotency-Key | [optional] 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tokens_get**
> dict(str, object) tokens_get()

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()

try:
    # DEPRECATED
    api_response = api_instance.tokens_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->tokens_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**dict(str, object)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tokens_token_id_delete**
> tokens_token_id_delete(token_id)

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
token_id = 'token_id_example' # str | The token ID  

try:
    # DEPRECATED
    api_instance.tokens_token_id_delete(token_id)
except ApiException as e:
    print("Exception when calling PublicApi->tokens_token_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token_id** | **str**| The token ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_pipeline**
> Pipeline update_pipeline(body, org_id, app_id, pipeline_id, if_match=if_match)

update a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = NULL # object | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # update a Pipeline within an Application.
    api_response = api_instance.update_pipeline(body, org_id, app_id, pipeline_id, if_match=if_match)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->update_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**object**](object.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-yaml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_get**
> dict(str, object) users_me_get()

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()

try:
    # DEPRECATED
    api_response = api_instance.users_me_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->users_me_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**dict(str, object)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_get**
> list[TokenInfoResponse] users_user_id_tokens_get(user_id)

Lists tokens associated with a user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
user_id = 'user_id_example' # str | The user ID.  

try:
    # Lists tokens associated with a user
    api_response = api_instance.users_user_id_tokens_get(user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->users_user_id_tokens_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 

### Return type

[**list[TokenInfoResponse]**](TokenInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_post**
> TokenResponse users_user_id_tokens_post(body, user_id)

Creates a new static token for a user.

This is only supported for users of type `service`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
body = swagger_client.TokenDefinitionRequest() # TokenDefinitionRequest | The definition of the token.


user_id = 'user_id_example' # str | The user ID.  

try:
    # Creates a new static token for a user.
    api_response = api_instance.users_user_id_tokens_post(body, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->users_user_id_tokens_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenDefinitionRequest**](TokenDefinitionRequest.md)| The definition of the token.

 | 
 **user_id** | **str**| The user ID.   | 

### Return type

[**TokenResponse**](TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_token_id_delete**
> users_user_id_tokens_token_id_delete(user_id, token_id)

Deletes a specific token associated with a user

This is only possible for static tokens. To revoke session tokens - use `POST /auth/logout` with the required session token.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
user_id = 'user_id_example' # str | The user ID.  
token_id = 'token_id_example' # str | The token ID.  

try:
    # Deletes a specific token associated with a user
    api_instance.users_user_id_tokens_token_id_delete(user_id, token_id)
except ApiException as e:
    print("Exception when calling PublicApi->users_user_id_tokens_token_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 
 **token_id** | **str**| The token ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_token_id_get**
> TokenInfoResponse users_user_id_tokens_token_id_get(user_id, token_id)

Gets a specific token associated with a user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicApi()
user_id = 'user_id_example' # str | The user ID.  
token_id = 'token_id_example' # str | The token ID.  

try:
    # Gets a specific token associated with a user
    api_response = api_instance.users_user_id_tokens_token_id_get(user_id, token_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicApi->users_user_id_tokens_token_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 
 **token_id** | **str**| The token ID.   | 

### Return type

[**TokenInfoResponse**](TokenInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

