# swagger_client.ArtefactApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_artefacts_artefact_id_delete**](ArtefactApi.md#orgs_org_id_artefacts_artefact_id_delete) | **DELETE** /orgs/{orgId}/artefacts/{artefactId} | Delete Artefact and all related Artefact Versions
[**orgs_org_id_artefacts_get**](ArtefactApi.md#orgs_org_id_artefacts_get) | **GET** /orgs/{orgId}/artefacts | List all Artefacts.

# **orgs_org_id_artefacts_artefact_id_delete**
> orgs_org_id_artefacts_artefact_id_delete(org_id, artefact_id)

Delete Artefact and all related Artefact Versions

The specified Artefact and its Artefact Versions will be permanently deleted. Only Administrators can delete an Artefact.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  

try:
    # Delete Artefact and all related Artefact Versions
    api_instance.orgs_org_id_artefacts_artefact_id_delete(org_id, artefact_id)
except ApiException as e:
    print("Exception when calling ArtefactApi->orgs_org_id_artefacts_artefact_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_get**
> list[ArtefactResponse] orgs_org_id_artefacts_get(org_id, type=type, name=name)

List all Artefacts.

Returns the Artefacts registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactApi()
org_id = 'org_id_example' # str | The organization ID.  
type = 'type_example' # str | (Optional) Filter Artefacts by type.   (optional)
name = 'name_example' # str | (Optional) Filter Artefacts by name.   (optional)

try:
    # List all Artefacts.
    api_response = api_instance.orgs_org_id_artefacts_get(org_id, type=type, name=name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactApi->orgs_org_id_artefacts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **type** | **str**| (Optional) Filter Artefacts by type.   | [optional] 
 **name** | **str**| (Optional) Filter Artefacts by name.   | [optional] 

### Return type

[**list[ArtefactResponse]**](ArtefactResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

