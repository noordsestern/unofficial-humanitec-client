# SecretStoreRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**awssm** | [**AWSSMRequest**](AWSSMRequest.md) |  | [optional] 
**azurekv** | [**AzureKVRequest**](AzureKVRequest.md) |  | [optional] 
**created_at** | **str** |  | [optional] 
**created_by** | **str** |  | [optional] 
**gcpsm** | [**GCPSMRequest**](GCPSMRequest.md) |  | [optional] 
**humanitec** | [**HumanitecRequest**](HumanitecRequest.md) |  | [optional] 
**id** | **str** |  | [optional] 
**primary** | **bool** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**updated_by** | **str** |  | [optional] 
**vault** | [**VaultRequest**](VaultRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

