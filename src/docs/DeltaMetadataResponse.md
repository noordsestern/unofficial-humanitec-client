# DeltaMetadataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**archived** | **bool** |  | 
**contributers** | **list[str]** |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **str** |  | 
**env_id** | **str** |  | [optional] 
**last_modified_at** | **datetime** |  | 
**name** | **str** |  | [optional] 
**shared** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

