# ArtefactVersionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**archived** | **bool** | If the Artefact Version is archived. | [optional] 
**artefact_id** | **str** | The UUID of the Artefact. | 
**commit** | **str** | (Optional) The commit ID the Artefact Version was built on. | [optional] 
**digest** | **str** | (Optional) The Artefact Version digest. | [optional] 
**id** | **str** | The UUID of the Artefact Version. | 
**name** | **str** | The name of the Artefact. | 
**ref** | **str** | (Optional) The ref the Artefact Version was built from. | [optional] 
**version** | **str** | (Optional) The version of the Artefact Version. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

