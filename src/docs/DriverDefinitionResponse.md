# DriverDefinitionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_types** | **list[str]** | List of resources accounts types supported by the driver | 
**id** | **str** | The ID for this driver. Is used as &#x60;driver_type&#x60;. | 
**inputs_schema** | **dict(str, object)** | A JSON Schema specifying the driver-specific input parameters. | 
**org_id** | **str** | The Organization this driver exists under. Useful as public drivers are accessible to other orgs. | 
**target** | **str** | The prefix where the driver resides or, if the driver is a virtual driver, the reference to an existing driver using the &#x60;driver://&#x60; schema of the format &#x60;driver://{orgId}/{driverId}&#x60;. Only members of the organization the driver belongs to can see &#x60;target&#x60;. | [optional] 
**template** | **object** | If the driver is a virtual driver, template defines a Go template that converts the driver inputs supplied in the resource definition into the driver inputs for the target driver. | [optional] 
**type** | **str** | The type of resource produced by this driver | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

