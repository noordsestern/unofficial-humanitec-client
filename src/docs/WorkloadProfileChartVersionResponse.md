# WorkloadProfileChartVersionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** | Creation date | 
**created_by** | **str** | User created the profile | 
**org_id** | **str** | Organization ID | 
**id** | **str** | Workload Profile Chart Version ID | 
**version** | **str** | Version | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

