# swagger_client.ResourceProvisionApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_resources_graph_post**](ResourceProvisionApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_graph_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources/graph | Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.

# **orgs_org_id_apps_app_id_envs_env_id_resources_graph_post**
> list[NodeBodyResponse] orgs_org_id_apps_app_id_envs_env_id_resources_graph_post(body, org_id, app_id, env_id)

Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceProvisionApi()
body = [swagger_client.ResourceProvisionRequestRequest()] # list[ResourceProvisionRequestRequest] | Resources to provision.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Lists the resource objects that hold the information needed to provision the resources specified in the request and the resources they depend on.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_graph_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceProvisionApi->orgs_org_id_apps_app_id_envs_env_id_resources_graph_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[ResourceProvisionRequestRequest]**](ResourceProvisionRequestRequest.md)| Resources to provision.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[NodeBodyResponse]**](NodeBodyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

