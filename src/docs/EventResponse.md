# EventResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | **list[str]** | List of event properties which can be used as variables for this event | 
**scope** | **str** | Event scope | 
**type** | **str** | Event type | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

