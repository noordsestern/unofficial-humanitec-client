# swagger_client.MatchingCriteriaApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**](MatchingCriteriaApi.md#orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete) | **DELETE** /orgs/{orgId}/resources/defs/{defId}/criteria/{criteriaId} | Delete a Matching Criteria from a Resource Definition.
[**orgs_org_id_resources_defs_def_id_criteria_post**](MatchingCriteriaApi.md#orgs_org_id_resources_defs_def_id_criteria_post) | **POST** /orgs/{orgId}/resources/defs/{defId}/criteria | Add a new Matching Criteria to a Resource Definition.

# **orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**
> orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)

Delete a Matching Criteria from a Resource Definition.

If there **are no** Active Resources that would match to a different Resource Definition when the current Matching Criteria is deleted, the Matching Criteria is deleted immediately.  If there **are** Active Resources that would match to a different Resource Definition, the request fails with HTTP status code 409 (Conflict). The response content will list all of affected Active Resources and their new matches.  The request can take an optional `force` query parameter. If set to `true`, the Matching Criteria is deleted immediately. Referenced Active Resources would match to a different Resource Definition during the next deployment in the target environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.MatchingCriteriaApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  
criteria_id = 'criteria_id_example' # str | The Matching Criteria ID.  
force = true # bool | If set to `true`, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   (optional)

try:
    # Delete a Matching Criteria from a Resource Definition.
    api_instance.orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)
except ApiException as e:
    print("Exception when calling MatchingCriteriaApi->orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 
 **criteria_id** | **str**| The Matching Criteria ID.   | 
 **force** | **bool**| If set to &#x60;true&#x60;, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_criteria_post**
> MatchingCriteriaResponse orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)

Add a new Matching Criteria to a Resource Definition.

Matching Criteria are combined with Resource Type to select a specific definition. Matching Criteria can be set for any combination of Application ID, Environment ID, Environment Type, and Resource ID. In the event of multiple matches, the most specific match is chosen.  For example, given 3 sets of matching criteria for the same type:  ```  1. {\"env_type\":\"test\"}  2. {\"env_type\":\"development\"}  3. {\"env_type\":\"test\", \"app_id\":\"my-app\"} ```  If, a resource of that time was needed in an Application `my-app`, Environment `qa-team` with Type `test` and Resource ID `modules.my-module-externals.my-resource`, there would be two resource definitions matching the criteria: #1 & #3. Definition #3 will be chosen because its matching criteria is the most specific.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.MatchingCriteriaApi()
body = swagger_client.MatchingCriteriaRuleRequest() # MatchingCriteriaRuleRequest | Matching Criteria rules.


org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Add a new Matching Criteria to a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchingCriteriaApi->orgs_org_id_resources_defs_def_id_criteria_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MatchingCriteriaRuleRequest**](MatchingCriteriaRuleRequest.md)| Matching Criteria rules.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**MatchingCriteriaResponse**](MatchingCriteriaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

