# ImagesRegistryCredsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**images** | **list[str]** | List of images associated with the registry. | 
**registry** | **str** | Registry name, usually in a \&quot;{domain}\&quot; or \&quot;{domain}/{project}\&quot; format. | 
**secrets** | [**ClusterSecretsMapResponse**](ClusterSecretsMapResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

