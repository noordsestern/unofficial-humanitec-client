# UpdateResourceDefinitionRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driver_account** | **str** | (Optional) Security account required by the driver. | [optional] 
**driver_inputs** | [**ValuesSecretsRefsRequest**](ValuesSecretsRefsRequest.md) |  | [optional] 
**name** | **str** | The display name. | 
**provision** | [**dict(str, ProvisionDependenciesRequest)**](ProvisionDependenciesRequest.md) | (Optional) A map where the keys are resType#resId (if resId is omitted, the same id of the current resource definition is used) of the resources that should be provisioned when the current resource is provisioned. This also specifies if the resources have a dependency on the current resource or if they have the same dependent resources. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

