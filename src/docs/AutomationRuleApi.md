# swagger_client.AutomationRuleApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_rules_get**](AutomationRuleApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules | List all Automation Rules in an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_post**](AutomationRuleApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules | Create a new Automation Rule for an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete**](AutomationRuleApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Delete Automation Rule from an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get**](AutomationRuleApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Get a specific Automation Rule for an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put**](AutomationRuleApi.md#orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/rules/{ruleId} | Update an existing Automation Rule for an Environment.

# **orgs_org_id_apps_app_id_envs_env_id_rules_get**
> list[AutomationRuleResponse] orgs_org_id_apps_app_id_envs_env_id_rules_get(org_id, app_id, env_id)

List all Automation Rules in an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AutomationRuleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List all Automation Rules in an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutomationRuleApi->orgs_org_id_apps_app_id_envs_env_id_rules_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[AutomationRuleResponse]**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_post**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_post(body, org_id, app_id, env_id)

Create a new Automation Rule for an Environment.

Items marked as deprecated are still supported (however not recommended) for use and are incompatible with properties of the latest api version. In particular an error is raised if  `images_filter` (deprecated) and `artefacts_filter` are used in the same payload. The same is true for `exclude_images_filter` (deprecated) and `exclude_artefacts_filter`. `match` and `update_to` are still supported but will trigger an error if combined with `match_ref`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AutomationRuleApi()
body = swagger_client.AutomationRuleRequest() # AutomationRuleRequest | The definition of the Automation Rule.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Create a new Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutomationRuleApi->orgs_org_id_apps_app_id_envs_env_id_rules_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AutomationRuleRequest**](AutomationRuleRequest.md)| The definition of the Automation Rule.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete(org_id, app_id, env_id, rule_id)

Delete Automation Rule from an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AutomationRuleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Delete Automation Rule from an Environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete(org_id, app_id, env_id, rule_id)
except ApiException as e:
    print("Exception when calling AutomationRuleApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get(org_id, app_id, env_id, rule_id)

Get a specific Automation Rule for an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AutomationRuleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Get a specific Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get(org_id, app_id, env_id, rule_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutomationRuleApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put**
> AutomationRuleResponse orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put(body, org_id, app_id, env_id, rule_id)

Update an existing Automation Rule for an Environment.

Items marked as deprecated are still supported (however not recommended) for use and are incompatible with properties of the latest api version. In particular an error is raised if  `images_filter` (deprecated) and `artefacts_filter` are used in the same payload. The same is true for `exclude_images_filter` (deprecated) and `exclude_artefacts_filter`. `match` and `update_to` are still supported but will trigger an error if combined with `match_ref`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AutomationRuleApi()
body = swagger_client.AutomationRuleRequest() # AutomationRuleRequest | The definition of the Automation Rule.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
rule_id = 'rule_id_example' # str | The Automation Rule ID.  

try:
    # Update an existing Automation Rule for an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put(body, org_id, app_id, env_id, rule_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutomationRuleApi->orgs_org_id_apps_app_id_envs_env_id_rules_rule_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AutomationRuleRequest**](AutomationRuleRequest.md)| The definition of the Automation Rule.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **rule_id** | **str**| The Automation Rule ID.   | 

### Return type

[**AutomationRuleResponse**](AutomationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

