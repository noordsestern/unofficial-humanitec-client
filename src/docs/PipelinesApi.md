# swagger_client.PipelinesApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_pipeline**](PipelinesApi.md#create_pipeline) | **POST** /orgs/{orgId}/apps/{appId}/pipelines | Create a Pipeline within an Application.
[**delete_pipeline**](PipelinesApi.md#delete_pipeline) | **DELETE** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | Delete a pipeline within an application.
[**get_latest_pipeline_definition_schema**](PipelinesApi.md#get_latest_pipeline_definition_schema) | **GET** /orgs/{orgId}/pipeline-schemas/latest | Gets the latest pipeline schema
[**get_pipeline**](PipelinesApi.md#get_pipeline) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | Get a Pipeline within an Application.
[**get_pipeline_definition**](PipelinesApi.md#get_pipeline_definition) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/schema | Get a pipeline schema.
[**list_pipeline_versions**](PipelinesApi.md#list_pipeline_versions) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/versions | List all versions of the pipeline
[**list_pipelines**](PipelinesApi.md#list_pipelines) | **GET** /orgs/{orgId}/apps/{appId}/pipelines | List Pipelines within an Application.
[**list_pipelines_in_org**](PipelinesApi.md#list_pipelines_in_org) | **GET** /orgs/{orgId}/pipelines | List all Pipelines within an Organization. This can be filtered by Application.
[**update_pipeline**](PipelinesApi.md#update_pipeline) | **PATCH** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId} | update a Pipeline within an Application.

# **create_pipeline**
> Pipeline create_pipeline(body, org_id, app_id)

Create a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
body = NULL # object | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID

try:
    # Create a Pipeline within an Application.
    api_response = api_instance.create_pipeline(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->create_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**object**](object.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-yaml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_pipeline**
> delete_pipeline(org_id, app_id, pipeline_id, if_match=if_match)

Delete a pipeline within an application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Delete a pipeline within an application.
    api_instance.delete_pipeline(org_id, app_id, pipeline_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PipelinesApi->delete_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_latest_pipeline_definition_schema**
> object get_latest_pipeline_definition_schema(org_id)

Gets the latest pipeline schema

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID

try:
    # Gets the latest pipeline schema
    api_response = api_instance.get_latest_pipeline_definition_schema(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->get_latest_pipeline_definition_schema: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline**
> Pipeline get_pipeline(org_id, app_id, pipeline_id, version=version)

Get a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
version = 'version_example' # str | An optional Pipeline Version ID. (optional)

try:
    # Get a Pipeline within an Application.
    api_response = api_instance.get_pipeline(org_id, app_id, pipeline_id, version=version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->get_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **version** | **str**| An optional Pipeline Version ID. | [optional] 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_definition**
> object get_pipeline_definition(org_id, app_id, pipeline_id, version=version, accept=accept)

Get a pipeline schema.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
version = 'version_example' # str | An optional Pipeline Version ID. (optional)
accept = 'accept_example' # str |  (optional)

try:
    # Get a pipeline schema.
    api_response = api_instance.get_pipeline_definition(org_id, app_id, pipeline_id, version=version, accept=accept)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->get_pipeline_definition: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **version** | **str**| An optional Pipeline Version ID. | [optional] 
 **accept** | **str**|  | [optional] 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x.humanitec-pipelines-v1.0+yaml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_versions**
> list[PipelineVersion] list_pipeline_versions(org_id, app_id, pipeline_id, per_page=per_page, page=page)

List all versions of the pipeline

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List all versions of the pipeline
    api_response = api_instance.list_pipeline_versions(org_id, app_id, pipeline_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->list_pipeline_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[PipelineVersion]**](PipelineVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipelines**
> list[Pipeline] list_pipelines(org_id, app_id, per_page=per_page, page=page, trigger=trigger)

List Pipelines within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
trigger = 'trigger_example' # str | An optional filter by trigger type. (optional)

try:
    # List Pipelines within an Application.
    api_response = api_instance.list_pipelines(org_id, app_id, per_page=per_page, page=page, trigger=trigger)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->list_pipelines: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **trigger** | **str**| An optional filter by trigger type. | [optional] 

### Return type

[**list[Pipeline]**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipelines_in_org**
> list[Pipeline] list_pipelines_in_org(org_id, app=app, per_page=per_page, page=page, trigger=trigger)

List all Pipelines within an Organization. This can be filtered by Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
org_id = 'org_id_example' # str | The Organization ID
app = ['app_example'] # list[str] | An optional list of Application IDs. (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
trigger = 'trigger_example' # str | An optional filter by trigger type. (optional)

try:
    # List all Pipelines within an Organization. This can be filtered by Application.
    api_response = api_instance.list_pipelines_in_org(org_id, app=app, per_page=per_page, page=page, trigger=trigger)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->list_pipelines_in_org: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app** | [**list[str]**](str.md)| An optional list of Application IDs. | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **trigger** | **str**| An optional filter by trigger type. | [optional] 

### Return type

[**list[Pipeline]**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_pipeline**
> Pipeline update_pipeline(body, org_id, app_id, pipeline_id, if_match=if_match)

update a Pipeline within an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelinesApi()
body = NULL # object | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # update a Pipeline within an Application.
    api_response = api_instance.update_pipeline(body, org_id, app_id, pipeline_id, if_match=if_match)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelinesApi->update_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**object**](object.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-yaml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

