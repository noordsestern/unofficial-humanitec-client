# swagger_client.ValueSetVersionApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions | List Value Set Versions in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId} | Get a single Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/purge/{key} | Purge the value of a specific Shared Value from the App Environment Version history.
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/restore/{key} | Restore a specific key from the Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/value-set-versions/{valueSetVersionId}/restore | Restore a Value Set Version in an Environment of an App
[**orgs_org_id_apps_app_id_value_set_versions_get**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_value_set_versions_get) | **GET** /orgs/{orgId}/apps/{appId}/value-set-versions | List Value Set Versions in the App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get) | **GET** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId} | Get a single Value Set Version from the App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/purge/{key} | Purge the value of a specific Shared Value from the App Version history.
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/restore/{key} | Restore a specific key from the Value Set Version in an App
[**orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post**](ValueSetVersionApi.md#orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post) | **POST** /orgs/{orgId}/apps/{appId}/value-set-versions/{valueSetVersionId}/restore | Restore a Value Set Version in an App

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get**
> list[ValueSetVersionResponse] orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get(org_id, app_id, env_id, key_changed=key_changed)

List Value Set Versions in an Environment of an App

A new Value Set Version is created on every modification of a Value inside the an Environment of an App. In case this environment has no overrides the response is the same as the App level endpoint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key_changed = 'key_changed_example' # str | (Optional) Return only value set version where the specified key changed   (optional)

try:
    # List Value Set Versions in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get(org_id, app_id, env_id, key_changed=key_changed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key_changed** | **str**| (Optional) Return only value set version where the specified key changed   | [optional] 

### Return type

[**list[ValueSetVersionResponse]**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get(org_id, app_id, env_id, value_set_version_id)

Get a single Value Set Version in an Environment of an App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Get a single Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get(org_id, app_id, env_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post**
> orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, env_id, value_set_version_id, key)

Purge the value of a specific Shared Value from the App Environment Version history.

Purging permanently removes the value of a specific Shared Value in an application. A purged value is no longer accessible, can't be restored and can't be used by deployments referencing a Value Set Version where the value was present.  Learn more about purging in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#purge). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be purged.  

try:
    # Purge the value of a specific Shared Value from the App Environment Version history.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, env_id, value_set_version_id, key)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_purge_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be purged.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, env_id, value_set_version_id, key)

Restore a specific key from the Value Set Version in an Environment of an App

Restore the values of a single Shared Value in an Environment from a specific version.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be restored.  

try:
    # Restore a specific key from the Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, env_id, value_set_version_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be restored.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, env_id, value_set_version_id)

Restore a Value Set Version in an Environment of an App

Restore the values of all Shared Values in an environment from a specific version. Keys not existing in the selected version are deleted.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Restore a Value Set Version in an Environment of an App
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, env_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_envs_env_id_value_set_versions_value_set_version_id_restore_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_get**
> list[ValueSetVersionResponse] orgs_org_id_apps_app_id_value_set_versions_get(org_id, app_id, key_changed=key_changed)

List Value Set Versions in the App

A new Value Set Version is created on every modification of a Value inside the app.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key_changed = 'key_changed_example' # str | (Optional) Return only value set version where the specified key changed   (optional)

try:
    # List Value Set Versions in the App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_get(org_id, app_id, key_changed=key_changed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_value_set_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key_changed** | **str**| (Optional) Return only value set version where the specified key changed   | [optional] 

### Return type

[**list[ValueSetVersionResponse]**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get(org_id, app_id, value_set_version_id)

Get a single Value Set Version from the App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Get a single Value Set Version from the App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get(org_id, app_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post**
> orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, value_set_version_id, key)

Purge the value of a specific Shared Value from the App Version history.

Purging permanently removes the value of a specific Shared Value in an Application. A purged value is no longer accessible, can't be restored and can't be used by deployments referencing a Value Set Version where the value was present.  Learn more about purging in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#purge). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be purged.  

try:
    # Purge the value of a specific Shared Value from the App Version history.
    api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post(body, org_id, app_id, value_set_version_id, key)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_purge_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be purged.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, value_set_version_id, key)

Restore a specific key from the Value Set Version in an App

Restore the values of a single Shared Value in an application from a specific version.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  
key = 'key_example' # str | Key of the value to be restored.  

try:
    # Restore a specific key from the Value Set Version in an App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post(body, org_id, app_id, value_set_version_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_key_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 
 **key** | **str**| Key of the value to be restored.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post**
> ValueSetVersionResponse orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, value_set_version_id)

Restore a Value Set Version in an App

Restore the values of all Shared Values in an application from a specific version. Keys not existing in the selected version are deleted.  Learn more about reverting in our [docs](https://docs.humanitec.com/reference/concepts/app-config/shared-app-values#revert). 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueSetVersionApi()
body = swagger_client.ValueSetActionPayloadRequest() # ValueSetActionPayloadRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
value_set_version_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | The ValueSetVersion ID.  

try:
    # Restore a Value Set Version in an App
    api_response = api_instance.orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post(body, org_id, app_id, value_set_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueSetVersionApi->orgs_org_id_apps_app_id_value_set_versions_value_set_version_id_restore_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueSetActionPayloadRequest**](ValueSetActionPayloadRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **value_set_version_id** | [**str**](.md)| The ValueSetVersion ID.   | 

### Return type

[**ValueSetVersionResponse**](ValueSetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

