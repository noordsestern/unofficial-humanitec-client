# ApprovalRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The id of the approval object. | [optional] 
**org_id** | **str** | The id of the Organization. | [optional] 
**app_id** | **str** | The id of the Application. | [optional] 
**pipeline_id** | **str** | The id of the Pipeline. | [optional] 
**run_id** | **str** | The id of the Pipeline&#x27;s Run. | [optional] 
**job_id** | **str** | The id of the Run&#x27;s Job. | [optional] 
**env_id** | **str** | The environment for which the approver needs to have deploy permission to approve the job. | [optional] 
**message** | **str** | A human-readable message indicating the reason for approval. | [optional] 
**created_at** | **datetime** | The date and time when the approval request was created. | [optional] 
**status** | **str** | The current status of the approval request. | [optional] 
**approved_by** | **str** | The user who approved or denied the request. | [optional] 
**approved_at** | **datetime** | The date and time when the request was approved or denied. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

