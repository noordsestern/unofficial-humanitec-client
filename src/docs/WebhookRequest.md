# WebhookRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disabled** | **bool** | Defines whether this job is currently disabled. | [optional] 
**headers** | [**JSONFieldRequest**](JSONFieldRequest.md) |  | [optional] 
**id** | **str** | Job ID, unique within the Organization | [optional] 
**payload** | [**JSONFieldRequest**](JSONFieldRequest.md) |  | [optional] 
**triggers** | [**list[EventBaseRequest]**](EventBaseRequest.md) | A list of Events by which the Job is triggered | [optional] 
**url** | **str** | The webhook&#x27;s URL (without protocol, only HTTPS is supported). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

