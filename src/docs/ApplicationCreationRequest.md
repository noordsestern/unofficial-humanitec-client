# ApplicationCreationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**env** | [**EnvironmentBaseRequest**](EnvironmentBaseRequest.md) |  | [optional] 
**id** | **str** | The ID which refers to a specific application. | 
**name** | **str** | The Human-friendly name for the Application. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

