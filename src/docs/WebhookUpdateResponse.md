# WebhookUpdateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disabled** | **bool** | Defines whether this job is currently disabled. | 
**headers** | [**JSONFieldResponse**](JSONFieldResponse.md) |  | 
**payload** | [**JSONFieldResponse**](JSONFieldResponse.md) |  | 
**triggers** | [**list[EventBaseResponse]**](EventBaseResponse.md) | A list of Events by which the Job is triggered | 
**url** | **str** | The webhook&#x27;s URL (without protocol, only HTTPS is supported) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

