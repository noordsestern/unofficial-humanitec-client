# ResourceDefinitionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp of when this record has been created. | [optional] 
**created_by** | **str** | The user who created this record. | [optional] 
**criteria** | [**list[MatchingCriteriaRequest]**](MatchingCriteriaRequest.md) | (Optional) The criteria to use when looking for a Resource Definition during the deployment. | [optional] 
**driver_account** | **str** | (Optional) Security account required by the driver. | [optional] 
**driver_inputs** | [**ValuesSecretsRefsRequest**](ValuesSecretsRefsRequest.md) |  | [optional] 
**driver_type** | **str** | The driver to be used to create the resource. | [optional] 
**id** | **str** | The Resource Definition ID. | [optional] 
**is_default** | **bool** | Indicates this definition is a built-in one (provided by Humanitec). | [optional] 
**is_deleted** | **bool** | Indicates if this record has been marked for deletion. The Resource Definition that has been marked for deletion cannot be used to provision new resources. | [optional] 
**name** | **str** | The display name. | [optional] 
**org_id** | **str** | The Organization ID. | [optional] 
**provision** | [**dict(str, ProvisionDependenciesRequest)**](ProvisionDependenciesRequest.md) | (Optional) A map where the keys are resType#resId (if resId is omitted, the same id of the current resource definition is used) of the resources that should be provisioned when the current resource is provisioned. This also specifies if the resources have a dependency on the current resource. | [optional] 
**type** | **str** | The Resource Type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

