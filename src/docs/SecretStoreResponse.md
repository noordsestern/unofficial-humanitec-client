# SecretStoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**awssm** | [**AWSSMResponse**](AWSSMResponse.md) |  | [optional] 
**azurekv** | [**AzureKVResponse**](AzureKVResponse.md) |  | [optional] 
**created_at** | **str** |  | 
**created_by** | **str** |  | 
**gcpsm** | [**GCPSMResponse**](GCPSMResponse.md) |  | [optional] 
**humanitec** | [**HumanitecResponse**](HumanitecResponse.md) |  | [optional] 
**id** | **str** |  | 
**primary** | **bool** |  | 
**updated_at** | **str** |  | 
**updated_by** | **str** |  | 
**vault** | [**VaultResponse**](VaultResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

