# WorkloadProfileVersionSpecDefinitionRuntimeProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**WorkloadProfileVersionSpecDefinitionRuntimePropertyType**](WorkloadProfileVersionSpecDefinitionRuntimePropertyType.md) |  | 
**feature_name** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**ui_hints** | [**WorkloadProfileVersionSpecDefinitionPropertyUIHints**](WorkloadProfileVersionSpecDefinitionPropertyUIHints.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

