# EnvironmentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp in UTC of when the Environment was created. | 
**created_by** | **str** | The user who created the Environment | 
**from_deploy** | [**DeploymentResponse**](DeploymentResponse.md) |  | [optional] 
**id** | **str** | The ID the Environment is referenced as. | 
**last_deploy** | [**DeploymentResponse**](DeploymentResponse.md) |  | [optional] 
**name** | **str** | The Human-friendly name for the Environment. | 
**type** | **str** | The Environment Type. This is used for organizing and managing Environments. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

