# WorkloadProfileVersionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | **dict(str, object)** | A map of Features. If referencing built in Humanitec features, the fully qualified feature name must be used: e.g. &#x60;humanitec/annotations&#x60;.  {  } | [optional] 
**notes** | **str** | Notes | [optional] 
**spec_definition** | [**WorkloadProfileVersionSpecDefinition**](WorkloadProfileVersionSpecDefinition.md) |  | [optional] 
**version** | **str** | Version | 
**workload_profile_chart** | [**WorkloadProfileChartReference**](WorkloadProfileChartReference.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

