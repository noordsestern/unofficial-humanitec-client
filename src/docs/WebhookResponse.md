# WebhookResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp of when this Job was created. | [optional] 
**created_by** | **str** | The user who created this Job | [optional] 
**disabled** | **bool** | Defines whether this job is currently disabled. | 
**headers** | [**JSONFieldResponse**](JSONFieldResponse.md) |  | 
**id** | **str** | Job ID, unique within the Organization | 
**payload** | [**JSONFieldResponse**](JSONFieldResponse.md) |  | 
**triggers** | [**list[EventBaseResponse]**](EventBaseResponse.md) | A list of Events by which the Job is triggered | 
**url** | **str** | The webhook&#x27;s URL (without protocol, only HTTPS is supported). | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

