# UserRoleRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **str** | The role that this user holds | [optional] 
**user** | **str** | The user ID that hold the role | [optional] 
**created_at** | **str** | The time the user was first registered with Humanitec | [optional] 
**email** | **str** | The email address of the user from the profile | [optional] 
**id** | **str** | The User ID for this user | [optional] 
**invite** | **str** | The status of an invitation (If applicable) | [optional] 
**name** | **str** | The name the user goes by | [optional] 
**type** | **str** | The type of the account. Could be user, service or system | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

