# swagger_client.ResourceTypeApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_types_get**](ResourceTypeApi.md#orgs_org_id_resources_types_get) | **GET** /orgs/{orgId}/resources/types | List Resource Types.

# **orgs_org_id_resources_types_get**
> list[ResourceTypeResponse] orgs_org_id_resources_types_get(org_id)

List Resource Types.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceTypeApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Types.
    api_response = api_instance.orgs_org_id_resources_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceTypeApi->orgs_org_id_resources_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ResourceTypeResponse]**](ResourceTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

