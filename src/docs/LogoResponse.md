# LogoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dark_url** | **str** |  | [optional] 
**light_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

