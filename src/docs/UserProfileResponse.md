# UserProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The time the user was first registered with Humanitec | 
**email** | **str** | The email address of the user from the profile | [optional] 
**id** | **str** | The User ID for this user | 
**name** | **str** | The name the user goes by | 
**type** | **str** | The type of the account. Could be user, service or system | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

