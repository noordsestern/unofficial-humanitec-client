# swagger_client.PublicKeysApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_public_key**](PublicKeysApi.md#create_public_key) | **POST** /orgs/{orgId}/keys | Associate a new RSA public key to an organization.
[**delete_public_key**](PublicKeysApi.md#delete_public_key) | **DELETE** /orgs/{orgId}/keys/{keyId} | Delete a public key associated to an organization.
[**get_public_key**](PublicKeysApi.md#get_public_key) | **GET** /orgs/{orgId}/keys/{keyId} | Retrieve a single public key associated to an organization.
[**get_public_keys**](PublicKeysApi.md#get_public_keys) | **GET** /orgs/{orgId}/keys | List all public keys associated to an organization.

# **create_public_key**
> PublicKey create_public_key(body, org_id)

Associate a new RSA public key to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicKeysApi()
body = '\"-----BEGIN PUBLIC KEY-----\\\\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAo22jj/h+WPhL5M8RUyqC\\\\nEqRK3FOYD6KuDTyUHZL2QUX/q35bT1aGIOtu+22oM/8hTZ/6+BTknvF+Z+e7At8E\\\\n/I6/qNZilJFH/HqNHYflykceILVCFQM6v0ar6CSPh88DjwQPGLrh+UYp0NKBF6D5\\\\n2LEVQpxsM/0qV4fOZngRVI9UeKiYehk8aXJi20nI5Cj9GnV7BuEo7sKq7NewTOMN\\\\nwHqSnCtMV+E7SIgyy4+aKAFHOR0Y4FgJN14bAjE2GX/VUmAYBNtPgGwkCNDbA9v1\\\\nJJnFmdKflp8foqeWwC9UcUDLqjZzMT2a16pnL89iHV4fJ4vI6h26Jf2wUJbb7xE3\\\\nxJNhCR9Qf2IHx8TTVetIRl5I/ZUPkRrq5iskOIWV4+I+NzS+gbnCUv6sCO3cnZaa\\\\noZ4Wk5V+5qfctANeTn0TEAabYAJ/zFgYcs2IwYNqcN5eTss99zGNgtDL3oJr/A6g\\\\neGBgzad3p80qPMb1le64cHAAFZaerI2kcdnNpWGLXJ/J7IKF9uNfsrvi7Zdv8AXg\\\\nVAyqBARfzSkgnUKN6iWkWxAAzylKnQ3Etw81huaLwDG/6Lqd8I5OePMwHEZkollD\\\\nEBD16L/iyV9veru2zLVXkX/nL64YtZERLOWI5NUMaSurAR4N3ptFAsDRcO5Z4+Dq\\\\nO3pqszSh1aCyJvyl6cjQLT8CAwEAAQ==\\\\n-----END PUBLIC KEY-----\\\\n\"' # str | A pcks8 RSA public key PEM encoded (as the ones produced by openssl), whose module length is greater or equal than 4096 bits. It should be provided as a single line. This might be accomplished through the usage of `awk` bash tool: `awk -v ORS='\n' '1' public_key.pem`
org_id = 'org_id_example' # str | The organization ID.

try:
    # Associate a new RSA public key to an organization.
    api_response = api_instance.create_public_key(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicKeysApi->create_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| A pcks8 RSA public key PEM encoded (as the ones produced by openssl), whose module length is greater or equal than 4096 bits. It should be provided as a single line. This might be accomplished through the usage of &#x60;awk&#x60; bash tool: &#x60;awk -v ORS&#x3D;&#x27;\n&#x27; &#x27;1&#x27; public_key.pem&#x60; | 
 **org_id** | **str**| The organization ID. | 

### Return type

[**PublicKey**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_public_key**
> delete_public_key(org_id, key_id)

Delete a public key associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicKeysApi()
org_id = 'org_id_example' # str | The organization ID.
key_id = 'key_id_example' # str | The public key ID.

try:
    # Delete a public key associated to an organization.
    api_instance.delete_public_key(org_id, key_id)
except ApiException as e:
    print("Exception when calling PublicKeysApi->delete_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **key_id** | **str**| The public key ID. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_key**
> PublicKey get_public_key(org_id, key_id)

Retrieve a single public key associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicKeysApi()
org_id = 'org_id_example' # str | The organization ID.
key_id = 'key_id_example' # str | The public key ID.

try:
    # Retrieve a single public key associated to an organization.
    api_response = api_instance.get_public_key(org_id, key_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicKeysApi->get_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **key_id** | **str**| The public key ID. | 

### Return type

[**PublicKey**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_keys**
> list[PublicKey] get_public_keys(org_id, fingerprint=fingerprint)

List all public keys associated to an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicKeysApi()
org_id = 'org_id_example' # str | The organization ID.
fingerprint = 'fingerprint_example' # str | The fingerprint of the requested key. If a value is provided, the result will contain a single key, if any. (optional)

try:
    # List all public keys associated to an organization.
    api_response = api_instance.get_public_keys(org_id, fingerprint=fingerprint)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicKeysApi->get_public_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **fingerprint** | **str**| The fingerprint of the requested key. If a value is provided, the result will contain a single key, if any. | [optional] 

### Return type

[**list[PublicKey]**](PublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

