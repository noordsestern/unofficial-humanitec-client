# swagger_client.EnvironmentApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_delete**](EnvironmentApi.md#orgs_org_id_apps_app_id_envs_env_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId} | Delete a specific Environment.
[**orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put**](EnvironmentApi.md#orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/from_deploy_id | Rebase to a different Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_get**](EnvironmentApi.md#orgs_org_id_apps_app_id_envs_env_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId} | Get a specific Environment.
[**orgs_org_id_apps_app_id_envs_get**](EnvironmentApi.md#orgs_org_id_apps_app_id_envs_get) | **GET** /orgs/{orgId}/apps/{appId}/envs | List all Environments.
[**orgs_org_id_apps_app_id_envs_post**](EnvironmentApi.md#orgs_org_id_apps_app_id_envs_post) | **POST** /orgs/{orgId}/apps/{appId}/envs | Add a new Environment to an Application.

# **orgs_org_id_apps_app_id_envs_env_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_delete(org_id, app_id, env_id)

Delete a specific Environment.

Deletes a specific Environment in an Application.  Deleting an Environment will also delete the Deployment history of the Environment.  _Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Delete a specific Environment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_delete(org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling EnvironmentApi->orgs_org_id_apps_app_id_envs_env_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put**
> orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put(body, org_id, app_id, env_id)

Rebase to a different Deployment.

Rebasing an Environment means that the next Deployment to the Environment will be based on the Deployment specified in the rebase rather than the last one in the Environment. The Deployment to rebase to can either be current or a previous Deployment. The Deployment can be from any Environment of the same Application.  _Running code will only be affected on the next Deployment to the Environment._  Common use cases for rebasing an Environment:  * _Rollback_: Rebasing to a previous Deployment in the current Environment and then Deploying without additional changes will execute a rollback to the previous Deployment state.  * _Clone_: Rebasing to the current Deployment in a different Environment and then deploying without additional changes will clone all of the configuration of the other Environment into the current one. (NOTE: External Resources will not be cloned in the process - the current External Resources of the Environment will remain unchanged and will be used by the deployed Application in the Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentApi()
body = 'body_example' # str | The Deployment ID to rebase to.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Rebase to a different Deployment.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put(body, org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling EnvironmentApi->orgs_org_id_apps_app_id_envs_env_id_from_deploy_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The Deployment ID to rebase to.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_get**
> EnvironmentResponse orgs_org_id_apps_app_id_envs_env_id_get(org_id, app_id, env_id)

Get a specific Environment.

Gets a specific Environment in an Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Get a specific Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentApi->orgs_org_id_apps_app_id_envs_env_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**EnvironmentResponse**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_get**
> list[EnvironmentResponse] orgs_org_id_apps_app_id_envs_get(org_id, app_id)

List all Environments.

Lists all of the Environments in the Application.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List all Environments.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentApi->orgs_org_id_apps_app_id_envs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[EnvironmentResponse]**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_post**
> EnvironmentResponse orgs_org_id_apps_app_id_envs_post(body, org_id, app_id)

Add a new Environment to an Application.

Creates a new Environment of the specified Type and associates it with the Application specified by `appId`.  The Environment is also initialized to the **current or past state of Deployment in another Environment**. This ensures that every Environment is derived from a previously known state. This means it is not possible to create a new Environment for an Application until at least one Deployment has occurred. (The Deployment does not have to be successful.)  The Type of the Environment must be already defined in the Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EnvironmentApi()
body = swagger_client.EnvironmentDefinitionRequest() # EnvironmentDefinitionRequest | The ID, Name, Type, and Deployment the Environment will be derived from.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Add a new Environment to an Application.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnvironmentApi->orgs_org_id_apps_app_id_envs_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnvironmentDefinitionRequest**](EnvironmentDefinitionRequest.md)| The ID, Name, Type, and Deployment the Environment will be derived from.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**EnvironmentResponse**](EnvironmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

