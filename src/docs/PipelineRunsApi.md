# swagger_client.PipelineRunsApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_run**](PipelineRunsApi.md#cancel_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/cancel | Cancel a Run within an Pipeline.
[**create_pipeline_run**](PipelineRunsApi.md#create_pipeline_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs | Create a run within a pipeline.
[**delete_run**](PipelineRunsApi.md#delete_run) | **DELETE** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId} | Deleting a completed Run within a Pipeline.
[**get_run**](PipelineRunsApi.md#get_run) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId} | Get a run within an pipeline.
[**get_run_job**](PipelineRunsApi.md#get_run_job) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId} | List the details of a Job including Step information
[**list_pipeline_runs**](PipelineRunsApi.md#list_pipeline_runs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs | List runs within a pipeline.
[**list_pipeline_runs_by_org**](PipelineRunsApi.md#list_pipeline_runs_by_org) | **GET** /orgs/{orgId}/pipeline-runs | List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.
[**list_run_job_step_logs**](PipelineRunsApi.md#list_run_job_step_logs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/steps/{stepIndex}/logs | Get a page of log output for a given step within a job.
[**list_run_jobs**](PipelineRunsApi.md#list_run_jobs) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs | List the details of the jobs with in a pipeline run.
[**restart_run**](PipelineRunsApi.md#restart_run) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/restart | Restart a Run within an Pipeline by cloning it with the same trigger and inputs.

# **cancel_run**
> cancel_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)

Cancel a Run within an Pipeline.

Attempts to cancel the specified Run. If the Run is in a queued state, this cancellation will be applied immediately. If the Run is executing, the cancellation will be stored and will be resolved by the next Job or Step that supports in-flight cancellation. Runs that are in any other state, are not cancellable. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Cancel a Run within an Pipeline.
    api_instance.cancel_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->cancel_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_pipeline_run**
> RunResponse create_pipeline_run(body, org_id, app_id, pipeline_id, idempotency_key=idempotency_key)

Create a run within a pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
body = swagger_client.RunCreateRequest() # RunCreateRequest | 
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
idempotency_key = 'idempotency_key_example' # str | The HTTP Idempotency-Key (optional)

try:
    # Create a run within a pipeline.
    api_response = api_instance.create_pipeline_run(body, org_id, app_id, pipeline_id, idempotency_key=idempotency_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->create_pipeline_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RunCreateRequest**](RunCreateRequest.md)|  | 
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **idempotency_key** | **str**| The HTTP Idempotency-Key | [optional] 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_run**
> delete_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)

Deleting a completed Run within a Pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
if_match = 'if_match_example' # str | Indicate that the request should only succeed if there is an etag match (optional)

try:
    # Deleting a completed Run within a Pipeline.
    api_instance.delete_run(org_id, app_id, pipeline_id, run_id, if_match=if_match)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->delete_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **if_match** | **str**| Indicate that the request should only succeed if there is an etag match | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_run**
> RunResponse get_run(org_id, app_id, pipeline_id, run_id)

Get a run within an pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID

try:
    # Get a run within an pipeline.
    api_response = api_instance.get_run(org_id, app_id, pipeline_id, run_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->get_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_run_job**
> RunJobResponse get_run_job(org_id, app_id, pipeline_id, run_id, job_id)

List the details of a Job including Step information

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID

try:
    # List the details of a Job including Step information
    api_response = api_instance.get_run_job(org_id, app_id, pipeline_id, run_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->get_run_job: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 

### Return type

[**RunJobResponse**](RunJobResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_runs**
> list[RunResponse] list_pipeline_runs(org_id, app_id, pipeline_id, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)

List runs within a pipeline.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
status = ['status_example'] # list[str] | Optional filter by status. (optional)
completed = true # bool | Optional filer by completed or not. (optional)
created_after = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation after date time. (optional)
created_before = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation before date time (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List runs within a pipeline.
    api_response = api_instance.list_pipeline_runs(org_id, app_id, pipeline_id, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->list_pipeline_runs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **completed** | **bool**| Optional filer by completed or not. | [optional] 
 **created_after** | **datetime**| Optional filter by creation after date time. | [optional] 
 **created_before** | **datetime**| Optional filter by creation before date time | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunResponse]**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_pipeline_runs_by_org**
> list[RunResponse] list_pipeline_runs_by_org(org_id, app=app, pipeline=pipeline, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)

List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app = ['app_example'] # list[str] | An optional list of Application IDs. (optional)
pipeline = ['pipeline_example'] # list[str] | An optional list of Pipeline IDs. (optional)
status = ['status_example'] # list[str] | Optional filter by status. (optional)
completed = true # bool | Optional filer by completed or not. (optional)
created_after = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation after date time. (optional)
created_before = '2013-10-20T19:20:30+01:00' # datetime | Optional filter by creation before date time (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List all pipeline runs within the Org. This can be filtered by app, pipeline, and status.
    api_response = api_instance.list_pipeline_runs_by_org(org_id, app=app, pipeline=pipeline, status=status, completed=completed, created_after=created_after, created_before=created_before, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->list_pipeline_runs_by_org: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app** | [**list[str]**](str.md)| An optional list of Application IDs. | [optional] 
 **pipeline** | [**list[str]**](str.md)| An optional list of Pipeline IDs. | [optional] 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **completed** | **bool**| Optional filer by completed or not. | [optional] 
 **created_after** | **datetime**| Optional filter by creation after date time. | [optional] 
 **created_before** | **datetime**| Optional filter by creation before date time | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunResponse]**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_run_job_step_logs**
> list[RunJobStepLog] list_run_job_step_logs(org_id, app_id, pipeline_id, run_id, job_id, step_index, page=page)

Get a page of log output for a given step within a job.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
step_index = 56 # int | The index of the step within the Job
page = 'page_example' # str | The page token to request from (optional)

try:
    # Get a page of log output for a given step within a job.
    api_response = api_instance.list_run_job_step_logs(org_id, app_id, pipeline_id, run_id, job_id, step_index, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->list_run_job_step_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **step_index** | **int**| The index of the step within the Job | 
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunJobStepLog]**](RunJobStepLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_run_jobs**
> list[RunJobListResponse] list_run_jobs(org_id, app_id, pipeline_id, run_id, status=status, per_page=per_page, page=page)

List the details of the jobs with in a pipeline run.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
status = ['status_example'] # list[str] | Optional filter by status. (optional)
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List the details of the jobs with in a pipeline run.
    api_response = api_instance.list_run_jobs(org_id, app_id, pipeline_id, run_id, status=status, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->list_run_jobs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **status** | [**list[str]**](str.md)| Optional filter by status. | [optional] 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[RunJobListResponse]**](RunJobListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **restart_run**
> RunResponse restart_run(org_id, app_id, pipeline_id, run_id, idempotency_key=idempotency_key)

Restart a Run within an Pipeline by cloning it with the same trigger and inputs.

Attempts to copy and restart the specified Run. The run must be in a completed state. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineRunsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
idempotency_key = 'idempotency_key_example' # str | The HTTP Idempotency-Key (optional)

try:
    # Restart a Run within an Pipeline by cloning it with the same trigger and inputs.
    api_response = api_instance.restart_run(org_id, app_id, pipeline_id, run_id, idempotency_key=idempotency_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunsApi->restart_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **idempotency_key** | **str**| The HTTP Idempotency-Key | [optional] 

### Return type

[**RunResponse**](RunResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

