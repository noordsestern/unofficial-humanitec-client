# PipelineVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The unique id of the current Pipeline Version. | 
**org_id** | **str** | The id of the Organization containing this Run. | 
**app_id** | **str** | The id of the Application containing this Run. | 
**pipeline_id** | **str** | The id of the Pipeline associated with the Run. | 
**created_by** | **str** | User id of the pipeline version. | 
**created_at** | **datetime** | The date and time when the specific pipeline version was created. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

