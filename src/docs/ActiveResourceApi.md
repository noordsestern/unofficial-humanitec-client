# swagger_client.ActiveResourceApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_resources_get**](ActiveResourceApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources | List Active Resources provisioned in an environment.
[**orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete**](ActiveResourceApi.md#orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/resources/{type}/{resId} | Delete Active Resources.
[**orgs_org_id_resources_defs_def_id_resources_get**](ActiveResourceApi.md#orgs_org_id_resources_defs_def_id_resources_get) | **GET** /orgs/{orgId}/resources/defs/{defId}/resources | List Active Resources provisioned via a specific Resource Definition.

# **orgs_org_id_apps_app_id_envs_env_id_resources_get**
> list[ActiveResourceResponse] orgs_org_id_apps_app_id_envs_env_id_resources_get(org_id, app_id, env_id)

List Active Resources provisioned in an environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ActiveResourceApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Active Resources provisioned in an environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActiveResourceApi->orgs_org_id_apps_app_id_envs_env_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[ActiveResourceResponse]**](ActiveResourceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete**
> orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete(org_id, app_id, env_id, type, res_id)

Delete Active Resources.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ActiveResourceApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
type = 'type_example' # str | The Resource Type, may include a resource class: {type}.{class}.  
res_id = 'res_id_example' # str | The Resource ID.  

try:
    # Delete Active Resources.
    api_instance.orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete(org_id, app_id, env_id, type, res_id)
except ApiException as e:
    print("Exception when calling ActiveResourceApi->orgs_org_id_apps_app_id_envs_env_id_resources_type_res_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **type** | **str**| The Resource Type, may include a resource class: {type}.{class}.   | 
 **res_id** | **str**| The Resource ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_resources_get**
> list[ActiveResourceResponse] orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)

List Active Resources provisioned via a specific Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ActiveResourceApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # List Active Resources provisioned via a specific Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActiveResourceApi->orgs_org_id_resources_defs_def_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**list[ActiveResourceResponse]**](ActiveResourceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

