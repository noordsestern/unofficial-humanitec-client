# swagger_client.ValueApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_values_delete**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | Delete all Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_get**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | List Shared Values in an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_delete**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Delete Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_patch**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Update Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_key_put**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_key_put) | **PUT** /orgs/{orgId}/apps/{appId}/envs/{envId}/values/{key} | Update Shared Value for an Environment
[**orgs_org_id_apps_app_id_envs_env_id_values_post**](ValueApi.md#orgs_org_id_apps_app_id_envs_env_id_values_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/values | Create a Shared Value for an Environment
[**orgs_org_id_apps_app_id_values_delete**](ValueApi.md#orgs_org_id_apps_app_id_values_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/values | Delete all Shared Value for an App
[**orgs_org_id_apps_app_id_values_get**](ValueApi.md#orgs_org_id_apps_app_id_values_get) | **GET** /orgs/{orgId}/apps/{appId}/values | List Shared Values in an Application
[**orgs_org_id_apps_app_id_values_key_delete**](ValueApi.md#orgs_org_id_apps_app_id_values_key_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/values/{key} | Delete Shared Value for an Application
[**orgs_org_id_apps_app_id_values_key_patch**](ValueApi.md#orgs_org_id_apps_app_id_values_key_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/values/{key} | Update Shared Value for an Application
[**orgs_org_id_apps_app_id_values_key_put**](ValueApi.md#orgs_org_id_apps_app_id_values_key_put) | **PUT** /orgs/{orgId}/apps/{appId}/values/{key} | Update Shared Value for an Application
[**orgs_org_id_apps_app_id_values_post**](ValueApi.md#orgs_org_id_apps_app_id_values_post) | **POST** /orgs/{orgId}/apps/{appId}/values | Create a Shared Value for an Application

# **orgs_org_id_apps_app_id_envs_env_id_values_delete**
> orgs_org_id_apps_app_id_envs_env_id_values_delete(org_id, app_id, env_id)

Delete all Shared Value for an Environment

All Shared Values will be deleted. If the Shared Values are marked as a secret, they will also be deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Delete all Shared Value for an Environment
    api_instance.orgs_org_id_apps_app_id_envs_env_id_values_delete(org_id, app_id, env_id)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_get**
> list[ValueResponse] orgs_org_id_apps_app_id_envs_env_id_values_get(org_id, app_id, env_id)

List Shared Values in an Environment

The returned values will be the base Application values with the Environment overrides where applicable. The `source` field will specify the level from which the value is from.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Shared Values in an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[ValueResponse]**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_delete**
> orgs_org_id_apps_app_id_envs_env_id_values_key_delete(org_id, app_id, env_id, key)

Delete Shared Value for an Environment

The specified Shared Value will be permanently deleted. If the Shared Value is marked as a secret, it will also be permanently deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Delete Shared Value for an Environment
    api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_delete(org_id, app_id, env_id, key)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_key_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_patch**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_key_patch(body, org_id, app_id, env_id, key)

Update Shared Value for an Environment

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValuePatchPayloadRequest() # ValuePatchPayloadRequest | At least `value` or `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_patch(body, org_id, app_id, env_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_key_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValuePatchPayloadRequest**](ValuePatchPayloadRequest.md)| At least &#x60;value&#x60; or &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_key_put**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_key_put(body, org_id, app_id, env_id, key)

Update Shared Value for an Environment

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValueEditPayloadRequest() # ValueEditPayloadRequest | Both `value` and `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_key_put(body, org_id, app_id, env_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_key_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueEditPayloadRequest**](ValueEditPayloadRequest.md)| Both &#x60;value&#x60; and &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_values_post**
> ValueResponse orgs_org_id_apps_app_id_envs_env_id_values_post(body, org_id, app_id, env_id)

Create a Shared Value for an Environment

The Shared Value created will only be available to the specific Environment.  If a Value is marked as a secret, it will be securely stored. It will not be possible to retrieve the value again through the API. The value of the secret can however be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValueCreatePayloadRequest() # ValueCreatePayloadRequest | Definition of the new Shared Value.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Create a Shared Value for an Environment
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_values_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_envs_env_id_values_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueCreatePayloadRequest**](ValueCreatePayloadRequest.md)| Definition of the new Shared Value.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_delete**
> orgs_org_id_apps_app_id_values_delete(org_id, app_id)

Delete all Shared Value for an App

All Shared Values will be deleted. If the Shared Values are marked as a secret, they will also be deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Delete all Shared Value for an App
    api_instance.orgs_org_id_apps_app_id_values_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_get**
> list[ValueResponse] orgs_org_id_apps_app_id_values_get(org_id, app_id)

List Shared Values in an Application

The returned values will be the \"base\" values for the Application. The overridden value for the Environment can be retrieved via the `/orgs/{orgId}/apps/{appId}/envs/{envId}/values` endpoint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Shared Values in an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[ValueResponse]**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_delete**
> orgs_org_id_apps_app_id_values_key_delete(org_id, app_id, key)

Delete Shared Value for an Application

The specified Shared Value will be permanently deleted. If the Shared Value is marked as a secret, it will also be permanently deleted.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Delete Shared Value for an Application
    api_instance.orgs_org_id_apps_app_id_values_key_delete(org_id, app_id, key)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_key_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_patch**
> ValueResponse orgs_org_id_apps_app_id_values_key_patch(body, org_id, app_id, key)

Update Shared Value for an Application

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValuePatchPayloadRequest() # ValuePatchPayloadRequest | At least `value` or `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_key_patch(body, org_id, app_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_key_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValuePatchPayloadRequest**](ValuePatchPayloadRequest.md)| At least &#x60;value&#x60; or &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_key_put**
> ValueResponse orgs_org_id_apps_app_id_values_key_put(body, org_id, app_id, key)

Update Shared Value for an Application

Update the value or description of the Shared Value. Shared Values marked as secret can also be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValueEditPayloadRequest() # ValueEditPayloadRequest | Both `value` and `description` must be supplied. All other fields will be ignored.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
key = 'key_example' # str | The key to update.  

try:
    # Update Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_key_put(body, org_id, app_id, key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_key_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueEditPayloadRequest**](ValueEditPayloadRequest.md)| Both &#x60;value&#x60; and &#x60;description&#x60; must be supplied. All other fields will be ignored.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **key** | **str**| The key to update.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_values_post**
> ValueResponse orgs_org_id_apps_app_id_values_post(body, org_id, app_id)

Create a Shared Value for an Application

The Shared Value created will be available to all Environments in that Application.  If a Value is marked as a secret, it will be securely stored. It will not be possible to retrieve the value again through the API. The value of the secret can however be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValueApi()
body = swagger_client.ValueCreatePayloadRequest() # ValueCreatePayloadRequest | Definition of the new Shared Value.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a Shared Value for an Application
    api_response = api_instance.orgs_org_id_apps_app_id_values_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValueApi->orgs_org_id_apps_app_id_values_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ValueCreatePayloadRequest**](ValueCreatePayloadRequest.md)| Definition of the new Shared Value.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**ValueResponse**](ValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

