# swagger_client.OrganizationApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_get**](OrganizationApi.md#orgs_get) | **GET** /orgs | List active organizations the user has access to.
[**orgs_org_id_get**](OrganizationApi.md#orgs_org_id_get) | **GET** /orgs/{orgId} | Get the specified Organization.

# **orgs_get**
> list[OrganizationResponse] orgs_get()

List active organizations the user has access to.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.OrganizationApi()

try:
    # List active organizations the user has access to.
    api_response = api_instance.orgs_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->orgs_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[OrganizationResponse]**](OrganizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_get**
> OrganizationResponse orgs_org_id_get(org_id)

Get the specified Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.OrganizationApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Get the specified Organization.
    api_response = api_instance.orgs_org_id_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->orgs_org_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**OrganizationResponse**](OrganizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

