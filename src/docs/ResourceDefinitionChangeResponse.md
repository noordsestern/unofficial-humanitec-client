# ResourceDefinitionChangeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | **str** | The ID of the App the resource is associated with. | 
**env_id** | **str** | The ID of the Environment the resource is associated with. | 
**from_def** | **str** | The Resource Definition that this resource was provisioned from. | 
**res_id** | **str** | The ID of the resource | 
**to_def** | **str** | The Resource Definition that resource *will be* provisioned from if the change is applied. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

