# swagger_client.SetApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_sets**](SetApi.md#get_sets) | **GET** /orgs/{orgId}/apps/{appId}/sets | Get all Deployment Sets
[**orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get**](SetApi.md#orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get) | **GET** /orgs/{orgId}/apps/{appId}/sets/{setId}/diff/{sourceSetId} | Get the difference between 2 Deployment Sets
[**orgs_org_id_apps_app_id_sets_set_id_get**](SetApi.md#orgs_org_id_apps_app_id_sets_set_id_get) | **GET** /orgs/{orgId}/apps/{appId}/sets/{setId} | Get a Deployment Set
[**orgs_org_id_apps_app_id_sets_set_id_post**](SetApi.md#orgs_org_id_apps_app_id_sets_set_id_post) | **POST** /orgs/{orgId}/apps/{appId}/sets/{setId} | Apply a Deployment Delta to a Deployment Set

# **get_sets**
> list[SetResponse] get_sets(org_id, app_id)

Get all Deployment Sets

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SetApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Get all Deployment Sets
    api_response = api_instance.get_sets(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SetApi->get_sets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[SetResponse]**](SetResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get**
> PlainDeltaResponse orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get(org_id, app_id, set_id, source_set_id)

Get the difference between 2 Deployment Sets

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SetApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  
source_set_id = 'source_set_id_example' # str | ID of the Deployment Set to diff against.  

try:
    # Get the difference between 2 Deployment Sets
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get(org_id, app_id, set_id, source_set_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SetApi->orgs_org_id_apps_app_id_sets_set_id_diff_source_set_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 
 **source_set_id** | **str**| ID of the Deployment Set to diff against.   | 

### Return type

[**PlainDeltaResponse**](PlainDeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_get**
> InlineResponse2001 orgs_org_id_apps_app_id_sets_set_id_get(org_id, app_id, set_id, diff=diff)

Get a Deployment Set

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SetApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  
diff = 'diff_example' # str | ID of the Deployment Set to compared against. (optional)

try:
    # Get a Deployment Set
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_get(org_id, app_id, set_id, diff=diff)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SetApi->orgs_org_id_apps_app_id_sets_set_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 
 **diff** | **str**| ID of the Deployment Set to compared against. | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_sets_set_id_post**
> str orgs_org_id_apps_app_id_sets_set_id_post(body, org_id, app_id, set_id)

Apply a Deployment Delta to a Deployment Set

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SetApi()
body = swagger_client.DeltaRequest() # DeltaRequest | The Delta to apply to the Set.

NOTE: The `id` parameter is ignored if provided. The request body should be the full Delta.
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
set_id = 'set_id_example' # str | ID of the Deployment Set.  

try:
    # Apply a Deployment Delta to a Deployment Set
    api_response = api_instance.orgs_org_id_apps_app_id_sets_set_id_post(body, org_id, app_id, set_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SetApi->orgs_org_id_apps_app_id_sets_set_id_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| The Delta to apply to the Set.

NOTE: The &#x60;id&#x60; parameter is ignored if provided. The request body should be the full Delta. | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **set_id** | **str**| ID of the Deployment Set.   | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

