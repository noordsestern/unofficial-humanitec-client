# swagger_client.SecretStoreApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_secretstores_get**](SecretStoreApi.md#orgs_org_id_secretstores_get) | **GET** /orgs/{orgId}/secretstores | Get list of Secret Stores for the given organization.
[**orgs_org_id_secretstores_post**](SecretStoreApi.md#orgs_org_id_secretstores_post) | **POST** /orgs/{orgId}/secretstores | Create a Secret Store for the given organization.
[**orgs_org_id_secretstores_store_id_delete**](SecretStoreApi.md#orgs_org_id_secretstores_store_id_delete) | **DELETE** /orgs/{orgId}/secretstores/{storeId} | Delete the Secret Store.
[**orgs_org_id_secretstores_store_id_get**](SecretStoreApi.md#orgs_org_id_secretstores_store_id_get) | **GET** /orgs/{orgId}/secretstores/{storeId} | Get the Secret Store.
[**orgs_org_id_secretstores_store_id_patch**](SecretStoreApi.md#orgs_org_id_secretstores_store_id_patch) | **PATCH** /orgs/{orgId}/secretstores/{storeId} | Update the Secret Store.

# **orgs_org_id_secretstores_get**
> list[SecretStoreResponse] orgs_org_id_secretstores_get(org_id)

Get list of Secret Stores for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SecretStoreApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Get list of Secret Stores for the given organization.
    api_response = api_instance.orgs_org_id_secretstores_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecretStoreApi->orgs_org_id_secretstores_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[SecretStoreResponse]**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_post**
> SecretStoreResponse orgs_org_id_secretstores_post(body, org_id)

Create a Secret Store for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SecretStoreApi()
body = swagger_client.CreateSecretStorePayloadRequest() # CreateSecretStorePayloadRequest | Secret Store data.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a Secret Store for the given organization.
    api_response = api_instance.orgs_org_id_secretstores_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecretStoreApi->orgs_org_id_secretstores_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateSecretStorePayloadRequest**](CreateSecretStorePayloadRequest.md)| Secret Store data.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_delete**
> orgs_org_id_secretstores_store_id_delete(org_id, store_id)

Delete the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SecretStoreApi()
org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Delete the Secret Store.
    api_instance.orgs_org_id_secretstores_store_id_delete(org_id, store_id)
except ApiException as e:
    print("Exception when calling SecretStoreApi->orgs_org_id_secretstores_store_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_get**
> SecretStoreResponse orgs_org_id_secretstores_store_id_get(org_id, store_id)

Get the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SecretStoreApi()
org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Get the Secret Store.
    api_response = api_instance.orgs_org_id_secretstores_store_id_get(org_id, store_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecretStoreApi->orgs_org_id_secretstores_store_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_secretstores_store_id_patch**
> SecretStoreResponse orgs_org_id_secretstores_store_id_patch(body, org_id, store_id)

Update the Secret Store.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SecretStoreApi()
body = swagger_client.UpdateSecretStorePayloadRequest() # UpdateSecretStorePayloadRequest | Secret Store data.


org_id = 'org_id_example' # str | The Organization ID.  
store_id = 'store_id_example' # str | The Secret Store ID.  

try:
    # Update the Secret Store.
    api_response = api_instance.orgs_org_id_secretstores_store_id_patch(body, org_id, store_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecretStoreApi->orgs_org_id_secretstores_store_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateSecretStorePayloadRequest**](UpdateSecretStorePayloadRequest.md)| Secret Store data.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **store_id** | **str**| The Secret Store ID.   | 

### Return type

[**SecretStoreResponse**](SecretStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

