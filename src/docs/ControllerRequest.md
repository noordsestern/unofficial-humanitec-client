# ControllerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **str** |  | [optional] 
**message** | **str** |  | [optional] 
**pods** | [**list[PodStateRequest]**](PodStateRequest.md) |  | [optional] 
**replicas** | **int** |  | [optional] 
**revision** | **int** |  | [optional] 
**status** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

