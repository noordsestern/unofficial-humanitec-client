# ModuleDeltasRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add** | [**dict(str, ModuleRequest)**](ModuleRequest.md) |  | [optional] 
**remove** | **list[str]** |  | [optional] 
**update** | **dict(str, list[UpdateActionRequest])** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

