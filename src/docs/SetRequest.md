# SetRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modules** | [**dict(str, ModuleRequest)**](ModuleRequest.md) | The Modules that make up the Set | [optional] 
**shared** | **dict(str, object)** | Resources that are shared across the set | [optional] 
**version** | **int** | The version of the Deployment Set Schema to use. (Currently, only 0 is supported, and if omitted, version 0 is assumed.) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

