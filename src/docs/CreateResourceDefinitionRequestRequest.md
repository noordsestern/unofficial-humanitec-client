# CreateResourceDefinitionRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**criteria** | [**list[MatchingCriteriaRequest]**](MatchingCriteriaRequest.md) | (Optional) The criteria to use when looking for a Resource Definition during the deployment. | [optional] 
**driver_account** | **str** | (Optional) Security account required by the driver. | [optional] 
**driver_inputs** | [**ValuesSecretsRefsRequest**](ValuesSecretsRefsRequest.md) |  | [optional] 
**driver_type** | **str** | The driver to be used to create the resource. | 
**id** | **str** | The Resource Definition ID. | 
**name** | **str** | The display name. | 
**provision** | [**dict(str, ProvisionDependenciesRequest)**](ProvisionDependenciesRequest.md) | (Optional) A map where the keys are resType#resId (if resId is omitted, the same id of the current resource definition is used) of the resources that should be provisioned when the current resource is provisioned. This also specifies if the resources have a dependency on the current resource. | [optional] 
**type** | **str** | The Resource Type. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

