# ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **str** | A short code representing the class of error. This code can be used for tracking and observability or to find appropriate troubleshooting documentation.  | 
**message** | **str** | A human-readable explanation of the error. | 
**details** | **dict(str, object)** | An optional payload of metadata associated with the error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

