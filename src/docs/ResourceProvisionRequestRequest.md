# ResourceProvisionRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_class** | **str** | (Optional) A resource class | [optional] 
**id** | **str** |  | 
**resource** | **dict(str, object)** | (Optional) The input parameters for the resource passed from the deployment set. | [optional] 
**type** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

