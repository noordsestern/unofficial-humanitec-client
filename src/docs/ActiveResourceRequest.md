# ActiveResourceRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | **str** | The ID of the App the resource is associated with. | [optional] 
**_class** | **str** | The Resource Class of the resource | [optional] 
**criteria_id** | **str** | The Matching Criteria ID. | [optional] 
**def_id** | **str** | The Resource Definition that this resource was provisioned from. | [optional] 
**deploy_id** | **str** | The deployment that the resource was last provisioned in. | [optional] 
**driver_type** | **str** | The driver to be used to create the resource. | [optional] 
**env_id** | **str** | The ID of the Environment the resource is associated with. | [optional] 
**env_type** | **str** | The Environment Type of the Environment specified by env_id. | [optional] 
**gu_res_id** | **str** | Globally unique resource id | [optional] 
**org_id** | **str** | the ID of the Organization the Active Resource is associated with. | [optional] 
**res_id** | **str** | The ID of the resource | [optional] 
**resource** | **dict(str, object)** | The data that the resource passes into the deployment (&#x27;values&#x27; only). | [optional] 
**status** | **str** | Current resource status: &#x27;pending&#x27;, &#x27;active&#x27;, or &#x27;deleting&#x27;. | [optional] 
**type** | **str** | The Resource Type of the resource | [optional] 
**updated_at** | **str** | The time the resource was last provisioned as part of a deployment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

