# PodStateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_statuses** | **list[dict(str, object)]** |  | [optional] 
**phase** | **str** |  | [optional] 
**pod_name** | **str** |  | [optional] 
**revision** | **int** |  | [optional] 
**status** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

