# AddArtefactVersionPayloadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | **str** | (Optional) The commit ID the Artefact Version was built on. | [optional] 
**digest** | **str** | (Optional) The Artefact Version digest. | [optional] 
**name** | **str** | The Artefact name. | 
**ref** | **str** | (Optional) The ref the Artefact Version was built from. | [optional] 
**type** | **str** | The Artefact Version type. | 
**version** | **str** | (Optional) The Artefact Version. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

