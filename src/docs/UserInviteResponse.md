# UserInviteResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp this invitation was created. | 
**created_by** | **str** | The ID of the user who created this invitation. | 
**email** | **str** | The email address of the user from the profile. | [optional] 
**expires_at** | **str** | The timestamp this invitation would expire. | 
**user_id** | **str** | The User ID for this user. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

