# swagger_client.DriverDefinitionApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_drivers_driver_id_delete**](DriverDefinitionApi.md#orgs_org_id_resources_drivers_driver_id_delete) | **DELETE** /orgs/{orgId}/resources/drivers/{driverId} | Delete a Resources Driver.
[**orgs_org_id_resources_drivers_driver_id_get**](DriverDefinitionApi.md#orgs_org_id_resources_drivers_driver_id_get) | **GET** /orgs/{orgId}/resources/drivers/{driverId} | Get a Resource Driver.
[**orgs_org_id_resources_drivers_driver_id_put**](DriverDefinitionApi.md#orgs_org_id_resources_drivers_driver_id_put) | **PUT** /orgs/{orgId}/resources/drivers/{driverId} | Update a Resource Driver.
[**orgs_org_id_resources_drivers_get**](DriverDefinitionApi.md#orgs_org_id_resources_drivers_get) | **GET** /orgs/{orgId}/resources/drivers | List Resource Drivers.
[**orgs_org_id_resources_drivers_post**](DriverDefinitionApi.md#orgs_org_id_resources_drivers_post) | **POST** /orgs/{orgId}/resources/drivers | Register a new Resource Driver.

# **orgs_org_id_resources_drivers_driver_id_delete**
> orgs_org_id_resources_drivers_driver_id_delete(org_id, driver_id)

Delete a Resources Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DriverDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resources Driver ID to delete.  

try:
    # Delete a Resources Driver.
    api_instance.orgs_org_id_resources_drivers_driver_id_delete(org_id, driver_id)
except ApiException as e:
    print("Exception when calling DriverDefinitionApi->orgs_org_id_resources_drivers_driver_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resources Driver ID to delete.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_driver_id_get**
> DriverDefinitionResponse orgs_org_id_resources_drivers_driver_id_get(org_id, driver_id)

Get a Resource Driver.

# Only drivers that belongs to the given organization or registered as `public` are accessible through this endpoint

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DriverDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resource Driver ID.  

try:
    # Get a Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_driver_id_get(org_id, driver_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DriverDefinitionApi->orgs_org_id_resources_drivers_driver_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resource Driver ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_driver_id_put**
> DriverDefinitionResponse orgs_org_id_resources_drivers_driver_id_put(body, org_id, driver_id)

Update a Resource Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DriverDefinitionApi()
body = swagger_client.UpdateDriverRequestRequest() # UpdateDriverRequestRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
driver_id = 'driver_id_example' # str | The Resource Driver ID.  

try:
    # Update a Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_driver_id_put(body, org_id, driver_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DriverDefinitionApi->orgs_org_id_resources_drivers_driver_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateDriverRequestRequest**](UpdateDriverRequestRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **driver_id** | **str**| The Resource Driver ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_get**
> list[DriverDefinitionResponse] orgs_org_id_resources_drivers_get(org_id)

List Resource Drivers.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DriverDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Drivers.
    api_response = api_instance.orgs_org_id_resources_drivers_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DriverDefinitionApi->orgs_org_id_resources_drivers_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[DriverDefinitionResponse]**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_drivers_post**
> DriverDefinitionResponse orgs_org_id_resources_drivers_post(body, org_id)

Register a new Resource Driver.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DriverDefinitionApi()
body = swagger_client.CreateDriverRequestRequest() # CreateDriverRequestRequest | Resources Driver details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Register a new Resource Driver.
    api_response = api_instance.orgs_org_id_resources_drivers_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DriverDefinitionApi->orgs_org_id_resources_drivers_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateDriverRequestRequest**](CreateDriverRequestRequest.md)| Resources Driver details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**DriverDefinitionResponse**](DriverDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

