# UpdateDriverRequestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_types** | **list[str]** | List of resources accounts types supported by the driver | 
**inputs_schema** | **dict(str, object)** | A JSON Schema specifying the driver-specific input parameters. | 
**target** | **str** | The prefix where the driver resides or, if the driver is a virtual driver, the reference to an existing driver using the &#x60;driver://&#x60; schema of the format &#x60;driver://{orgId}/{driverId}&#x60;. Only members of the organization the driver belongs to can see &#x27;target&#x27;. | 
**template** | **object** | If the driver is a virtual driver, template defines a Go template that converts the driver inputs supplied in the resource definition into the driver inputs for the target driver. | [optional] 
**type** | **str** | The type of resource produced by this driver | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

