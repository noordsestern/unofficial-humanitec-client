# swagger_client.ResourceDefinitionApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete) | **DELETE** /orgs/{orgId}/resources/defs/{defId}/criteria/{criteriaId} | Delete a Matching Criteria from a Resource Definition.
[**orgs_org_id_resources_defs_def_id_criteria_post**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_criteria_post) | **POST** /orgs/{orgId}/resources/defs/{defId}/criteria | Add a new Matching Criteria to a Resource Definition.
[**orgs_org_id_resources_defs_def_id_delete**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_delete) | **DELETE** /orgs/{orgId}/resources/defs/{defId} | Delete a Resource Definition.
[**orgs_org_id_resources_defs_def_id_get**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_get) | **GET** /orgs/{orgId}/resources/defs/{defId} | Get a specific Resource Definition.
[**orgs_org_id_resources_defs_def_id_patch**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_patch) | **PATCH** /orgs/{orgId}/resources/defs/{defId} | Update a Resource Definition.
[**orgs_org_id_resources_defs_def_id_put**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_put) | **PUT** /orgs/{orgId}/resources/defs/{defId} | Update a Resource Definition.
[**orgs_org_id_resources_defs_def_id_resources_get**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_def_id_resources_get) | **GET** /orgs/{orgId}/resources/defs/{defId}/resources | List Active Resources provisioned via a specific Resource Definition.
[**orgs_org_id_resources_defs_get**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_get) | **GET** /orgs/{orgId}/resources/defs | List Resource Definitions.
[**orgs_org_id_resources_defs_post**](ResourceDefinitionApi.md#orgs_org_id_resources_defs_post) | **POST** /orgs/{orgId}/resources/defs | Create a new Resource Definition.

# **orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete**
> orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)

Delete a Matching Criteria from a Resource Definition.

If there **are no** Active Resources that would match to a different Resource Definition when the current Matching Criteria is deleted, the Matching Criteria is deleted immediately.  If there **are** Active Resources that would match to a different Resource Definition, the request fails with HTTP status code 409 (Conflict). The response content will list all of affected Active Resources and their new matches.  The request can take an optional `force` query parameter. If set to `true`, the Matching Criteria is deleted immediately. Referenced Active Resources would match to a different Resource Definition during the next deployment in the target environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  
criteria_id = 'criteria_id_example' # str | The Matching Criteria ID.  
force = true # bool | If set to `true`, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   (optional)

try:
    # Delete a Matching Criteria from a Resource Definition.
    api_instance.orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete(org_id, def_id, criteria_id, force=force)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_criteria_criteria_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 
 **criteria_id** | **str**| The Matching Criteria ID.   | 
 **force** | **bool**| If set to &#x60;true&#x60;, the Matching Criteria is deleted immediately, even if this action affects existing Active Resources.   | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_criteria_post**
> MatchingCriteriaResponse orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)

Add a new Matching Criteria to a Resource Definition.

Matching Criteria are combined with Resource Type to select a specific definition. Matching Criteria can be set for any combination of Application ID, Environment ID, Environment Type, and Resource ID. In the event of multiple matches, the most specific match is chosen.  For example, given 3 sets of matching criteria for the same type:  ```  1. {\"env_type\":\"test\"}  2. {\"env_type\":\"development\"}  3. {\"env_type\":\"test\", \"app_id\":\"my-app\"} ```  If, a resource of that time was needed in an Application `my-app`, Environment `qa-team` with Type `test` and Resource ID `modules.my-module-externals.my-resource`, there would be two resource definitions matching the criteria: #1 & #3. Definition #3 will be chosen because its matching criteria is the most specific.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
body = swagger_client.MatchingCriteriaRuleRequest() # MatchingCriteriaRuleRequest | Matching Criteria rules.


org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Add a new Matching Criteria to a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_criteria_post(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_criteria_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MatchingCriteriaRuleRequest**](MatchingCriteriaRuleRequest.md)| Matching Criteria rules.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**MatchingCriteriaResponse**](MatchingCriteriaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_delete**
> orgs_org_id_resources_defs_def_id_delete(org_id, def_id, force=force)

Delete a Resource Definition.

If there **are no** Active Resources provisioned via the current definition, the Resource Definition is deleted immediately.  If there **are** Active Resources provisioned via the current definition, the request fails. The response will describe the changes to the affected Active Resources if operation is forced.  The request can take an optional `force` query parameter. If set to `true`, the current Resource Definition is **marked as** pending deletion and will be deleted (purged) as soon as no existing Active Resources reference it. With the next deployment matching criteria for Resources will be re-evaluated, and current Active Resources for the target environment would be either linked to another matching Resource Definition or decommissioned and created using the new or default Resource Definition (when available).  The Resource Definition that has been marked for deletion cannot be used to provision new resources.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  
force = true # bool | If set to `true`, will mark the Resource Definition for deletion, even if it affects existing Active Resources.   (optional)

try:
    # Delete a Resource Definition.
    api_instance.orgs_org_id_resources_defs_def_id_delete(org_id, def_id, force=force)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 
 **force** | **bool**| If set to &#x60;true&#x60;, will mark the Resource Definition for deletion, even if it affects existing Active Resources.   | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_get**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_get(org_id, def_id)

Get a specific Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Get a specific Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_get(org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_patch**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_patch(body, org_id, def_id)

Update a Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
body = swagger_client.PatchResourceDefinitionRequestRequest() # PatchResourceDefinitionRequestRequest | The Resource Definition record details.

The PATCH operation would change the value of the property if it is included in the request payload JSON, and not `null`. Missing and `null` properties are ignored.

For the map properties, such as PatchResourceDefinitionRequest.DriverInputs, the merge operation is applied.

Merge rules are as follows:

- If a map property has a value, it is replaced (or added).

- If a map property is set to `null`, it is removed.

- If a map property is not included (missing in JSON), it remains unchanged.
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Update a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_patch(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PatchResourceDefinitionRequestRequest**](PatchResourceDefinitionRequestRequest.md)| The Resource Definition record details.

The PATCH operation would change the value of the property if it is included in the request payload JSON, and not &#x60;null&#x60;. Missing and &#x60;null&#x60; properties are ignored.

For the map properties, such as PatchResourceDefinitionRequest.DriverInputs, the merge operation is applied.

Merge rules are as follows:

- If a map property has a value, it is replaced (or added).

- If a map property is set to &#x60;null&#x60;, it is removed.

- If a map property is not included (missing in JSON), it remains unchanged. | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_put**
> ResourceDefinitionResponse orgs_org_id_resources_defs_def_id_put(body, org_id, def_id)

Update a Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
body = swagger_client.UpdateResourceDefinitionRequestRequest() # UpdateResourceDefinitionRequestRequest | The Resource Definition record details.

The PUT operation updates a resource definition using the provided payload. An empty driver_account or driver_inputs property will unset the existing values.

Currently the resource and driver types can't be changed.
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # Update a Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_put(body, org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateResourceDefinitionRequestRequest**](UpdateResourceDefinitionRequestRequest.md)| The Resource Definition record details.

The PUT operation updates a resource definition using the provided payload. An empty driver_account or driver_inputs property will unset the existing values.

Currently the resource and driver types can&#x27;t be changed. | 
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_def_id_resources_get**
> list[ActiveResourceResponse] orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)

List Active Resources provisioned via a specific Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
def_id = 'def_id_example' # str | The Resource Definition ID.  

try:
    # List Active Resources provisioned via a specific Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_def_id_resources_get(org_id, def_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_def_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **def_id** | **str**| The Resource Definition ID.   | 

### Return type

[**list[ActiveResourceResponse]**](ActiveResourceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_get**
> list[ResourceDefinitionResponse] orgs_org_id_resources_defs_get(org_id, app=app, env=env, env_type=env_type, res=res, res_type=res_type, _class=_class)

List Resource Definitions.

Filter criteria can be applied to obtain all the resource definitions that could match the filters, grouped by type and sorted by matching rank.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
org_id = 'org_id_example' # str | The Organization ID.  
app = 'app_example' # str | (Optional) Filter Resource Definitions that may match a specific Application.   (optional)
env = 'env_example' # str | (Optional) Filter Resource Definitions that may match a specific Environment.   (optional)
env_type = 'env_type_example' # str | (Optional) Filter Resource Definitions that may match a specific Environment Type.   (optional)
res = 'res_example' # str | (Optional) Filter Resource Definitions that may match a specific Resource.   (optional)
res_type = 'res_type_example' # str | (Optional) Filter Resource Definitions that may match a specific Resource Type.   (optional)
_class = '_class_example' # str | (Optional) Filter Resource Definitions that may match a specific Class.   (optional)

try:
    # List Resource Definitions.
    api_response = api_instance.orgs_org_id_resources_defs_get(org_id, app=app, env=env, env_type=env_type, res=res, res_type=res_type, _class=_class)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app** | **str**| (Optional) Filter Resource Definitions that may match a specific Application.   | [optional] 
 **env** | **str**| (Optional) Filter Resource Definitions that may match a specific Environment.   | [optional] 
 **env_type** | **str**| (Optional) Filter Resource Definitions that may match a specific Environment Type.   | [optional] 
 **res** | **str**| (Optional) Filter Resource Definitions that may match a specific Resource.   | [optional] 
 **res_type** | **str**| (Optional) Filter Resource Definitions that may match a specific Resource Type.   | [optional] 
 **_class** | **str**| (Optional) Filter Resource Definitions that may match a specific Class.   | [optional] 

### Return type

[**list[ResourceDefinitionResponse]**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_resources_defs_post**
> ResourceDefinitionResponse orgs_org_id_resources_defs_post(body, org_id)

Create a new Resource Definition.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ResourceDefinitionApi()
body = swagger_client.CreateResourceDefinitionRequestRequest() # CreateResourceDefinitionRequestRequest | The Resource Definition details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create a new Resource Definition.
    api_response = api_instance.orgs_org_id_resources_defs_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceDefinitionApi->orgs_org_id_resources_defs_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateResourceDefinitionRequestRequest**](CreateResourceDefinitionRequestRequest.md)| The Resource Definition details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ResourceDefinitionResponse**](ResourceDefinitionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

