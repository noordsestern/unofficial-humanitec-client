# swagger_client.DeltaApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_delta**](DeltaApi.md#get_delta) | **GET** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Fetch an existing Delta
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/archived | Mark a Delta as \&quot;archived\&quot;
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/env_id | Change the Environment of a Delta
[**orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId}/metadata/name | Change the name of a Delta
[**orgs_org_id_apps_app_id_deltas_delta_id_patch**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_delta_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Update an existing Delta
[**orgs_org_id_apps_app_id_deltas_get**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_get) | **GET** /orgs/{orgId}/apps/{appId}/deltas | List Deltas in an Application
[**orgs_org_id_apps_app_id_deltas_post**](DeltaApi.md#orgs_org_id_apps_app_id_deltas_post) | **POST** /orgs/{orgId}/apps/{appId}/deltas | Create a new Delta
[**put_delta**](DeltaApi.md#put_delta) | **PUT** /orgs/{orgId}/apps/{appId}/deltas/{deltaId} | Update an existing Delta

# **get_delta**
> DeltaResponse get_delta(org_id, app_id, delta_id)

Fetch an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to fetch.  

try:
    # Fetch an existing Delta
    api_response = api_instance.get_delta(org_id, app_id, delta_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeltaApi->get_delta: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to fetch.   | 

### Return type

[**DeltaResponse**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put(body, org_id, app_id, delta_id)

Mark a Delta as \"archived\"

Archived Deltas are still accessible but can no longer be updated.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = true # bool | Either `true` or `false`.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Mark a Delta as \"archived\"
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_archived_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**bool**](bool.md)| Either &#x60;true&#x60; or &#x60;false&#x60;.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put(body, org_id, app_id, delta_id)

Change the Environment of a Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = '\"new-env\"' # str | The new Environment ID. (NOTE: The string must still be JSON encoded.)


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Change the Environment of a Delta
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_env_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The new Environment ID. (NOTE: The string must still be JSON encoded.)

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put**
> orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put(body, org_id, app_id, delta_id)

Change the name of a Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = '\"Update for ticket #s 2568 & 2572\"' # str | The new name.(NOTE: The string must still be JSON encoded.)


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Deployment Delta.  

try:
    # Change the name of a Delta
    api_instance.orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_delta_id_metadata_name_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**str**](str.md)| The new name.(NOTE: The string must still be JSON encoded.)

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Deployment Delta.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_delta_id_patch**
> DeltaResponse orgs_org_id_apps_app_id_deltas_delta_id_patch(body, org_id, app_id, delta_id)

Update an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = [swagger_client.DeltaRequest()] # list[DeltaRequest] | An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the `modules.add` property are replaced with the new Delta's values. If the value of a Module is `null`, and the ID is in the `modules.remove` list, it is removed from the `modules.remove` list.

* All IDs listed in `modules.remove` are combined. Any ID in `modules.remove` and also in `modules.add` are removed from `modules.add`

* The lists of JSON Patches in `modules.update` are concatenated or created in `modules.updates`.

Simplification involves:

* Applying any entries in `modules.updates` that have matching IDs in `modules.add` to the `modules.add` entry and removing the `modules.update` entry.

* Reducing the number of JSON Patches in each `modules.update` entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the `value` of the `remove` action can be set to `{"scope": "delta"}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the `id` or `metadata` properties are specified, they will be ignored._
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to update.  

try:
    # Update an existing Delta
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_delta_id_patch(body, org_id, app_id, delta_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_delta_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[DeltaRequest]**](DeltaRequest.md)| An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the &#x60;modules.add&#x60; property are replaced with the new Delta&#x27;s values. If the value of a Module is &#x60;null&#x60;, and the ID is in the &#x60;modules.remove&#x60; list, it is removed from the &#x60;modules.remove&#x60; list.

* All IDs listed in &#x60;modules.remove&#x60; are combined. Any ID in &#x60;modules.remove&#x60; and also in &#x60;modules.add&#x60; are removed from &#x60;modules.add&#x60;

* The lists of JSON Patches in &#x60;modules.update&#x60; are concatenated or created in &#x60;modules.updates&#x60;.

Simplification involves:

* Applying any entries in &#x60;modules.updates&#x60; that have matching IDs in &#x60;modules.add&#x60; to the &#x60;modules.add&#x60; entry and removing the &#x60;modules.update&#x60; entry.

* Reducing the number of JSON Patches in each &#x60;modules.update&#x60; entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the &#x60;value&#x60; of the &#x60;remove&#x60; action can be set to &#x60;{&quot;scope&quot;: &quot;delta&quot;}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the &#x60;id&#x60; or &#x60;metadata&#x60; properties are specified, they will be ignored._ | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to update.   | 

### Return type

[**DeltaResponse**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_get**
> list[DeltaResponse] orgs_org_id_apps_app_id_deltas_get(org_id, app_id, archived=archived, env=env)

List Deltas in an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
archived = true # bool | If true, return archived Deltas.   (optional)
env = 'env_example' # str | Only return Deltas associated with the specified Environment.   (optional)

try:
    # List Deltas in an Application
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_get(org_id, app_id, archived=archived, env=env)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **archived** | **bool**| If true, return archived Deltas.   | [optional] 
 **env** | **str**| Only return Deltas associated with the specified Environment.   | [optional] 

### Return type

[**list[DeltaResponse]**](DeltaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_deltas_post**
> InlineResponse200 orgs_org_id_apps_app_id_deltas_post(body, org_id, app_id)

Create a new Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = swagger_client.DeltaRequest() # DeltaRequest | A Deployment Delta to create.

The Deployment Delta will be added with the provided content of `modules` and the 'env_id' and 'name' properties of the 'metadata' property.

NOTE: If the `id` property is specified, it will be ignored. A new ID will be generated and returned in the response.
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a new Delta
    api_response = api_instance.orgs_org_id_apps_app_id_deltas_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeltaApi->orgs_org_id_apps_app_id_deltas_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| A Deployment Delta to create.

The Deployment Delta will be added with the provided content of &#x60;modules&#x60; and the &#x27;env_id&#x27; and &#x27;name&#x27; properties of the &#x27;metadata&#x27; property.

NOTE: If the &#x60;id&#x60; property is specified, it will be ignored. A new ID will be generated and returned in the response. | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_delta**
> put_delta(body, org_id, app_id, delta_id)

Update an existing Delta

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeltaApi()
body = swagger_client.DeltaRequest() # DeltaRequest | An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the `modules.add` property are replaced with the new Delta's values. If the value of a Module is `null`, and the ID is in the `modules.remove` list, it is removed from the `modules.remove` list.

* All IDs listed in `modules.remove` are combined. Any ID in `modules.remove` and also in `modules.add` are removed from `modules.add`

* The lists of JSON Patches in `modules.update` are concatenated or created in `modules.updates`.

Simplification involves:

* Applying any entries in `modules.updates` that have matching IDs in `modules.add` to the `modules.add` entry and removing the `modules.update` entry.

* Reducing the number of JSON Patches in each `modules.update` entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the `value` of the `remove` action can be set to `{"scope": "delta"}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the `id` or `metadata` properties are specified, they will be ignored._
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
delta_id = 'delta_id_example' # str | ID of the Delta to update.  

try:
    # Update an existing Delta
    api_instance.put_delta(body, org_id, app_id, delta_id)
except ApiException as e:
    print("Exception when calling DeltaApi->put_delta: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeltaRequest**](DeltaRequest.md)| An array of Deltas.

The Deltas in the request are combined, meaning the current Delta is updated in turn by each Delta in the request. Once all Deltas have been combined, the resulting Delta is simplified.

* All Modules in the &#x60;modules.add&#x60; property are replaced with the new Delta&#x27;s values. If the value of a Module is &#x60;null&#x60;, and the ID is in the &#x60;modules.remove&#x60; list, it is removed from the &#x60;modules.remove&#x60; list.

* All IDs listed in &#x60;modules.remove&#x60; are combined. Any ID in &#x60;modules.remove&#x60; and also in &#x60;modules.add&#x60; are removed from &#x60;modules.add&#x60;

* The lists of JSON Patches in &#x60;modules.update&#x60; are concatenated or created in &#x60;modules.updates&#x60;.

Simplification involves:

* Applying any entries in &#x60;modules.updates&#x60; that have matching IDs in &#x60;modules.add&#x60; to the &#x60;modules.add&#x60; entry and removing the &#x60;modules.update&#x60; entry.

* Reducing the number of JSON Patches in each &#x60;modules.update&#x60; entry to the smallest set that has the same effect.

**Extension to JSON Patch**

If a JSON Patch entry needs to be removed, without side effects, the &#x60;value&#x60; of the &#x60;remove&#x60; action can be set to &#x60;{&quot;scope&quot;: &quot;delta&quot;}. This will result in the remove action being used during simplification but be discarded before the Delta is finalized.

If the user making the request is not the user who created the Delta and they are not already on the contributors list, they will be added to the contributors list.

_NOTE: If the &#x60;id&#x60; or &#x60;metadata&#x60; properties are specified, they will be ignored._ | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **delta_id** | **str**| ID of the Delta to update.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

