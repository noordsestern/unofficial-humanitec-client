# ValueRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | A Human friendly description of what the Shared Value is. | [optional] 
**is_secret** | **bool** | Specified that the Shared Value contains a secret. | [optional] 
**key** | **str** | The unique key by which the Shared Value can be referenced. pattern: ^[a-zA-Z0-9._-]+$. | [optional] 
**value** | **str** | The value that will be stored. (Will be always empty for secrets.) | [optional] 
**secret_ref** | [**SecretReference**](SecretReference.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

