# RuntimeInfoRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modules** | [**dict(str, ModuleRequest)**](ModuleRequest.md) |  | [optional] 
**namespace** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

