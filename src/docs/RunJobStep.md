# RunJobStep

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | The index of the Step within the Pipeline Schema. | [optional] 
**status** | **str** | The current status of this Step within the Job. | [optional] 
**status_message** | **str** | A human-readable message indicating the reason for the status. | [optional] 
**created_at** | **datetime** | The date and time when this Step was first created within the Job. | [optional] 
**completed_at** | **datetime** | The date and time when this Step entered a successful, failed, or cancelled status. | [optional] 
**timeout_seconds** | **int** | The timeout for this Job. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

