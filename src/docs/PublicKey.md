# PublicKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**key** | **str** |  | 
**created_at** | **datetime** |  | 
**created_by** | **str** |  | 
**expired_at** | **datetime** |  | 
**fingerprint** | **str** | Key is the sha256 public key fingerprint, it&#x27;s computed and stored when a new key is uploaded. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

