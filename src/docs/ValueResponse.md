# ValueResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** |  | 
**description** | **str** | A Human friendly description of what the Shared Value is. | 
**is_secret** | **bool** | Specified that the Shared Value contains a secret. | 
**key** | **str** | The unique key by which the Shared Value can be referenced. pattern: ^[a-zA-Z0-9._-]+$. | 
**secret_key** | **str** | Location of the secret value in the secret store. | 
**secret_store_id** | **str** |  | 
**secret_version** | **str** | Version of the current secret value as returned by the secret store. | 
**source** | [**ValueSource**](ValueSource.md) |  | 
**updated_at** | **datetime** |  | 
**value** | **str** | The value that will be stored. (Will be always empty for secrets.) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

