# AzureKVRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth** | [**AzureAuthRequest**](AzureAuthRequest.md) |  | [optional] 
**tenant_id** | **str** |  | [optional] 
**url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

