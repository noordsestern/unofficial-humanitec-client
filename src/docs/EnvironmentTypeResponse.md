# EnvironmentTypeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | A Human-readable description of the Environment Type | 
**id** | **str** | The ID of the Environment Type. (Must be unique within an Organization.) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

