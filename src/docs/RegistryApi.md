# swagger_client.RegistryApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_registries_get**](RegistryApi.md#orgs_org_id_registries_get) | **GET** /orgs/{orgId}/registries | Lists available registries for the organization.
[**orgs_org_id_registries_post**](RegistryApi.md#orgs_org_id_registries_post) | **POST** /orgs/{orgId}/registries | Creates a new registry record.
[**orgs_org_id_registries_reg_id_creds_get**](RegistryApi.md#orgs_org_id_registries_reg_id_creds_get) | **GET** /orgs/{orgId}/registries/{regId}/creds | Returns current account credentials or secret details for the registry.
[**orgs_org_id_registries_reg_id_delete**](RegistryApi.md#orgs_org_id_registries_reg_id_delete) | **DELETE** /orgs/{orgId}/registries/{regId} | Deletes an existing registry record and all associated credentials and secrets.
[**orgs_org_id_registries_reg_id_get**](RegistryApi.md#orgs_org_id_registries_reg_id_get) | **GET** /orgs/{orgId}/registries/{regId} | Loads a registry record details.
[**orgs_org_id_registries_reg_id_patch**](RegistryApi.md#orgs_org_id_registries_reg_id_patch) | **PATCH** /orgs/{orgId}/registries/{regId} | Updates (patches) an existing registry record.

# **orgs_org_id_registries_get**
> list[RegistryResponse] orgs_org_id_registries_get(org_id)

Lists available registries for the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  

try:
    # Lists available registries for the organization.
    api_response = api_instance.orgs_org_id_registries_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 

### Return type

[**list[RegistryResponse]**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_post**
> RegistryResponse orgs_org_id_registries_post(body, org_id)

Creates a new registry record.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
body = swagger_client.RegistryRequest() # RegistryRequest | A new record details.


org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  

try:
    # Creates a new registry record.
    api_response = api_instance.orgs_org_id_registries_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RegistryRequest**](RegistryRequest.md)| A new record details.

 | 
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_creds_get**
> RegistryCredsResponse orgs_org_id_registries_reg_id_creds_get(org_id, reg_id)

Returns current account credentials or secret details for the registry.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Returns current account credentials or secret details for the registry.
    api_response = api_instance.orgs_org_id_registries_reg_id_creds_get(org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_reg_id_creds_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryCredsResponse**](RegistryCredsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_delete**
> orgs_org_id_registries_reg_id_delete(org_id, reg_id)

Deletes an existing registry record and all associated credentials and secrets.

_Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Deletes an existing registry record and all associated credentials and secrets.
    api_instance.orgs_org_id_registries_reg_id_delete(org_id, reg_id)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_reg_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_get**
> RegistryResponse orgs_org_id_registries_reg_id_get(org_id, reg_id)

Loads a registry record details.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Loads a registry record details.
    api_response = api_instance.orgs_org_id_registries_reg_id_get(org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_reg_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_registries_reg_id_patch**
> RegistryResponse orgs_org_id_registries_reg_id_patch(body, org_id, reg_id)

Updates (patches) an existing registry record.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RegistryApi()
body = swagger_client.RegistryRequest() # RegistryRequest | Record details to update.


org_id = 'org_id_example' # str | Unique (alpha-numerical) organization identifier.  
reg_id = 'reg_id_example' # str | Unique (alpha-numerical) registry identifier.  

try:
    # Updates (patches) an existing registry record.
    api_response = api_instance.orgs_org_id_registries_reg_id_patch(body, org_id, reg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RegistryApi->orgs_org_id_registries_reg_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RegistryRequest**](RegistryRequest.md)| Record details to update.

 | 
 **org_id** | **str**| Unique (alpha-numerical) organization identifier.   | 
 **reg_id** | **str**| Unique (alpha-numerical) registry identifier.   | 

### Return type

[**RegistryResponse**](RegistryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

