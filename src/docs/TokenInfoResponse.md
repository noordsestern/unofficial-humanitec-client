# TokenInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** |  | 
**created_by** | **str** |  | 
**description** | **str** |  | 
**expires_at** | **str** |  | [optional] 
**id** | **str** |  | 
**type** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

