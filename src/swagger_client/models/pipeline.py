# coding: utf-8

"""
    Humanitec API

    # Introduction The *Humanitec API* allows you to automate and integrate Humanitec into your developer and operational workflows. The API is a REST based API. It is based around a set of concepts:  * Core * External Resources * Sets and Deltas  ## Authentication Almost all requests made to the Humanitec API require Authentication. Humanitec provides 2 ways of authenticating with the API: `Bearer` and `JWT`.  ### Bearer Authentication This form of authentication makes use of a **static token**. It is intended to be used when machines interact with the Humanitec API. Bearer tokens should be used for very narrow purposes. This allows for the token to be revoked if it is compromised and so limit the scope of exposure. New Bearer tokens can be obtained via the UI:  1. Log into Humanitec at https://app.humanitec.io 1. Go to **Organization Settings** 1. Select **API tokens** 1. Enter a *name* for the new token and click on **Generate new token**  The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_TOKEN`, the request could be made as follows:  ```     curl -H 'Authorization: Bearer HUMANITEC_TOKEN' https://api.humanitec.io/orgs/my-org/apps ```  ### JWT Authentication This form of authentication makes use of a **JSON Web Token (JWT)**. It is intended to be used when humans interact with the Humanitec API. JWTs expire after a period of time. This means that a new JWT will need to be generated regularly. This makes them well suited to working in short sessions, but not for automation. (See Bearer Authentication.) The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_JWT`, the request could be made as follows:  ```     curl -H 'Authorization: JWT HUMANITEC_JWT' https://api.humanitec.io/orgs/my-org/apps ```  ## Content Types All of the Humanitec API unless explicitly only accepts content types of `application/json` and will always return valid `application/json` or an empty response.  ## Response Codes ### Success Any response code in the `2xx` range should be regarded as success.  | **Code** | **Meaning** | | --- | --- | | `200` | Success | | `201` | Success (In future, `201` will be replaced by `200`) | | `204` | Success, but no content in response |  _Note: We plan to simplify the interface by replacing 201 with 200 status codes._  ### Failure Any response code in the `4xx` should be regarded as an error which can be rectified by the client. `5xx` error codes indicate errors that cannot be corrected by the client.  | **Code** | **Meaning** | | --- | --- | | `400` | General error. (Body will contain details) | | `401` | Attempt to access protected resource without `Authorization` Header. | | `403` | The `Bearer` or `JWT` does not grant access to the requested resource. | | `404` | Resource not found. | | `405` | Method not allowed | | `409` | Conflict. Usually indicated a resource with that ID already exists. | | `422` | Unprocessable Entity. The body was not valid JSON, was empty or contained an object different from what was expected. | | `429` | Too many requests - request rate limit has been reached. | | `500` | Internal Error. If it occurs repeatedly, contact support. |   # noqa: E501

    OpenAPI spec version: 0.24.1
    Contact: apiteam@humanitec.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Pipeline(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'etag': 'str',
        'org_id': 'str',
        'app_id': 'str',
        'name': 'str',
        'status': 'str',
        'version': 'str',
        'created_at': 'datetime',
        'trigger_types': 'list[str]',
        'metadata': 'dict(str, str)'
    }

    attribute_map = {
        'id': 'id',
        'etag': 'etag',
        'org_id': 'org_id',
        'app_id': 'app_id',
        'name': 'name',
        'status': 'status',
        'version': 'version',
        'created_at': 'created_at',
        'trigger_types': 'trigger_types',
        'metadata': 'metadata'
    }

    def __init__(self, id=None, etag=None, org_id=None, app_id=None, name=None, status=None, version=None, created_at=None, trigger_types=None, metadata=None):  # noqa: E501
        """Pipeline - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._etag = None
        self._org_id = None
        self._app_id = None
        self._name = None
        self._status = None
        self._version = None
        self._created_at = None
        self._trigger_types = None
        self._metadata = None
        self.discriminator = None
        self.id = id
        self.etag = etag
        self.org_id = org_id
        self.app_id = app_id
        self.name = name
        self.status = status
        self.version = version
        self.created_at = created_at
        self.trigger_types = trigger_types
        if metadata is not None:
            self.metadata = metadata

    @property
    def id(self):
        """Gets the id of this Pipeline.  # noqa: E501

        The id of the Pipeline.  # noqa: E501

        :return: The id of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Pipeline.

        The id of the Pipeline.  # noqa: E501

        :param id: The id of this Pipeline.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def etag(self):
        """Gets the etag of this Pipeline.  # noqa: E501

        The current entity tag value for this Pipeline.  # noqa: E501

        :return: The etag of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._etag

    @etag.setter
    def etag(self, etag):
        """Sets the etag of this Pipeline.

        The current entity tag value for this Pipeline.  # noqa: E501

        :param etag: The etag of this Pipeline.  # noqa: E501
        :type: str
        """
        if etag is None:
            raise ValueError("Invalid value for `etag`, must not be `None`")  # noqa: E501

        self._etag = etag

    @property
    def org_id(self):
        """Gets the org_id of this Pipeline.  # noqa: E501

        The id of the Organization containing this Pipeline.  # noqa: E501

        :return: The org_id of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this Pipeline.

        The id of the Organization containing this Pipeline.  # noqa: E501

        :param org_id: The org_id of this Pipeline.  # noqa: E501
        :type: str
        """
        if org_id is None:
            raise ValueError("Invalid value for `org_id`, must not be `None`")  # noqa: E501

        self._org_id = org_id

    @property
    def app_id(self):
        """Gets the app_id of this Pipeline.  # noqa: E501

        The id of the Application containing this Pipeline.  # noqa: E501

        :return: The app_id of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._app_id

    @app_id.setter
    def app_id(self, app_id):
        """Sets the app_id of this Pipeline.

        The id of the Application containing this Pipeline.  # noqa: E501

        :param app_id: The app_id of this Pipeline.  # noqa: E501
        :type: str
        """
        if app_id is None:
            raise ValueError("Invalid value for `app_id`, must not be `None`")  # noqa: E501

        self._app_id = app_id

    @property
    def name(self):
        """Gets the name of this Pipeline.  # noqa: E501

        The name of the Pipeline.  # noqa: E501

        :return: The name of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Pipeline.

        The name of the Pipeline.  # noqa: E501

        :param name: The name of this Pipeline.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def status(self):
        """Gets the status of this Pipeline.  # noqa: E501

        The current status of the Pipeline.  # noqa: E501

        :return: The status of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this Pipeline.

        The current status of the Pipeline.  # noqa: E501

        :param status: The status of this Pipeline.  # noqa: E501
        :type: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        self._status = status

    @property
    def version(self):
        """Gets the version of this Pipeline.  # noqa: E501

        The unique id of the current Pipeline Version.  # noqa: E501

        :return: The version of this Pipeline.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this Pipeline.

        The unique id of the current Pipeline Version.  # noqa: E501

        :param version: The version of this Pipeline.  # noqa: E501
        :type: str
        """
        if version is None:
            raise ValueError("Invalid value for `version`, must not be `None`")  # noqa: E501

        self._version = version

    @property
    def created_at(self):
        """Gets the created_at of this Pipeline.  # noqa: E501

        The date and time when the Pipeline was created.  # noqa: E501

        :return: The created_at of this Pipeline.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Pipeline.

        The date and time when the Pipeline was created.  # noqa: E501

        :param created_at: The created_at of this Pipeline.  # noqa: E501
        :type: datetime
        """
        if created_at is None:
            raise ValueError("Invalid value for `created_at`, must not be `None`")  # noqa: E501

        self._created_at = created_at

    @property
    def trigger_types(self):
        """Gets the trigger_types of this Pipeline.  # noqa: E501

        The list of trigger types in the current schema.  # noqa: E501

        :return: The trigger_types of this Pipeline.  # noqa: E501
        :rtype: list[str]
        """
        return self._trigger_types

    @trigger_types.setter
    def trigger_types(self, trigger_types):
        """Sets the trigger_types of this Pipeline.

        The list of trigger types in the current schema.  # noqa: E501

        :param trigger_types: The trigger_types of this Pipeline.  # noqa: E501
        :type: list[str]
        """
        if trigger_types is None:
            raise ValueError("Invalid value for `trigger_types`, must not be `None`")  # noqa: E501

        self._trigger_types = trigger_types

    @property
    def metadata(self):
        """Gets the metadata of this Pipeline.  # noqa: E501

        The map of key value pipeline additional information  # noqa: E501

        :return: The metadata of this Pipeline.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this Pipeline.

        The map of key value pipeline additional information  # noqa: E501

        :param metadata: The metadata of this Pipeline.  # noqa: E501
        :type: dict(str, str)
        """

        self._metadata = metadata

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Pipeline, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Pipeline):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
