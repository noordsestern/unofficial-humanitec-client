# DeploymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | An optional comment to help communicate the purpose of the Deployment. | [optional] 
**delta_id** | **str** | ID of the Deployment Delta describing the changes to the current Environment for this Deployment. | [optional] 
**set_id** | **str** | ID of the Deployment Set describing the state of the Environment after Deployment. | [optional] 
**value_set_version_id** | **str** | ID of the Value Set Version describe the values to be used for this Deployment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

