# UpdateSecretStorePayloadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**awssm** | [**AWSSMRequest**](AWSSMRequest.md) |  | [optional] 
**azurekv** | [**AzureKVRequest**](AzureKVRequest.md) |  | [optional] 
**gcpsm** | [**GCPSMRequest**](GCPSMRequest.md) |  | [optional] 
**primary** | **bool** | Defines whether the Secret Store is the primary secret management system for the organization. | [optional] 
**vault** | [**VaultRequest**](VaultRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

