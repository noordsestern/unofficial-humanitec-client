# WorkloadProfileVersionSpecDefinitionProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**WorkloadProfileVersionSpecDefinitionPropertyType**](WorkloadProfileVersionSpecDefinitionPropertyType.md) |  | 
**feature_name** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**ui_hints** | [**WorkloadProfileVersionSpecDefinitionPropertyUIHints**](WorkloadProfileVersionSpecDefinitionPropertyUIHints.md) |  | [optional] 
**schema** | **dict(str, object)** |  | [optional] 
**runtime_properties** | [**list[WorkloadProfileVersionSpecDefinitionRuntimeProperty]**](WorkloadProfileVersionSpecDefinitionRuntimeProperty.md) |  | [optional] 
**properties** | [**WorkloadProfileVersionSpecDefinitionProperties**](WorkloadProfileVersionSpecDefinitionProperties.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

