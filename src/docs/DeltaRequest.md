# DeltaRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Ignored, but can be provided. | [optional] 
**metadata** | [**DeltaMetadataRequest**](DeltaMetadataRequest.md) |  | [optional] 
**modules** | [**ModuleDeltasRequest**](ModuleDeltasRequest.md) |  | [optional] 
**shared** | [**list[UpdateActionRequest]**](UpdateActionRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

