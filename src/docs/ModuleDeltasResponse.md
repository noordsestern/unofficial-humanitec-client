# ModuleDeltasResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add** | [**dict(str, ModuleResponse)**](ModuleResponse.md) |  | 
**remove** | **list[str]** |  | 
**update** | **dict(str, list[UpdateActionResponse])** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

