# SetResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The ID which is a hash of the content of the Deployment Set. | 
**modules** | [**dict(str, ModuleResponse)**](ModuleResponse.md) | The Modules that make up the Set | 
**shared** | **dict(str, object)** | Resources that are shared across the set | 
**version** | **int** | The version of the Deployment Set Schema to use. (Currently, only 0 is supported, and if omitted, version 0 is assumed.) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

