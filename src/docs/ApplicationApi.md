# swagger_client.ApplicationApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_delete**](ApplicationApi.md#orgs_org_id_apps_app_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId} | Delete an Application
[**orgs_org_id_apps_app_id_get**](ApplicationApi.md#orgs_org_id_apps_app_id_get) | **GET** /orgs/{orgId}/apps/{appId} | Get an existing Application
[**orgs_org_id_apps_get**](ApplicationApi.md#orgs_org_id_apps_get) | **GET** /orgs/{orgId}/apps | List all Applications in an Organization.
[**orgs_org_id_apps_post**](ApplicationApi.md#orgs_org_id_apps_post) | **POST** /orgs/{orgId}/apps | Add a new Application to an Organization

# **orgs_org_id_apps_app_id_delete**
> orgs_org_id_apps_app_id_delete(org_id, app_id)

Delete an Application

Deleting an Application will also delete everything associated with it. This includes Environments, Deployment history on those Environments, and any shared values and secrets associated.  _Deletions are currently irreversible._

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ApplicationApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Delete an Application
    api_instance.orgs_org_id_apps_app_id_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling ApplicationApi->orgs_org_id_apps_app_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_get**
> ApplicationResponse orgs_org_id_apps_app_id_get(org_id, app_id)

Get an existing Application

Gets a specific Application in the specified Organization by ID.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ApplicationApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Get an existing Application
    api_response = api_instance.orgs_org_id_apps_app_id_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApplicationApi->orgs_org_id_apps_app_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**ApplicationResponse**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_get**
> list[ApplicationResponse] orgs_org_id_apps_get(org_id)

List all Applications in an Organization.

Listing or lists of all Applications that exist within a specific Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ApplicationApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List all Applications in an Organization.
    api_response = api_instance.orgs_org_id_apps_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApplicationApi->orgs_org_id_apps_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[ApplicationResponse]**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_post**
> ApplicationResponse orgs_org_id_apps_post(body, org_id)

Add a new Application to an Organization

Creates a new Application, then adds it to the specified Organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ApplicationApi()
body = swagger_client.ApplicationCreationRequest() # ApplicationCreationRequest | The request ID, Human-friendly name and environment of the Application.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Add a new Application to an Organization
    api_response = api_instance.orgs_org_id_apps_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApplicationApi->orgs_org_id_apps_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApplicationCreationRequest**](ApplicationCreationRequest.md)| The request ID, Human-friendly name and environment of the Application.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**ApplicationResponse**](ApplicationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

