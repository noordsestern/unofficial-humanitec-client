# OrganizationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Timestamp when the Organization was created. | 
**created_by** | **str** | User ID that created the Organization. | 
**id** | **str** | Unique ID for the Organization. | 
**logo** | [**LogoResponse**](LogoResponse.md) |  | [optional] 
**name** | **str** | Human friendly name for the Organization. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

