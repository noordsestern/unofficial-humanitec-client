# ValuesSecretsRefsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secret_refs** | **dict(str, object)** | Secrets section of the data set. They can hold sensitive information that will be stored in the primary organization secret store and replaced with the secret store paths when sent outside, or secret references stored in a defined secret store. Can&#x27;t be used together with &#x60;secrets&#x60;.  They can hold a nested structure but leaf objects need to be of type SecretReference, where:  - &#x60;store&#x60; is a Secret Store id. It can&#x27;t be &#x60;humanitec&#x60;. It&#x27;s mandatory if &#x60;ref&#x60; is defined and can&#x27;t be used in request payloads if &#x60;value&#x60; is defined.  - &#x60;ref&#x60; is the secret key in the format of the target store. It can&#x27;t be used in request payloads if &#x60;value&#x60; is defined.  - &#x60;version&#x60; is the version of the secret as defined in the target store. It can be defined only if &#x60;ref&#x60; is defined.  - &#x60;value&#x60; is the value to store in the organizations primary secret store. It can&#x27;t be used in request payloads if &#x60;ref&#x60; is defined. | [optional] 
**secrets** | **dict(str, object)** | Secrets section of the data set. Sensitive information is stored in the primary organization secret store and replaced with the secret store paths when sent outside. Can&#x27;t be used together with &#x60;secret_refs&#x60;. | [optional] 
**values** | **dict(str, object)** | Values section of the data set. Passed around as-is. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

