# EnvironmentDefinitionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_deploy_id** | **str** | Defines the existing Deployment the new Environment will be based on. | 
**id** | **str** | The ID the Environment is referenced as. | 
**name** | **str** | The Human-friendly name for the Environment. | 
**type** | **str** | The Environment Type. This is used for organizing and managing Environments. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

