# swagger_client.AccountTypeApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_resources_account_types_get**](AccountTypeApi.md#orgs_org_id_resources_account_types_get) | **GET** /orgs/{orgId}/resources/account-types | List Resource Account Types available to the organization.

# **orgs_org_id_resources_account_types_get**
> list[AccountTypeResponse] orgs_org_id_resources_account_types_get(org_id)

List Resource Account Types available to the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccountTypeApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Resource Account Types available to the organization.
    api_response = api_instance.orgs_org_id_resources_account_types_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountTypeApi->orgs_org_id_resources_account_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[AccountTypeResponse]**](AccountTypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

