# RunResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The unique id of the Run. | [optional] 
**etag** | **str** | The current entity tag value for this Run. | [optional] 
**org_id** | **str** | The id of the Organization containing this Run. | [optional] 
**app_id** | **str** | The id of the Application containing this Run. | [optional] 
**pipeline_id** | **str** | The id of the Pipeline associated with the Run. | [optional] 
**pipeline_version** | **str** | The id of the Pipeline Version associated with the Run. | [optional] 
**status** | **str** | The current status of this Run. | [optional] 
**status_message** | **str** | A human-readable message indicating the reason for the status. | [optional] 
**created_at** | **datetime** | The date and time when this Run was first created. | [optional] 
**executing_at** | **datetime** | The date and time when this Run entered executing status. | [optional] 
**cancellation_requested_at** | **datetime** | The date and time when cancellation of this Run was requested. | [optional] 
**completed_at** | **datetime** | The date and time when this Run entered a successful, failed, or cancelled status. | [optional] 
**timeout_seconds** | **int** | The timeout for this Run. | [optional] 
**inputs** | **dict(str, object)** | The inputs that were provided for this Run. | [optional] 
**run_as** | **str** | The user id that the pipeline run is executing as when it calls Humanitec APIs. | [optional] 
**concurrency_group** | **str** | The optional concurrency group for this run within the application | [optional] 
**waiting_for** | **dict(str, str)** | Aggregated events on which run&#x27;s jobs are waiting for | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

