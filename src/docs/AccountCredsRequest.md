# AccountCredsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expires** | **str** | Account credentials expiration timestamp. | [optional] 
**password** | **str** | Account password or token secret. | 
**username** | **str** | Security account login or token. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

