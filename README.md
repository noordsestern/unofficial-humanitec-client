[![PyPi license](https://badgen.net/gitlab/license/Noordsestern/unofficial-humanitec-client/)](https://pypi.com/project/unofficial-humanitec-client/) [![PyPI download month](https://img.shields.io/pypi/dm/unofficial-humanitec-client.svg)](https://pypi.python.org/pypi/unofficial-humanitec-client/) 

# Unofficial Client for Humanitec API

Based on generated client with swagger-codegen. Contains a few tweaks to tackle generation bugs from swagger-codegen. [Full generated documentation](src/README.md) of generated client.

This is for testing only. If you need to use a python-client in production, rather generate maintain one by yourself. 

However, if you just need a python module ready to access Humanitec API, run:

```
pip install unofficial-humanitec-client
```

# Support

Openapi specifications and generated clients are not perfect. Generators contain bugs, specifications are not deterministic. Here you may find support:

## Change Requests

### Open an Issue

If you require changes (either new feature of a bugfix) best choice us [opening an issue](https://gitlab.com/noordsestern/unofficial-humanitec-client/-/issues) at this project. You do require a Gitlab account, though.

### Direct Contact

This is an open source project built around specific use cases. If your use case is not covered and your issue is not prioritized fast enough, you can make [direct contact](mailto:markus.i.sverige@googlemail.com). *Sponsoring will definitely prioritize your ticket.*

Your email won't be published automatically at the issue board and remains confidential (as confidential as Gmail permits).

## Humanitec API

See [official documentation](https://api-docs.humanitec.com/) for information provided directly by Humanitec.


-----

# Trouble Shooting

Openapi specifications are not totally precise and code generators create 90% good code, but also a few bugs. Known issues that require some workaround are listed here.

## Authentication

Unfortunately, there is mix of missing security schemes in the original Humanitec specification (from version 0.24.1 on) and several bug in the swagger-codegen breaking the use of bearer authentication. As workaround use the folloying snippet:

```python
configuration = Configuration()
configuration.api_key['Authorization']='<your humanitec api token>'
configuration.api_key_prefix['Authorization']='Bearer'
api_client = swagger_client.ApiClient(configuration=configuration)
```

Use that api_client as base client for every api_instance you create, i.e.

```python
def get_all_apps(org_id: str):
    # Use api_client with bearer authentication configured
    api_instance = swagger_client.ApplicationApi(api_client=api_client)

    try:
        # List all Applications in an Organization.
        api_response = api_instance.orgs_org_id_apps_get(org_id)
        print(api_response)
    except ApiException as e:
        print("Exception when calling ApplicationApi->orgs_org_id_apps_get: %s\n" % e)
```

## Invalid value for '<some method>', must not be 'None'

**Please [create an issue](https://gitlab.com/noordsestern/unofficial-humanitec-client/-/issues) for this error when you come across this error message. Collecting these issues will help creating a generic fix.**

Some endpoints cannot deal with null values. On the one side nullable fields are missing from the original Humanitec specification (0.24.1), but even when added, swagger-codegen still makes all fields required.

Known endpoints running in to this bug are `delta` endpoints. In that case, you have to rely on `reuests` module, which begs the question why using a generated client at all...

----

# License

The original openapi specification is published under Apache 2 license. This generated client is published under EUROPEAN UNION PUBLIC LICENCE v. 1.2 
