# RunJobStepLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**at** | **datetime** | The date and time when this message was emitted or captured. | 
**level** | **str** | The log level of the message. | 
**message** | **str** | The content of the message. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

