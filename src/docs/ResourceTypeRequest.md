# ResourceTypeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **str** | Category name (used to group similar resources on the UI). | [optional] 
**inputs_schema** | **dict(str, object)** | A JSON Schema specifying the type-specific parameters for the driver (input). | [optional] 
**name** | **str** | Display name. | [optional] 
**outputs_schema** | **dict(str, object)** | A JSON Schema specifying the type-specific data passed to the deployment (output). | [optional] 
**type** | **str** | Unique resource type identifier (system-wide, across all organizations). | 
**use** | **str** | Kind of dependency between resource of this type and a workload. It should be one of: &#x60;direct&#x60;, &#x60;indirect&#x60;, &#x60;implicit&#x60;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

