# AutomationRuleRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** | Whether the rule will be processed or not. | [optional] 
**artefacts_filter** | **list[str]** | A list of artefact names to be processed by the rule. If the array is empty, it implies include all. If &#x60;exclude_artefacts_filter&#x60; is true, this list describes the artefacts to exclude. | [optional] 
**exclude_artefacts_filter** | **bool** | Whether the artefacts specified in &#x60;artefacts_filter&#x60; should be excluded (true) or included (false) in the automation rule. | [optional] 
**exclude_images_filter** | **bool** | DEPRECATED: Whether the images specified in &#x60;images_filter&#x60; should be excluded (true) or included (false) in the automation rule. | [optional] 
**images_filter** | **list[str]** | DEPRECATED: A list of image IDs to be processed by the rule. If the array is empty, it implies include all. If &#x60;exclude_images_filter&#x60; is true, this list describes images to exclude. | [optional] 
**match** | **str** | DEPRECATED: A regular expression applied to the branch or tag name depending on the value of &#x60;update_to&#x60;. Defaults to match all if omitted or empty. | [optional] 
**match_ref** | **str** | A regular expression applied to the ref of a new artefact version. Defaults to match all if omitted or empty. | [optional] 
**type** | **str** | Specifies the type of event. Currently, only updates to either branches or tags are supported. Must be &#x60;\&quot;update\&quot;&#x60;. | 
**update_to** | **str** | DEPRECATED: Specifies whether the update occurs on commit to branch or creation of tag. Must be one of &#x60;\&quot;branch\&quot;&#x60; or &#x60;\&quot;tag\&quot;&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

