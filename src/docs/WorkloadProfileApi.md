# swagger_client.WorkloadProfileApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_workload_profile_chart_version**](WorkloadProfileApi.md#create_workload_profile_chart_version) | **POST** /orgs/{orgId}/workload-profile-chart-versions | Add new Workload Profile Chart Version
[**create_workload_profile_version**](WorkloadProfileApi.md#create_workload_profile_version) | **POST** /orgs/{orgId}/workload-profiles/{profileQid}/versions | Add new Version of the Workload Profile
[**get_latest_workload_profile_version**](WorkloadProfileApi.md#get_latest_workload_profile_version) | **GET** /orgs/{orgId}/workload-profiles/{profileQid}/versions/latest | Latest version of the given workload profile with optional constraint.
[**list_workload_profile_chart_versions**](WorkloadProfileApi.md#list_workload_profile_chart_versions) | **GET** /orgs/{orgId}/workload-profile-chart-versions | Workload Profile Chart Versions for the given organization.
[**list_workload_profile_versions**](WorkloadProfileApi.md#list_workload_profile_versions) | **GET** /orgs/{orgId}/workload-profiles/{profileQid}/versions | List versions of the given workload profile.
[**orgs_org_id_workload_profiles_get**](WorkloadProfileApi.md#orgs_org_id_workload_profiles_get) | **GET** /orgs/{orgId}/workload-profiles | List workload profiles available to the organization.
[**orgs_org_id_workload_profiles_post**](WorkloadProfileApi.md#orgs_org_id_workload_profiles_post) | **POST** /orgs/{orgId}/workload-profiles | Create new Workload Profile
[**orgs_org_id_workload_profiles_profile_id_versions_version_delete**](WorkloadProfileApi.md#orgs_org_id_workload_profiles_profile_id_versions_version_delete) | **DELETE** /orgs/{orgId}/workload-profiles/{profileId}/versions/{version} | Delete a Workload Profile Version
[**orgs_org_id_workload_profiles_profile_qid_delete**](WorkloadProfileApi.md#orgs_org_id_workload_profiles_profile_qid_delete) | **DELETE** /orgs/{orgId}/workload-profiles/{profileQid} | Delete a Workload Profile
[**orgs_org_id_workload_profiles_profile_qid_get**](WorkloadProfileApi.md#orgs_org_id_workload_profiles_profile_qid_get) | **GET** /orgs/{orgId}/workload-profiles/{profileQid} | Get a Workload Profile

# **create_workload_profile_chart_version**
> WorkloadProfileChartVersionResponse create_workload_profile_chart_version(file, org_id)

Add new Workload Profile Chart Version

Creates a Workload Profile Chart Version from the uploaded Helm chart. The name and version is retrieved from the chart's metadata (Charts.yaml file).  The request has content type `multipart/form-data` and the request body includes one part:  1. `file` with `application/x-gzip` content type which is an archive containing a Helm chart.  Request body example:   Content-Type: multipart/form-data; boundary=----boundary  ----boundary  Content-Disposition: form-data; name=\"file\"; filename=\"my-workload-1.0.1.tgz\"  Content-Type: application/x-gzip  [TGZ_DATA]  ----boundary

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
file = 'file_example' # str | 
org_id = 'org_id_example' # str | The Organization ID

try:
    # Add new Workload Profile Chart Version
    api_response = api_instance.create_workload_profile_chart_version(file, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->create_workload_profile_chart_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **str**|  | 
 **org_id** | **str**| The Organization ID | 

### Return type

[**WorkloadProfileChartVersionResponse**](WorkloadProfileChartVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_workload_profile_version**
> WorkloadProfileVersionResponse create_workload_profile_version(body, org_id, profile_qid)

Add new Version of the Workload Profile

Creates a Workload Profile Version for the given Workload Profile.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
body = swagger_client.WorkloadProfileVersionRequest() # WorkloadProfileVersionRequest | Workload profile version metadata.


org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.

try:
    # Add new Version of the Workload Profile
    api_response = api_instance.create_workload_profile_version(body, org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->create_workload_profile_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WorkloadProfileVersionRequest**](WorkloadProfileVersionRequest.md)| Workload profile version metadata.

 | 
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 

### Return type

[**WorkloadProfileVersionResponse**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_latest_workload_profile_version**
> WorkloadProfileVersionResponse get_latest_workload_profile_version(org_id, profile_qid)

Latest version of the given workload profile with optional constraint.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.

try:
    # Latest version of the given workload profile with optional constraint.
    api_response = api_instance.get_latest_workload_profile_version(org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->get_latest_workload_profile_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 

### Return type

[**WorkloadProfileVersionResponse**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_workload_profile_chart_versions**
> list[WorkloadProfileChartVersionResponse] list_workload_profile_chart_versions(org_id, per_page=per_page, page=page)

Workload Profile Chart Versions for the given organization.

Returns all Workload Profile Chart Versions for the given organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # Workload Profile Chart Versions for the given organization.
    api_response = api_instance.list_workload_profile_chart_versions(org_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->list_workload_profile_chart_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileChartVersionResponse]**](WorkloadProfileChartVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_workload_profile_versions**
> list[WorkloadProfileVersionResponse] list_workload_profile_versions(org_id, profile_qid, per_page=per_page, page=page)

List versions of the given workload profile.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID
profile_qid = 'profile_qid_example' # str | The Workload Profile ID.
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List versions of the given workload profile.
    api_response = api_instance.list_workload_profile_versions(org_id, profile_qid, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->list_workload_profile_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **profile_qid** | **str**| The Workload Profile ID. | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileVersionResponse]**](WorkloadProfileVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_get**
> list[WorkloadProfileResponse] orgs_org_id_workload_profiles_get(org_id, per_page=per_page, page=page)

List workload profiles available to the organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)

try:
    # List workload profiles available to the organization.
    api_response = api_instance.orgs_org_id_workload_profiles_get(org_id, per_page=per_page, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->orgs_org_id_workload_profiles_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 

### Return type

[**list[WorkloadProfileResponse]**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_post**
> WorkloadProfileResponse orgs_org_id_workload_profiles_post(body, org_id)

Create new Workload Profile

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
body = swagger_client.WorkloadProfileRequest() # WorkloadProfileRequest | Workload profile details.


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Create new Workload Profile
    api_response = api_instance.orgs_org_id_workload_profiles_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->orgs_org_id_workload_profiles_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WorkloadProfileRequest**](WorkloadProfileRequest.md)| Workload profile details.

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**WorkloadProfileResponse**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_id_versions_version_delete**
> orgs_org_id_workload_profiles_profile_id_versions_version_delete(org_id, profile_id, version)

Delete a Workload Profile Version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_id = 'profile_id_example' # str | The Workload profile ID.  
version = 'version_example' # str | The Version.  

try:
    # Delete a Workload Profile Version
    api_instance.orgs_org_id_workload_profiles_profile_id_versions_version_delete(org_id, profile_id, version)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->orgs_org_id_workload_profiles_profile_id_versions_version_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_id** | **str**| The Workload profile ID.   | 
 **version** | **str**| The Version.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_qid_delete**
> orgs_org_id_workload_profiles_profile_qid_delete(org_id, profile_qid)

Delete a Workload Profile

This will also delete all versions of a workload profile.  It is not possible to delete profiles of other organizations.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_qid = 'profile_qid_example' # str | The Workload profile ID.  

try:
    # Delete a Workload Profile
    api_instance.orgs_org_id_workload_profiles_profile_qid_delete(org_id, profile_qid)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->orgs_org_id_workload_profiles_profile_qid_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_qid** | **str**| The Workload profile ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_workload_profiles_profile_qid_get**
> WorkloadProfileResponse orgs_org_id_workload_profiles_profile_qid_get(org_id, profile_qid)

Get a Workload Profile

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.WorkloadProfileApi()
org_id = 'org_id_example' # str | The Organization ID.  
profile_qid = 'profile_qid_example' # str | The fully qualified Workload ID. (If not a profile from the current org, must be prefixed with `{orgId}.` e.g. `humanitec.default-cronjob`)  

try:
    # Get a Workload Profile
    api_response = api_instance.orgs_org_id_workload_profiles_profile_qid_get(org_id, profile_qid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkloadProfileApi->orgs_org_id_workload_profiles_profile_qid_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **profile_qid** | **str**| The fully qualified Workload ID. (If not a profile from the current org, must be prefixed with &#x60;{orgId}.&#x60; e.g. &#x60;humanitec.default-cronjob&#x60;)   | 

### Return type

[**WorkloadProfileResponse**](WorkloadProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

