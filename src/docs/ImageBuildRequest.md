# ImageBuildRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch** | **str** | The branch name of the branch the build was built on | [optional] 
**commit** | **str** | The commit ID that this build was built from. | [optional] 
**image** | **str** | The fully qualified Image URL including registry, repository and tag. | [optional] 
**tags** | **list[str]** | The tag that the build was built from. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

