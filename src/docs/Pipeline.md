# Pipeline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The id of the Pipeline. | 
**etag** | **str** | The current entity tag value for this Pipeline. | 
**org_id** | **str** | The id of the Organization containing this Pipeline. | 
**app_id** | **str** | The id of the Application containing this Pipeline. | 
**name** | **str** | The name of the Pipeline. | 
**status** | **str** | The current status of the Pipeline. | 
**version** | **str** | The unique id of the current Pipeline Version. | 
**created_at** | **datetime** | The date and time when the Pipeline was created. | 
**trigger_types** | **list[str]** | The list of trigger types in the current schema. | 
**metadata** | **dict(str, str)** | The map of key value pipeline additional information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

