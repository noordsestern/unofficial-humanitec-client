# WorkloadProfileVersionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** | Creation date | 
**created_by** | **str** | User created the profile | 
**features** | **dict(str, object)** | A map of Features. If referencing built in Humanitec features, the fully qualified feature name must be used: e.g. &#x60;humanitec/annotations&#x60;.  {  } | 
**spec_schema** | **object** | OpenAPI schema used to validate the spec. | 
**spec_definition** | [**WorkloadProfileVersionSpecDefinition**](WorkloadProfileVersionSpecDefinition.md) |  | [optional] 
**notes** | **str** | Notes | 
**org_id** | **str** | Organization ID | 
**profile_id** | **str** | Workload Profile ID | 
**version** | **str** | Version | 
**workload_profile_chart** | [**WorkloadProfileChartReference**](WorkloadProfileChartReference.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

