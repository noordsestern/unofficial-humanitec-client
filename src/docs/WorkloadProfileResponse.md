# WorkloadProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** | Creation date | 
**created_by** | **str** | User created the profile | 
**id** | **str** | Workload Profile ID | 
**latest** | **str** | The latest version of the profile | 
**org_id** | **str** | Organization ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

