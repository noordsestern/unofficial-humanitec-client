# swagger_client.UserProfileApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**current_user_get**](UserProfileApi.md#current_user_get) | **GET** /current-user | Gets the extended profile of the current user
[**current_user_patch**](UserProfileApi.md#current_user_patch) | **PATCH** /current-user | Updates the extended profile of the current user.
[**orgs_org_id_users_post**](UserProfileApi.md#orgs_org_id_users_post) | **POST** /orgs/{orgId}/users | Creates a new service user.
[**tokens_get**](UserProfileApi.md#tokens_get) | **GET** /tokens | DEPRECATED
[**tokens_token_id_delete**](UserProfileApi.md#tokens_token_id_delete) | **DELETE** /tokens/{tokenId} | DEPRECATED
[**users_me_get**](UserProfileApi.md#users_me_get) | **GET** /users/me | DEPRECATED

# **current_user_get**
> UserProfileExtendedResponse current_user_get()

Gets the extended profile of the current user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()

try:
    # Gets the extended profile of the current user
    api_response = api_instance.current_user_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserProfileApi->current_user_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserProfileExtendedResponse**](UserProfileExtendedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **current_user_patch**
> UserProfileExtendedResponse current_user_patch(body)

Updates the extended profile of the current user.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()
body = swagger_client.UserProfileExtendedRequest() # UserProfileExtendedRequest | 

try:
    # Updates the extended profile of the current user.
    api_response = api_instance.current_user_patch(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserProfileApi->current_user_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserProfileExtendedRequest**](UserProfileExtendedRequest.md)|  | 

### Return type

[**UserProfileExtendedResponse**](UserProfileExtendedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_post**
> UserProfileResponse orgs_org_id_users_post(body, org_id)

Creates a new service user.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()
body = swagger_client.NewServiceUserRequest() # NewServiceUserRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Creates a new service user.
    api_response = api_instance.orgs_org_id_users_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserProfileApi->orgs_org_id_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NewServiceUserRequest**](NewServiceUserRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**UserProfileResponse**](UserProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tokens_get**
> dict(str, object) tokens_get()

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()

try:
    # DEPRECATED
    api_response = api_instance.tokens_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserProfileApi->tokens_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**dict(str, object)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tokens_token_id_delete**
> tokens_token_id_delete(token_id)

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()
token_id = 'token_id_example' # str | The token ID  

try:
    # DEPRECATED
    api_instance.tokens_token_id_delete(token_id)
except ApiException as e:
    print("Exception when calling UserProfileApi->tokens_token_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token_id** | **str**| The token ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_get**
> dict(str, object) users_me_get()

DEPRECATED

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserProfileApi()

try:
    # DEPRECATED
    api_response = api_instance.users_me_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserProfileApi->users_me_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**dict(str, object)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

