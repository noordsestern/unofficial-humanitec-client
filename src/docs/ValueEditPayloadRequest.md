# ValueEditPayloadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 
**value** | **str** |  | [optional] 
**is_secret** | **bool** |  | [optional] 
**key** | **str** |  | [optional] 
**secret_ref** | [**SecretReference**](SecretReference.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

