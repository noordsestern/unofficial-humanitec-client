# RunJobResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**steps** | [**list[RunJobStep]**](RunJobStep.md) | The collection of Steps that completed along with the current Step being executed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

