# ControllerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **str** |  | 
**message** | **str** |  | 
**pods** | [**list[PodStateResponse]**](PodStateResponse.md) |  | 
**replicas** | **int** |  | 
**revision** | **int** |  | 
**status** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

