# coding: utf-8

"""
    Humanitec API

    # Introduction The *Humanitec API* allows you to automate and integrate Humanitec into your developer and operational workflows. The API is a REST based API. It is based around a set of concepts:  * Core * External Resources * Sets and Deltas  ## Authentication Almost all requests made to the Humanitec API require Authentication. Humanitec provides 2 ways of authenticating with the API: `Bearer` and `JWT`.  ### Bearer Authentication This form of authentication makes use of a **static token**. It is intended to be used when machines interact with the Humanitec API. Bearer tokens should be used for very narrow purposes. This allows for the token to be revoked if it is compromised and so limit the scope of exposure. New Bearer tokens can be obtained via the UI:  1. Log into Humanitec at https://app.humanitec.io 1. Go to **Organization Settings** 1. Select **API tokens** 1. Enter a *name* for the new token and click on **Generate new token**  The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_TOKEN`, the request could be made as follows:  ```     curl -H 'Authorization: Bearer HUMANITEC_TOKEN' https://api.humanitec.io/orgs/my-org/apps ```  ### JWT Authentication This form of authentication makes use of a **JSON Web Token (JWT)**. It is intended to be used when humans interact with the Humanitec API. JWTs expire after a period of time. This means that a new JWT will need to be generated regularly. This makes them well suited to working in short sessions, but not for automation. (See Bearer Authentication.) The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_JWT`, the request could be made as follows:  ```     curl -H 'Authorization: JWT HUMANITEC_JWT' https://api.humanitec.io/orgs/my-org/apps ```  ## Content Types All of the Humanitec API unless explicitly only accepts content types of `application/json` and will always return valid `application/json` or an empty response.  ## Response Codes ### Success Any response code in the `2xx` range should be regarded as success.  | **Code** | **Meaning** | | --- | --- | | `200` | Success | | `201` | Success (In future, `201` will be replaced by `200`) | | `204` | Success, but no content in response |  _Note: We plan to simplify the interface by replacing 201 with 200 status codes._  ### Failure Any response code in the `4xx` should be regarded as an error which can be rectified by the client. `5xx` error codes indicate errors that cannot be corrected by the client.  | **Code** | **Meaning** | | --- | --- | | `400` | General error. (Body will contain details) | | `401` | Attempt to access protected resource without `Authorization` Header. | | `403` | The `Bearer` or `JWT` does not grant access to the requested resource. | | `404` | Resource not found. | | `405` | Method not allowed | | `409` | Conflict. Usually indicated a resource with that ID already exists. | | `422` | Unprocessable Entity. The body was not valid JSON, was empty or contained an object different from what was expected. | | `429` | Too many requests - request rate limit has been reached. | | `500` | Internal Error. If it occurs repeatedly, contact support. |   # noqa: E501

    OpenAPI spec version: 0.24.1
    Contact: apiteam@humanitec.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class HumanitecPublicKey(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'pub_key': 'str',
        'active': 'bool',
        'created_at': 'datetime',
        'updated_at': 'datetime',
        'expired_at': 'datetime'
    }

    attribute_map = {
        'id': 'id',
        'pub_key': 'pub_key',
        'active': 'active',
        'created_at': 'created_at',
        'updated_at': 'updated_at',
        'expired_at': 'expired_at'
    }

    def __init__(self, id=None, pub_key=None, active=None, created_at=None, updated_at=None, expired_at=None):  # noqa: E501
        """HumanitecPublicKey - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._pub_key = None
        self._active = None
        self._created_at = None
        self._updated_at = None
        self._expired_at = None
        self.discriminator = None
        self.id = id
        self.pub_key = pub_key
        self.active = active
        self.created_at = created_at
        self.updated_at = updated_at
        self.expired_at = expired_at

    @property
    def id(self):
        """Gets the id of this HumanitecPublicKey.  # noqa: E501


        :return: The id of this HumanitecPublicKey.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this HumanitecPublicKey.


        :param id: The id of this HumanitecPublicKey.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def pub_key(self):
        """Gets the pub_key of this HumanitecPublicKey.  # noqa: E501


        :return: The pub_key of this HumanitecPublicKey.  # noqa: E501
        :rtype: str
        """
        return self._pub_key

    @pub_key.setter
    def pub_key(self, pub_key):
        """Sets the pub_key of this HumanitecPublicKey.


        :param pub_key: The pub_key of this HumanitecPublicKey.  # noqa: E501
        :type: str
        """
        if pub_key is None:
            raise ValueError("Invalid value for `pub_key`, must not be `None`")  # noqa: E501

        self._pub_key = pub_key

    @property
    def active(self):
        """Gets the active of this HumanitecPublicKey.  # noqa: E501


        :return: The active of this HumanitecPublicKey.  # noqa: E501
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active):
        """Sets the active of this HumanitecPublicKey.


        :param active: The active of this HumanitecPublicKey.  # noqa: E501
        :type: bool
        """
        if active is None:
            raise ValueError("Invalid value for `active`, must not be `None`")  # noqa: E501

        self._active = active

    @property
    def created_at(self):
        """Gets the created_at of this HumanitecPublicKey.  # noqa: E501


        :return: The created_at of this HumanitecPublicKey.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this HumanitecPublicKey.


        :param created_at: The created_at of this HumanitecPublicKey.  # noqa: E501
        :type: datetime
        """
        if created_at is None:
            raise ValueError("Invalid value for `created_at`, must not be `None`")  # noqa: E501

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this HumanitecPublicKey.  # noqa: E501


        :return: The updated_at of this HumanitecPublicKey.  # noqa: E501
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this HumanitecPublicKey.


        :param updated_at: The updated_at of this HumanitecPublicKey.  # noqa: E501
        :type: datetime
        """
        if updated_at is None:
            raise ValueError("Invalid value for `updated_at`, must not be `None`")  # noqa: E501

        self._updated_at = updated_at

    @property
    def expired_at(self):
        """Gets the expired_at of this HumanitecPublicKey.  # noqa: E501


        :return: The expired_at of this HumanitecPublicKey.  # noqa: E501
        :rtype: datetime
        """
        return self._expired_at

    @expired_at.setter
    def expired_at(self, expired_at):
        """Sets the expired_at of this HumanitecPublicKey.


        :param expired_at: The expired_at of this HumanitecPublicKey.  # noqa: E501
        :type: datetime
        """
        if expired_at is None:
            raise ValueError("Invalid value for `expired_at`, must not be `None`")  # noqa: E501

        self._expired_at = expired_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(HumanitecPublicKey, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, HumanitecPublicKey):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
