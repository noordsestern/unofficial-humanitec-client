# DeploymentErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | 
**error_type** | **str** |  | 
**message** | **str** |  | 
**object_id** | **str** |  | 
**scope** | **str** |  | 
**summary** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

