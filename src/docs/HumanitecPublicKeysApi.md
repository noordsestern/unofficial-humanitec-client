# swagger_client.HumanitecPublicKeysApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_humanitec_public_keys**](HumanitecPublicKeysApi.md#get_humanitec_public_keys) | **GET** /orgs/{orgId}/humanitec-keys | List all the public keys Humanitec shares with an organization.

# **get_humanitec_public_keys**
> list[HumanitecPublicKey] get_humanitec_public_keys(org_id, active=active)

List all the public keys Humanitec shares with an organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HumanitecPublicKeysApi()
org_id = 'org_id_example' # str | The organization ID.
active = true # bool | If set to true, the response includes only the active key, if set to false only non-active keys, otherwise both active and non-active keys. (optional)

try:
    # List all the public keys Humanitec shares with an organization.
    api_response = api_instance.get_humanitec_public_keys(org_id, active=active)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HumanitecPublicKeysApi->get_humanitec_public_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID. | 
 **active** | **bool**| If set to true, the response includes only the active key, if set to false only non-active keys, otherwise both active and non-active keys. | [optional] 

### Return type

[**list[HumanitecPublicKey]**](HumanitecPublicKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

