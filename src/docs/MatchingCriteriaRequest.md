# MatchingCriteriaRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | **str** | (Optional) The ID of the Application that the Resources should belong to. | [optional] 
**_class** | **str** | (Optional) The class of the Resource in the Deployment Set. Can not be empty, if is not defined, set to &#x60;default&#x60;. | [optional] 
**env_id** | **str** | (Optional) The ID of the Environment that the Resources should belong to. If &#x60;env_type&#x60; is also set, it must match the Type of the Environment for the Criteria to match. | [optional] 
**env_type** | **str** | (Optional) The Type of the Environment that the Resources should belong to. If &#x60;env_id&#x60; is also set, it must have an Environment Type that matches this parameter for the Criteria to match. | [optional] 
**id** | **str** | Matching Criteria ID | [optional] 
**res_id** | **str** | (Optional) The ID of the Resource in the Deployment Set. The ID is normally a &#x60;.&#x60; separated path to the definition in the set, e.g. &#x60;modules.my-module.externals.my-database&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

