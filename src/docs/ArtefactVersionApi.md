# swagger_client.ArtefactVersionApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_artefact_versions_artefact_version_id_get**](ArtefactVersionApi.md#orgs_org_id_artefact_versions_artefact_version_id_get) | **GET** /orgs/{orgId}/artefact-versions/{artefactVersionId} | Get an Artefacts Versions.
[**orgs_org_id_artefact_versions_get**](ArtefactVersionApi.md#orgs_org_id_artefact_versions_get) | **GET** /orgs/{orgId}/artefact-versions | List all Artefacts Versions.
[**orgs_org_id_artefact_versions_post**](ArtefactVersionApi.md#orgs_org_id_artefact_versions_post) | **POST** /orgs/{orgId}/artefact-versions | Register a new Artefact Version with your organization.
[**orgs_org_id_artefacts_artefact_id_versions_get**](ArtefactVersionApi.md#orgs_org_id_artefacts_artefact_id_versions_get) | **GET** /orgs/{orgId}/artefacts/{artefactId}/versions | List all Artefact Versions of an Artefact.
[**orgs_org_id_artefacts_artefact_id_versions_version_id_patch**](ArtefactVersionApi.md#orgs_org_id_artefacts_artefact_id_versions_version_id_patch) | **PATCH** /orgs/{orgId}/artefacts/{artefactId}/versions/{versionId} | Update Version of an Artefact.

# **orgs_org_id_artefact_versions_artefact_version_id_get**
> ArtefactVersionResponse orgs_org_id_artefact_versions_artefact_version_id_get(org_id, artefact_version_id)

Get an Artefacts Versions.

Returns a specific Artefact Version.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactVersionApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_version_id = 'artefact_version_id_example' # str | The Artefact Version ID.  

try:
    # Get an Artefacts Versions.
    api_response = api_instance.orgs_org_id_artefact_versions_artefact_version_id_get(org_id, artefact_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactVersionApi->orgs_org_id_artefact_versions_artefact_version_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_version_id** | **str**| The Artefact Version ID.   | 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefact_versions_get**
> list[ArtefactVersionResponse] orgs_org_id_artefact_versions_get(org_id, name=name, reference=reference, archived=archived)

List all Artefacts Versions.

Returns the Artefact Versions registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactVersionApi()
org_id = 'org_id_example' # str | The organization ID.  
name = 'name_example' # str | (Optional) Filter Artefact Versions by name.   (optional)
reference = 'reference_example' # str | (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with `name`.   (optional)
archived = 'archived_example' # str | (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   (optional)

try:
    # List all Artefacts Versions.
    api_response = api_instance.orgs_org_id_artefact_versions_get(org_id, name=name, reference=reference, archived=archived)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactVersionApi->orgs_org_id_artefact_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **name** | **str**| (Optional) Filter Artefact Versions by name.   | [optional] 
 **reference** | **str**| (Optional) Filter Artefact Versions by the reference to a Version of the same Artefact. This cannot be used together with &#x60;name&#x60;.   | [optional] 
 **archived** | **str**| (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   | [optional] 

### Return type

[**list[ArtefactVersionResponse]**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefact_versions_post**
> ArtefactVersionResponse orgs_org_id_artefact_versions_post(body, org_id, vcs=vcs)

Register a new Artefact Version with your organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactVersionApi()
body = swagger_client.AddArtefactVersionPayloadRequest() # AddArtefactVersionPayloadRequest | The data needed to register a new Artefact Version within the organization.


org_id = 'org_id_example' # str | The organization ID.  
vcs = 'vcs_example' # str | (Optional) Which version control system the version comes from. Default value is \"git\". If this parameter is not supplied or its value is \"git\", the provided ref, if not empty, is checked to ensure that it has the prefix \"refs/\".   (optional)

try:
    # Register a new Artefact Version with your organization.
    api_response = api_instance.orgs_org_id_artefact_versions_post(body, org_id, vcs=vcs)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactVersionApi->orgs_org_id_artefact_versions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddArtefactVersionPayloadRequest**](AddArtefactVersionPayloadRequest.md)| The data needed to register a new Artefact Version within the organization.

 | 
 **org_id** | **str**| The organization ID.   | 
 **vcs** | **str**| (Optional) Which version control system the version comes from. Default value is \&quot;git\&quot;. If this parameter is not supplied or its value is \&quot;git\&quot;, the provided ref, if not empty, is checked to ensure that it has the prefix \&quot;refs/\&quot;.   | [optional] 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_artefact_id_versions_get**
> list[ArtefactVersionResponse] orgs_org_id_artefacts_artefact_id_versions_get(org_id, artefact_id, archived=archived, reference=reference, limit=limit)

List all Artefact Versions of an Artefact.

Returns the Artefact Versions of a specified Artefact registered with your organization. If no elements are found, an empty list is returned.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactVersionApi()
org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  
archived = 'archived_example' # str | (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   (optional)
reference = 'reference_example' # str | (Optional) Filter Artefact Versions by by name including a version or digest.   (optional)
limit = 'limit_example' # str | (Optional) Limit the number of versions returned by the endpoint.   (optional)

try:
    # List all Artefact Versions of an Artefact.
    api_response = api_instance.orgs_org_id_artefacts_artefact_id_versions_get(org_id, artefact_id, archived=archived, reference=reference, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactVersionApi->orgs_org_id_artefacts_artefact_id_versions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 
 **archived** | **str**| (Optional) Filter for non-archived Artefact Versions. If no filter is defined only non-archived Artefact Versions are returned, if the filter is true both archived and non-archived Versions are returned.   | [optional] 
 **reference** | **str**| (Optional) Filter Artefact Versions by by name including a version or digest.   | [optional] 
 **limit** | **str**| (Optional) Limit the number of versions returned by the endpoint.   | [optional] 

### Return type

[**list[ArtefactVersionResponse]**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_artefacts_artefact_id_versions_version_id_patch**
> ArtefactVersionResponse orgs_org_id_artefacts_artefact_id_versions_version_id_patch(body, org_id, artefact_id, version_id)

Update Version of an Artefact.

Update the version of a specified Artefact registered with your organization\".

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ArtefactVersionApi()
body = swagger_client.UpdateArtefactVersionPayloadRequest() # UpdateArtefactVersionPayloadRequest | The Artefact Version Update Request. Only the field `archive` can be updated.


org_id = 'org_id_example' # str | The organization ID.  
artefact_id = 'artefact_id_example' # str | The Artefact ID.  
version_id = 'version_id_example' # str | The Version ID.  

try:
    # Update Version of an Artefact.
    api_response = api_instance.orgs_org_id_artefacts_artefact_id_versions_version_id_patch(body, org_id, artefact_id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtefactVersionApi->orgs_org_id_artefacts_artefact_id_versions_version_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateArtefactVersionPayloadRequest**](UpdateArtefactVersionPayloadRequest.md)| The Artefact Version Update Request. Only the field &#x60;archive&#x60; can be updated.

 | 
 **org_id** | **str**| The organization ID.   | 
 **artefact_id** | **str**| The Artefact ID.   | 
 **version_id** | **str**| The Version ID.   | 

### Return type

[**ArtefactVersionResponse**](ArtefactVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

