# ImageBuildResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**added_at** | **str** | The time when the build was added to Humanitec. | [optional] 
**branch** | **str** | The branch name of the branch the build was built on | 
**commit** | **str** | The commit ID that this build was built from. | 
**image** | **str** | The fully qualified Image URL including registry, repository and tag. | 
**tags** | **list[str]** | The tag that the build was built from. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

