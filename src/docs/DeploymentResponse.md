# DeploymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | An optional comment to help communicate the purpose of the Deployment. | 
**created_at** | **str** | The Timestamp of when the Deployment was initiated. | 
**created_by** | **str** | The user who initiated the Deployment. | 
**delta_id** | **str** | ID of the Deployment Delta describing the changes to the current Environment for this Deployment. | [optional] 
**env_id** | **str** | The Environment where the Deployment occurred. | 
**export_file** | **str** |  | 
**export_status** | **str** |  | 
**from_id** | **str** | The ID of the Deployment that this Deployment was based on. | 
**id** | **str** | The ID of the Deployment. | 
**set_id** | **str** | ID of the Deployment Set describing the state of the Environment after Deployment. | 
**status** | **str** | The current status of the Deployment. Can be &#x60;pending&#x60;, &#x60;in progress&#x60;, &#x60;succeeded&#x60;, or &#x60;failed&#x60;. | 
**status_changed_at** | **str** | The timestamp of the last &#x60;status&#x60; change. If &#x60;status&#x60; is &#x60;succeeded&#x60; or &#x60;failed&#x60; it it will indicate when the Deployment finished. | 
**value_set_version_id** | **str** | ID of the Value Set Version describe the values to be used for this Deployment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

