# ProvisionDependenciesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_dependent** | **bool** | If the co-provisioned resource is dependendent on the current one. | 
**match_dependents** | **bool** | If the resources dependant on the main resource, are also dependant on the co-provisioned one. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

