# ResourceAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp of when the account was created. | 
**created_by** | **str** | The ID of the user who created the account. | 
**id** | **str** | Unique identifier for the account (in scope of the organization it belongs to). | 
**is_used** | **bool** | Indicates if this account is being used (referenced) by any resource definition. | 
**name** | **str** | Display name. | 
**type** | **str** | The type of the account | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

