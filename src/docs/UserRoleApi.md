# swagger_client.UserRoleApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_users_get**](UserRoleApi.md#orgs_org_id_apps_app_id_users_get) | **GET** /orgs/{orgId}/apps/{appId}/users | List Users with roles in an App
[**orgs_org_id_apps_app_id_users_post**](UserRoleApi.md#orgs_org_id_apps_app_id_users_post) | **POST** /orgs/{orgId}/apps/{appId}/users | Adds a User to an Application with a Role
[**orgs_org_id_apps_app_id_users_user_id_delete**](UserRoleApi.md#orgs_org_id_apps_app_id_users_user_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/users/{userId} | Remove the role of a User on an Application
[**orgs_org_id_apps_app_id_users_user_id_get**](UserRoleApi.md#orgs_org_id_apps_app_id_users_user_id_get) | **GET** /orgs/{orgId}/apps/{appId}/users/{userId} | Get the role of a User on an Application
[**orgs_org_id_apps_app_id_users_user_id_patch**](UserRoleApi.md#orgs_org_id_apps_app_id_users_user_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/users/{userId} | Update the role of a User on an Application
[**orgs_org_id_env_types_env_type_users_post**](UserRoleApi.md#orgs_org_id_env_types_env_type_users_post) | **POST** /orgs/{orgId}/env-types/{envType}/users | Adds a User to an Environment Type with a Role
[**orgs_org_id_env_types_env_type_users_user_id_delete**](UserRoleApi.md#orgs_org_id_env_types_env_type_users_user_id_delete) | **DELETE** /orgs/{orgId}/env-types/{envType}/users/{userId} | Remove the role of a User on an Environment Type
[**orgs_org_id_env_types_env_type_users_user_id_get**](UserRoleApi.md#orgs_org_id_env_types_env_type_users_user_id_get) | **GET** /orgs/{orgId}/env-types/{envType}/users/{userId} | Get the role of a User on an Environment Type
[**orgs_org_id_env_types_env_type_users_user_id_patch**](UserRoleApi.md#orgs_org_id_env_types_env_type_users_user_id_patch) | **PATCH** /orgs/{orgId}/env-types/{envType}/users/{userId} | Update the role of a User on an Environment Type
[**orgs_org_id_invitations_post**](UserRoleApi.md#orgs_org_id_invitations_post) | **POST** /orgs/{orgId}/invitations | Invites a user to an Organization with a specified role.
[**orgs_org_id_users_get**](UserRoleApi.md#orgs_org_id_users_get) | **GET** /orgs/{orgId}/users | List Users with roles in an Organization
[**orgs_org_id_users_user_id_delete**](UserRoleApi.md#orgs_org_id_users_user_id_delete) | **DELETE** /orgs/{orgId}/users/{userId} | Remove the role of a User on an Organization
[**orgs_org_id_users_user_id_get**](UserRoleApi.md#orgs_org_id_users_user_id_get) | **GET** /orgs/{orgId}/users/{userId} | Get the role of a User on an Organization
[**orgs_org_id_users_user_id_patch**](UserRoleApi.md#orgs_org_id_users_user_id_patch) | **PATCH** /orgs/{orgId}/users/{userId} | Update the role of a User on an Organization

# **orgs_org_id_apps_app_id_users_get**
> list[UserRoleResponse] orgs_org_id_apps_app_id_users_get(org_id, app_id)

List Users with roles in an App

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Users with roles in an App
    api_response = api_instance.orgs_org_id_apps_app_id_users_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_apps_app_id_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_post**
> UserRoleResponse orgs_org_id_apps_app_id_users_post(body, org_id, app_id)

Adds a User to an Application with a Role

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.UserRoleRequest() # UserRoleRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Adds a User to an Application with a Role
    api_response = api_instance.orgs_org_id_apps_app_id_users_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_apps_app_id_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserRoleRequest**](UserRoleRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_delete**
> orgs_org_id_apps_app_id_users_user_id_delete(org_id, app_id, user_id)

Remove the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Application
    api_instance.orgs_org_id_apps_app_id_users_user_id_delete(org_id, app_id, user_id)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_apps_app_id_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_get**
> UserRoleResponse orgs_org_id_apps_app_id_users_user_id_get(org_id, app_id, user_id)

Get the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Application
    api_response = api_instance.orgs_org_id_apps_app_id_users_user_id_get(org_id, app_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_apps_app_id_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_users_user_id_patch**
> UserRoleResponse orgs_org_id_apps_app_id_users_user_id_patch(body, org_id, app_id, user_id)

Update the role of a User on an Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user role


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Application
    api_response = api_instance.orgs_org_id_apps_app_id_users_user_id_patch(body, org_id, app_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_apps_app_id_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_post**
> UserRoleResponse orgs_org_id_env_types_env_type_users_post(body, org_id, env_type)

Adds a User to an Environment Type with a Role

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.UserRoleRequest() # UserRoleRequest | The user ID and the role


org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  

try:
    # Adds a User to an Environment Type with a Role
    api_response = api_instance.orgs_org_id_env_types_env_type_users_post(body, org_id, env_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_env_types_env_type_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserRoleRequest**](UserRoleRequest.md)| The user ID and the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_delete**
> orgs_org_id_env_types_env_type_users_user_id_delete(org_id, env_type, user_id)

Remove the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Environment Type
    api_instance.orgs_org_id_env_types_env_type_users_user_id_delete(org_id, env_type, user_id)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_env_types_env_type_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_get**
> UserRoleResponse orgs_org_id_env_types_env_type_users_user_id_get(org_id, env_type, user_id)

Get the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_users_user_id_get(org_id, env_type, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_env_types_env_type_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_env_types_env_type_users_user_id_patch**
> UserRoleResponse orgs_org_id_env_types_env_type_users_user_id_patch(body, org_id, env_type, user_id)

Update the role of a User on an Environment Type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user role


org_id = 'org_id_example' # str | The Organization ID.  
env_type = 'env_type_example' # str | The Environment Type.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Environment Type
    api_response = api_instance.orgs_org_id_env_types_env_type_users_user_id_patch(body, org_id, env_type, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_env_types_env_type_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **env_type** | **str**| The Environment Type.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_invitations_post**
> list[UserRoleResponse] orgs_org_id_invitations_post(body, org_id)

Invites a user to an Organization with a specified role.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.UserInviteRequestRequest() # UserInviteRequestRequest | The email and the desired role


org_id = 'org_id_example' # str | The Organization ID.  

try:
    # Invites a user to an Organization with a specified role.
    api_response = api_instance.orgs_org_id_invitations_post(body, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_invitations_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserInviteRequestRequest**](UserInviteRequestRequest.md)| The email and the desired role

 | 
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_get**
> list[UserRoleResponse] orgs_org_id_users_get(org_id)

List Users with roles in an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Users with roles in an Organization
    api_response = api_instance.orgs_org_id_users_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[UserRoleResponse]**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_delete**
> orgs_org_id_users_user_id_delete(org_id, user_id)

Remove the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Remove the role of a User on an Organization
    api_instance.orgs_org_id_users_user_id_delete(org_id, user_id)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_users_user_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_get**
> UserRoleResponse orgs_org_id_users_user_id_get(org_id, user_id)

Get the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Get the role of a User on an Organization
    api_response = api_instance.orgs_org_id_users_user_id_get(org_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_users_user_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_users_user_id_patch**
> UserRoleResponse orgs_org_id_users_user_id_patch(body, org_id, user_id)

Update the role of a User on an Organization

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UserRoleApi()
body = swagger_client.RoleRequest() # RoleRequest | The new user the role


org_id = 'org_id_example' # str | The Organization ID.  
user_id = 'user_id_example' # str | The User ID  

try:
    # Update the role of a User on an Organization
    api_response = api_instance.orgs_org_id_users_user_id_patch(body, org_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserRoleApi->orgs_org_id_users_user_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RoleRequest**](RoleRequest.md)| The new user the role

 | 
 **org_id** | **str**| The Organization ID.   | 
 **user_id** | **str**| The User ID   | 

### Return type

[**UserRoleResponse**](UserRoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

