# swagger_client.TokenInfoApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_user_id_tokens_get**](TokenInfoApi.md#users_user_id_tokens_get) | **GET** /users/{userId}/tokens | Lists tokens associated with a user
[**users_user_id_tokens_post**](TokenInfoApi.md#users_user_id_tokens_post) | **POST** /users/{userId}/tokens | Creates a new static token for a user.
[**users_user_id_tokens_token_id_delete**](TokenInfoApi.md#users_user_id_tokens_token_id_delete) | **DELETE** /users/{userId}/tokens/{tokenId} | Deletes a specific token associated with a user
[**users_user_id_tokens_token_id_get**](TokenInfoApi.md#users_user_id_tokens_token_id_get) | **GET** /users/{userId}/tokens/{tokenId} | Gets a specific token associated with a user

# **users_user_id_tokens_get**
> list[TokenInfoResponse] users_user_id_tokens_get(user_id)

Lists tokens associated with a user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.TokenInfoApi()
user_id = 'user_id_example' # str | The user ID.  

try:
    # Lists tokens associated with a user
    api_response = api_instance.users_user_id_tokens_get(user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenInfoApi->users_user_id_tokens_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 

### Return type

[**list[TokenInfoResponse]**](TokenInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_post**
> TokenResponse users_user_id_tokens_post(body, user_id)

Creates a new static token for a user.

This is only supported for users of type `service`.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.TokenInfoApi()
body = swagger_client.TokenDefinitionRequest() # TokenDefinitionRequest | The definition of the token.


user_id = 'user_id_example' # str | The user ID.  

try:
    # Creates a new static token for a user.
    api_response = api_instance.users_user_id_tokens_post(body, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenInfoApi->users_user_id_tokens_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenDefinitionRequest**](TokenDefinitionRequest.md)| The definition of the token.

 | 
 **user_id** | **str**| The user ID.   | 

### Return type

[**TokenResponse**](TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_token_id_delete**
> users_user_id_tokens_token_id_delete(user_id, token_id)

Deletes a specific token associated with a user

This is only possible for static tokens. To revoke session tokens - use `POST /auth/logout` with the required session token.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.TokenInfoApi()
user_id = 'user_id_example' # str | The user ID.  
token_id = 'token_id_example' # str | The token ID.  

try:
    # Deletes a specific token associated with a user
    api_instance.users_user_id_tokens_token_id_delete(user_id, token_id)
except ApiException as e:
    print("Exception when calling TokenInfoApi->users_user_id_tokens_token_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 
 **token_id** | **str**| The token ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_user_id_tokens_token_id_get**
> TokenInfoResponse users_user_id_tokens_token_id_get(user_id, token_id)

Gets a specific token associated with a user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.TokenInfoApi()
user_id = 'user_id_example' # str | The user ID.  
token_id = 'token_id_example' # str | The token ID.  

try:
    # Gets a specific token associated with a user
    api_response = api_instance.users_user_id_tokens_token_id_get(user_id, token_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenInfoApi->users_user_id_tokens_token_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The user ID.   | 
 **token_id** | **str**| The token ID.   | 

### Return type

[**TokenInfoResponse**](TokenInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

