# DeltaResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | A unique ID for the Delta | 
**metadata** | [**DeltaMetadataResponse**](DeltaMetadataResponse.md) |  | 
**modules** | [**ModuleDeltasResponse**](ModuleDeltasResponse.md) |  | 
**shared** | [**list[UpdateActionResponse]**](UpdateActionResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

