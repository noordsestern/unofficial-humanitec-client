# swagger_client.ImageApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_images_get**](ImageApi.md#orgs_org_id_images_get) | **GET** /orgs/{orgId}/images | List all Container Images
[**orgs_org_id_images_image_id_builds_get**](ImageApi.md#orgs_org_id_images_image_id_builds_get) | **GET** /orgs/{orgId}/images/{imageId}/builds | Lists all the Builds of an Image
[**orgs_org_id_images_image_id_builds_post**](ImageApi.md#orgs_org_id_images_image_id_builds_post) | **POST** /orgs/{orgId}/images/{imageId}/builds | Add a new Image Build
[**orgs_org_id_images_image_id_get**](ImageApi.md#orgs_org_id_images_image_id_get) | **GET** /orgs/{orgId}/images/{imageId} | Get a specific Image Object

# **orgs_org_id_images_get**
> list[ImageResponse] orgs_org_id_images_get(org_id)

List all Container Images

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  Lists all of the Container Images registered for this organization.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ImageApi()
org_id = 'org_id_example' # str | The organization ID.  

try:
    # List all Container Images
    api_response = api_instance.orgs_org_id_images_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImageApi->orgs_org_id_images_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 

### Return type

[**list[ImageResponse]**](ImageResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_builds_get**
> list[ImageBuildResponse] orgs_org_id_images_image_id_builds_get(org_id, image_id)

Lists all the Builds of an Image

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  The response lists all available Image Builds of an Image.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ImageApi()
org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Lists all the Builds of an Image
    api_response = api_instance.orgs_org_id_images_image_id_builds_get(org_id, image_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImageApi->orgs_org_id_images_image_id_builds_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

[**list[ImageBuildResponse]**](ImageBuildResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_builds_post**
> orgs_org_id_images_image_id_builds_post(body, org_id, image_id)

Add a new Image Build

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  This endpoint is used by Continuous Integration (CI) pipelines to notify Humanitec that a new Image Build is available.  If there is no Image with ID `imageId`, it will be automatically created.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ImageApi()
body = swagger_client.ImageBuildRequest() # ImageBuildRequest | The metadata associated with the build.


org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Add a new Image Build
    api_instance.orgs_org_id_images_image_id_builds_post(body, org_id, image_id)
except ApiException as e:
    print("Exception when calling ImageApi->orgs_org_id_images_image_id_builds_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ImageBuildRequest**](ImageBuildRequest.md)| The metadata associated with the build.

 | 
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_images_image_id_get**
> ImageResponse orgs_org_id_images_image_id_get(org_id, image_id)

Get a specific Image Object

DEPRECATED: This endpoint exists for historical compatibility and should not be used. Please use the [Artefact API](https://api-docs.humanitec.com/#tag/Artefact) instead.  The response includes a list of Image Builds as well as some metadata about the Image such as its Image Source.  Note, `imageId` may not be the same as the container name. `imageId` is determined by the system making notifications about new builds.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ImageApi()
org_id = 'org_id_example' # str | The organization ID.  
image_id = 'image_id_example' # str | The Image ID.  

try:
    # Get a specific Image Object
    api_response = api_instance.orgs_org_id_images_image_id_get(org_id, image_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImageApi->orgs_org_id_images_image_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The organization ID.   | 
 **image_id** | **str**| The Image ID.   | 

### Return type

[**ImageResponse**](ImageResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

