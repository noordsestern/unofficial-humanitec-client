# HumanitecErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **dict(str, object)** | (Optional) Additional information is enclosed here. | [optional] 
**error** | **str** | A short code to help with error identification. | 
**message** | **str** | A Human readable message about the error. | 
**status_code** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

