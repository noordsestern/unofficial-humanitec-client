# swagger_client.PipelineApprovalsApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**approve_approval_request**](PipelineApprovalsApi.md#approve_approval_request) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId}/approve | Approve the approval request
[**deny_approval_request**](PipelineApprovalsApi.md#deny_approval_request) | **POST** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId}/deny | Deny the approval request
[**get_approval_request**](PipelineApprovalsApi.md#get_approval_request) | **GET** /orgs/{orgId}/apps/{appId}/pipelines/{pipelineId}/runs/{runId}/jobs/{jobId}/approvals/{approvalId} | Get an approval request
[**list_approval_requests**](PipelineApprovalsApi.md#list_approval_requests) | **GET** /orgs/{orgId}/apps/{appId}/approvals | List of the approval requests

# **approve_approval_request**
> ApprovalRequest approve_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Approve the approval request

Approve the approval requested.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineApprovalsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Approve the approval request
    api_response = api_instance.approve_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApprovalsApi->approve_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deny_approval_request**
> ApprovalRequest deny_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Deny the approval request

Deny the approval requested.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineApprovalsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Deny the approval request
    api_response = api_instance.deny_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApprovalsApi->deny_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_approval_request**
> ApprovalRequest get_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)

Get an approval request

Get an approval request.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineApprovalsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
pipeline_id = 'pipeline_id_example' # str | The Pipeline ID
run_id = 'run_id_example' # str | The Run ID
job_id = 'job_id_example' # str | The Job ID
approval_id = 'approval_id_example' # str | The Approval ID

try:
    # Get an approval request
    api_response = api_instance.get_approval_request(org_id, app_id, pipeline_id, run_id, job_id, approval_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApprovalsApi->get_approval_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **pipeline_id** | **str**| The Pipeline ID | 
 **run_id** | **str**| The Run ID | 
 **job_id** | **str**| The Job ID | 
 **approval_id** | **str**| The Approval ID | 

### Return type

[**ApprovalRequest**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_approval_requests**
> list[ApprovalRequest] list_approval_requests(org_id, app_id, per_page=per_page, page=page, pipeline=pipeline, run=run, status=status)

List of the approval requests

List of the approval requests with in an app. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PipelineApprovalsApi()
org_id = 'org_id_example' # str | The Organization ID
app_id = 'app_id_example' # str | The Application ID
per_page = 50 # int | The maximum number of items to return in a page of results (optional) (default to 50)
page = 'page_example' # str | The page token to request from (optional)
pipeline = ['pipeline_example'] # list[str] | An optional list of Pipeline IDs. (optional)
run = ['run_example'] # list[str] | An optional Pipeline Run ID. (optional)
status = 'status_example' # str | Optional filter by status. (optional)

try:
    # List of the approval requests
    api_response = api_instance.list_approval_requests(org_id, app_id, per_page=per_page, page=page, pipeline=pipeline, run=run, status=status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApprovalsApi->list_approval_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID | 
 **app_id** | **str**| The Application ID | 
 **per_page** | **int**| The maximum number of items to return in a page of results | [optional] [default to 50]
 **page** | **str**| The page token to request from | [optional] 
 **pipeline** | [**list[str]**](str.md)| An optional list of Pipeline IDs. | [optional] 
 **run** | [**list[str]**](str.md)| An optional Pipeline Run ID. | [optional] 
 **status** | **str**| Optional filter by status. | [optional] 

### Return type

[**list[ApprovalRequest]**](ApprovalRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

