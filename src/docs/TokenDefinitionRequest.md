# TokenDefinitionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | A description of the token. (Optional) | [optional] 
**expires_at** | **str** | The time the token expires. If not set, the token will not expire. (Optional) | [optional] 
**id** | **str** | Identifier of the token. Must be unique for the user. | 
**type** | **str** | The type of the token. Can only be \&quot;static\&quot;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

