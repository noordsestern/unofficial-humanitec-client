# swagger_client.EventApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_jobs_delete**](EventApi.md#orgs_org_id_apps_app_id_jobs_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/jobs | Deletes all Jobs for the Application
[**orgs_org_id_apps_app_id_webhooks_get**](EventApi.md#orgs_org_id_apps_app_id_webhooks_get) | **GET** /orgs/{orgId}/apps/{appId}/webhooks | List Webhooks
[**orgs_org_id_apps_app_id_webhooks_job_id_delete**](EventApi.md#orgs_org_id_apps_app_id_webhooks_job_id_delete) | **DELETE** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Delete a Webhook
[**orgs_org_id_apps_app_id_webhooks_job_id_get**](EventApi.md#orgs_org_id_apps_app_id_webhooks_job_id_get) | **GET** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Get a Webhook
[**orgs_org_id_apps_app_id_webhooks_job_id_patch**](EventApi.md#orgs_org_id_apps_app_id_webhooks_job_id_patch) | **PATCH** /orgs/{orgId}/apps/{appId}/webhooks/{jobId} | Update a Webhook
[**orgs_org_id_apps_app_id_webhooks_post**](EventApi.md#orgs_org_id_apps_app_id_webhooks_post) | **POST** /orgs/{orgId}/apps/{appId}/webhooks | Create a new Webhook
[**orgs_org_id_events_get**](EventApi.md#orgs_org_id_events_get) | **GET** /orgs/{orgId}/events | List Events

# **orgs_org_id_apps_app_id_jobs_delete**
> orgs_org_id_apps_app_id_jobs_delete(org_id, app_id)

Deletes all Jobs for the Application

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Deletes all Jobs for the Application
    api_instance.orgs_org_id_apps_app_id_jobs_delete(org_id, app_id)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_jobs_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_get**
> list[WebhookResponse] orgs_org_id_apps_app_id_webhooks_get(org_id, app_id)

List Webhooks

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # List Webhooks
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_get(org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_webhooks_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**list[WebhookResponse]**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_delete**
> orgs_org_id_apps_app_id_webhooks_job_id_delete(org_id, app_id, job_id)

Delete a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Delete a Webhook
    api_instance.orgs_org_id_apps_app_id_webhooks_job_id_delete(org_id, app_id, job_id)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_webhooks_job_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_get**
> WebhookResponse orgs_org_id_apps_app_id_webhooks_job_id_get(org_id, app_id, job_id)

Get a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Get a Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_job_id_get(org_id, app_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_webhooks_job_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_job_id_patch**
> WebhookUpdateResponse orgs_org_id_apps_app_id_webhooks_job_id_patch(body, org_id, app_id, job_id)

Update a Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
body = swagger_client.WebhookRequest() # WebhookRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
job_id = 'job_id_example' # str | The Webhook ID.  

try:
    # Update a Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_job_id_patch(body, org_id, app_id, job_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_webhooks_job_id_patch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebhookRequest**](WebhookRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **job_id** | **str**| The Webhook ID.   | 

### Return type

[**WebhookUpdateResponse**](WebhookUpdateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_webhooks_post**
> WebhookResponse orgs_org_id_apps_app_id_webhooks_post(body, org_id, app_id)

Create a new Webhook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
body = swagger_client.WebhookRequest() # WebhookRequest | 
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  

try:
    # Create a new Webhook
    api_response = api_instance.orgs_org_id_apps_app_id_webhooks_post(body, org_id, app_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_apps_app_id_webhooks_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebhookRequest**](WebhookRequest.md)|  | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_events_get**
> list[EventResponse] orgs_org_id_events_get(org_id)

List Events

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.EventApi()
org_id = 'org_id_example' # str | The Organization ID.  

try:
    # List Events
    api_response = api_instance.orgs_org_id_events_get(org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->orgs_org_id_events_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 

### Return type

[**list[EventResponse]**](EventResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

