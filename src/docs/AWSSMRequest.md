# AWSSMRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth** | [**AWSAuthRequest**](AWSAuthRequest.md) |  | [optional] 
**endpoint** | **str** |  | [optional] 
**region** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

