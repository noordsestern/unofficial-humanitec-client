# coding: utf-8

"""
    Humanitec API

    # Introduction The *Humanitec API* allows you to automate and integrate Humanitec into your developer and operational workflows. The API is a REST based API. It is based around a set of concepts:  * Core * External Resources * Sets and Deltas  ## Authentication Almost all requests made to the Humanitec API require Authentication. Humanitec provides 2 ways of authenticating with the API: `Bearer` and `JWT`.  ### Bearer Authentication This form of authentication makes use of a **static token**. It is intended to be used when machines interact with the Humanitec API. Bearer tokens should be used for very narrow purposes. This allows for the token to be revoked if it is compromised and so limit the scope of exposure. New Bearer tokens can be obtained via the UI:  1. Log into Humanitec at https://app.humanitec.io 1. Go to **Organization Settings** 1. Select **API tokens** 1. Enter a *name* for the new token and click on **Generate new token**  The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_TOKEN`, the request could be made as follows:  ```     curl -H 'Authorization: Bearer HUMANITEC_TOKEN' https://api.humanitec.io/orgs/my-org/apps ```  ### JWT Authentication This form of authentication makes use of a **JSON Web Token (JWT)**. It is intended to be used when humans interact with the Humanitec API. JWTs expire after a period of time. This means that a new JWT will need to be generated regularly. This makes them well suited to working in short sessions, but not for automation. (See Bearer Authentication.) The token is passed to the API via the `Authorization` header. Assuming the issued token is `HUMANITEC_JWT`, the request could be made as follows:  ```     curl -H 'Authorization: JWT HUMANITEC_JWT' https://api.humanitec.io/orgs/my-org/apps ```  ## Content Types All of the Humanitec API unless explicitly only accepts content types of `application/json` and will always return valid `application/json` or an empty response.  ## Response Codes ### Success Any response code in the `2xx` range should be regarded as success.  | **Code** | **Meaning** | | --- | --- | | `200` | Success | | `201` | Success (In future, `201` will be replaced by `200`) | | `204` | Success, but no content in response |  _Note: We plan to simplify the interface by replacing 201 with 200 status codes._  ### Failure Any response code in the `4xx` should be regarded as an error which can be rectified by the client. `5xx` error codes indicate errors that cannot be corrected by the client.  | **Code** | **Meaning** | | --- | --- | | `400` | General error. (Body will contain details) | | `401` | Attempt to access protected resource without `Authorization` Header. | | `403` | The `Bearer` or `JWT` does not grant access to the requested resource. | | `404` | Resource not found. | | `405` | Method not allowed | | `409` | Conflict. Usually indicated a resource with that ID already exists. | | `422` | Unprocessable Entity. The body was not valid JSON, was empty or contained an object different from what was expected. | | `429` | Too many requests - request rate limit has been reached. | | `500` | Internal Error. If it occurs repeatedly, contact support. |   # noqa: E501

    OpenAPI spec version: 0.24.1
    Contact: apiteam@humanitec.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ArtefactVersionRequest(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'archived': 'bool',
        'artefact_id': 'str',
        'commit': 'str',
        'digest': 'str',
        'id': 'str',
        'name': 'str',
        'ref': 'str',
        'version': 'str'
    }

    attribute_map = {
        'archived': 'archived',
        'artefact_id': 'artefact_id',
        'commit': 'commit',
        'digest': 'digest',
        'id': 'id',
        'name': 'name',
        'ref': 'ref',
        'version': 'version'
    }

    def __init__(self, archived=None, artefact_id=None, commit=None, digest=None, id=None, name=None, ref=None, version=None):  # noqa: E501
        """ArtefactVersionRequest - a model defined in Swagger"""  # noqa: E501
        self._archived = None
        self._artefact_id = None
        self._commit = None
        self._digest = None
        self._id = None
        self._name = None
        self._ref = None
        self._version = None
        self.discriminator = None
        if archived is not None:
            self.archived = archived
        self.artefact_id = artefact_id
        if commit is not None:
            self.commit = commit
        if digest is not None:
            self.digest = digest
        self.id = id
        self.name = name
        if ref is not None:
            self.ref = ref
        if version is not None:
            self.version = version

    @property
    def archived(self):
        """Gets the archived of this ArtefactVersionRequest.  # noqa: E501

        If the Artefact Version is archived.  # noqa: E501

        :return: The archived of this ArtefactVersionRequest.  # noqa: E501
        :rtype: bool
        """
        return self._archived

    @archived.setter
    def archived(self, archived):
        """Sets the archived of this ArtefactVersionRequest.

        If the Artefact Version is archived.  # noqa: E501

        :param archived: The archived of this ArtefactVersionRequest.  # noqa: E501
        :type: bool
        """

        self._archived = archived

    @property
    def artefact_id(self):
        """Gets the artefact_id of this ArtefactVersionRequest.  # noqa: E501

        The UUID of the Artefact.  # noqa: E501

        :return: The artefact_id of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._artefact_id

    @artefact_id.setter
    def artefact_id(self, artefact_id):
        """Sets the artefact_id of this ArtefactVersionRequest.

        The UUID of the Artefact.  # noqa: E501

        :param artefact_id: The artefact_id of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """
        if artefact_id is None:
            raise ValueError("Invalid value for `artefact_id`, must not be `None`")  # noqa: E501

        self._artefact_id = artefact_id

    @property
    def commit(self):
        """Gets the commit of this ArtefactVersionRequest.  # noqa: E501

        (Optional) The commit ID the Artefact Version was built on.  # noqa: E501

        :return: The commit of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._commit

    @commit.setter
    def commit(self, commit):
        """Sets the commit of this ArtefactVersionRequest.

        (Optional) The commit ID the Artefact Version was built on.  # noqa: E501

        :param commit: The commit of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """

        self._commit = commit

    @property
    def digest(self):
        """Gets the digest of this ArtefactVersionRequest.  # noqa: E501

        (Optional) The Artefact Version digest.  # noqa: E501

        :return: The digest of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._digest

    @digest.setter
    def digest(self, digest):
        """Sets the digest of this ArtefactVersionRequest.

        (Optional) The Artefact Version digest.  # noqa: E501

        :param digest: The digest of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """

        self._digest = digest

    @property
    def id(self):
        """Gets the id of this ArtefactVersionRequest.  # noqa: E501

        The UUID of the Artefact Version.  # noqa: E501

        :return: The id of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ArtefactVersionRequest.

        The UUID of the Artefact Version.  # noqa: E501

        :param id: The id of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def name(self):
        """Gets the name of this ArtefactVersionRequest.  # noqa: E501

        The name of the Artefact.  # noqa: E501

        :return: The name of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ArtefactVersionRequest.

        The name of the Artefact.  # noqa: E501

        :param name: The name of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def ref(self):
        """Gets the ref of this ArtefactVersionRequest.  # noqa: E501

        (Optional) The ref the Artefact Version was built from.  # noqa: E501

        :return: The ref of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._ref

    @ref.setter
    def ref(self, ref):
        """Sets the ref of this ArtefactVersionRequest.

        (Optional) The ref the Artefact Version was built from.  # noqa: E501

        :param ref: The ref of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """

        self._ref = ref

    @property
    def version(self):
        """Gets the version of this ArtefactVersionRequest.  # noqa: E501

        (Optional) The version of the Artefact Version.  # noqa: E501

        :return: The version of this ArtefactVersionRequest.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this ArtefactVersionRequest.

        (Optional) The version of the Artefact Version.  # noqa: E501

        :param version: The version of this ArtefactVersionRequest.  # noqa: E501
        :type: str
        """

        self._version = version

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ArtefactVersionRequest, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ArtefactVersionRequest):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
