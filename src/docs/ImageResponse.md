# ImageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**added_at** | **str** | The time the first build of this Image was added to the organization | 
**builds** | [**list[ImageBuildResponse]**](ImageBuildResponse.md) | A list of Image Builds ordered by addition date. | 
**id** | **str** | The ID used to group different builds of the same Image together. | 
**source** | **str** | The Image Source that this Image is added via | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

