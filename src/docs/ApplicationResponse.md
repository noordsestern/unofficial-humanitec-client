# ApplicationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The timestamp in UTC indicates when the Application was created. | 
**created_by** | **str** | The user who created the Application. | 
**envs** | [**list[EnvironmentBaseResponse]**](EnvironmentBaseResponse.md) | The Environments associated with the Application. | 
**id** | **str** | The ID which refers to a specific application. | 
**name** | **str** | The Human-friendly name for the Application. | 
**org_id** | **str** | The Organization id of this Application | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

