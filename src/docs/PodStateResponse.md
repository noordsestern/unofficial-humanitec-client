# PodStateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_statuses** | **list[dict(str, object)]** |  | 
**phase** | **str** |  | 
**pod_name** | **str** |  | 
**revision** | **int** |  | 
**status** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

