# swagger_client.DeploymentApi

All URIs are relative to *https://api.humanitec.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get**](DeploymentApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys/{deployId}/errors | List errors that occurred in a Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get**](DeploymentApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys/{deployId} | Get a specific Deployment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_get**](DeploymentApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_get) | **GET** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys | List Deployments in an Environment.
[**orgs_org_id_apps_app_id_envs_env_id_deploys_post**](DeploymentApi.md#orgs_org_id_apps_app_id_envs_env_id_deploys_post) | **POST** /orgs/{orgId}/apps/{appId}/envs/{envId}/deploys | Start a new Deployment.

# **orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get**
> list[DeploymentErrorResponse] orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get(org_id, app_id, env_id, deploy_id)

List errors that occurred in a Deployment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeploymentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
deploy_id = 'deploy_id_example' # str | The Deployment ID.  

try:
    # List errors that occurred in a Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get(org_id, app_id, env_id, deploy_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_errors_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **deploy_id** | **str**| The Deployment ID.   | 

### Return type

[**list[DeploymentErrorResponse]**](DeploymentErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get**
> DeploymentResponse orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get(org_id, app_id, env_id, deploy_id)

Get a specific Deployment.

Gets a specific Deployment in an Application and an Environment.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeploymentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  
deploy_id = 'deploy_id_example' # str | The Deployment ID.  

try:
    # Get a specific Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get(org_id, app_id, env_id, deploy_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->orgs_org_id_apps_app_id_envs_env_id_deploys_deploy_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 
 **deploy_id** | **str**| The Deployment ID.   | 

### Return type

[**DeploymentResponse**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_get**
> list[DeploymentResponse] orgs_org_id_apps_app_id_envs_env_id_deploys_get(org_id, app_id, env_id)

List Deployments in an Environment.

List all of the Deployments that have been carried out in the current Environment. Deployments are returned with the newest first.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeploymentApi()
org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # List Deployments in an Environment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_get(org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->orgs_org_id_apps_app_id_envs_env_id_deploys_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**list[DeploymentResponse]**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orgs_org_id_apps_app_id_envs_env_id_deploys_post**
> DeploymentResponse orgs_org_id_apps_app_id_envs_env_id_deploys_post(body, org_id, app_id, env_id)

Start a new Deployment.

At Humanitec, Deployments are defined as changes to the state of the Environment. The state can be changed by defining a set of desired changes to the current state via a Deployment Delta or by resetting the current state after a previous Deployment. (See Environment Rebase.) Both types of changes can be combined into a single Deployment during which the Delta is applied to the Rebased state.  When specifying a Delta, a Delta ID must be used. That Delta must have been committed to the Delta store prior to the Deployment.  A Set ID can also be defined in the deployment to force the state of the environment to a particular state. This will be ignored if the Delta is specified.  **NOTE:**  Directly setting a `set_id` in a deployment is not recommended as it will not record history of where the set came from. If the intention is to replicate an existing environment, use the environment rebasing approach described above.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DeploymentApi()
body = swagger_client.DeploymentRequest() # DeploymentRequest | The Delta describing the change to the Environment and a comment.


org_id = 'org_id_example' # str | The Organization ID.  
app_id = 'app_id_example' # str | The Application ID.  
env_id = 'env_id_example' # str | The Environment ID.  

try:
    # Start a new Deployment.
    api_response = api_instance.orgs_org_id_apps_app_id_envs_env_id_deploys_post(body, org_id, app_id, env_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->orgs_org_id_apps_app_id_envs_env_id_deploys_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeploymentRequest**](DeploymentRequest.md)| The Delta describing the change to the Environment and a comment.

 | 
 **org_id** | **str**| The Organization ID.   | 
 **app_id** | **str**| The Application ID.   | 
 **env_id** | **str**| The Environment ID.   | 

### Return type

[**DeploymentResponse**](DeploymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

