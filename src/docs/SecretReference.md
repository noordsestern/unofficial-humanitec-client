# SecretReference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store** | **str** | Secret Store id. This can&#x27;t be &#x60;humanitec&#x60; (our internal Secret Store). It&#x27;s mandatory if &#x60;ref&#x60; is defined and can&#x27;t be used if &#x60;value&#x60; is defined. | [optional] 
**ref** | **str** | Secret reference in the format of the target store. It can&#x27;t be defined if &#x60;value&#x60; is defined. | [optional] 
**version** | **str** | Optional, only valid if &#x60;ref&#x60; is defined. It&#x27;s the version of the secret as defined in the target store. | [optional] 
**value** | **str** | Value to store in the secret store. It can&#x27;t be defined if &#x60;ref&#x60; is defined. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

