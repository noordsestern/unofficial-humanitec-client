# ManifestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **object** | Manifest data to inject. | [optional] 
**location** | **str** | Location to inject the Manifest at. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

