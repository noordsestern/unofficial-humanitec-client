# ArtefactResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | The time when the Artefact was added to Humanitec. | [optional] 
**created_by** | **str** | The user ID of the user who added the Artefact to Humanitec. | [optional] 
**id** | **str** | The UUID of the Artefact. | 
**name** | **str** | The name of the Artefact. | 
**type** | **str** | The type of the Artefact. | 
**updated_at** | **str** | The time when the Artefact was updated for the last time. | [optional] 
**updated_by** | **str** | The user ID of the user who updated the Artefact for the last time. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

