# RunJobListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The id of the Job within the Run. | [optional] 
**etag** | **str** | The current entity tag value for this Job. | [optional] 
**org_id** | **str** | The id of the Organization containing this Job. | [optional] 
**app_id** | **str** | The id of the Application containing this Job. | [optional] 
**pipeline_id** | **str** | The id of the Pipeline. | [optional] 
**pipeline_version** | **str** | The id of the Pipeline Version associated with the Run. | [optional] 
**run_id** | **str** | The id of the Run containing this Job. | [optional] 
**status** | **str** | The current status of this Job. | [optional] 
**waiting_for** | **str** | An event on which job is waiting | [optional] 
**status_message** | **str** | A human-readable message indicating the reason for the status. | [optional] 
**created_at** | **datetime** | The date and time when this Job was first created within the Run. | [optional] 
**cancellation_requested_at** | **datetime** | The date and time when cancellation of this Job was requested. | [optional] 
**completed_at** | **datetime** | The date and time when this Job entered a successful, failed, or cancelled status. | [optional] 
**timeout_seconds** | **int** | The timeout for this Job. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

